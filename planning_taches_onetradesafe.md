

	Modules
> gestion des comptes utilisateur
	- création de comptes
	- validation du mail
	- activation de comptes
	- vérification du compte ( de l’identité ) : photo de la phase avant et arrière de sa pièce d'identité
	- validation de l'ouverture d'un compte par un admin 
	- suspension/activation des comptes utilisateurs par un admin
	- connexion/déconnexion des utilisateurs
	- consultation du profil
	- modification du profil
	- tableau de bord utilisateur
	- tableau de bord de l'administrateur
	
		

> Modules de trading
	- Consulter les cours des cryptomonnaies en temps réel
	- recharge d'un compte
		+ le client initie l’opération de recharge
		+ l'administrateur valide
	- Passage d'ordres d'achat et de vente de cryptomonnaies : un client peut acheter / vendre de la cryptomonnaies
		+ l'administrateur peut donner le sens du marché
	- Le trading plateform
		+ un client peut commencer a trader ( montant, dateDebut, dateFin)
		+ un client peut acheter / vendre ces parts du marche
	-  Afficher des trades en bougie.
	-  Consultation de l'historique des transactions
	- transfert des fonds de lever pour exchange -Spécifications de la Partie Utilisateur
	-Fonctionnalité de Trading
    - Solde du Compte
    +	Le solde du compte est initialement vide à l'ouverture du compte client.
    +	L'administrateur peut modifier le solde de chaque compte client.
    - Nombre de Lots/Hands
    +	L'utilisateur saisit la valeur du nombre de lots/hands avec lequel il souhaite trader.
    +	Cette valeur est aléatoire et laissée au choix du client.
    - Achat ou Vente
    +	L'utilisateur choisit si le marché est à l'achat (Buy) ou à la vente (Sell).
    +	Cette action fait monter ou baisser la valeur.
    - Confirmation de l'Action
    +	Une page de confirmation s'affiche pour demander à l'utilisateur de valider son action.
    +	Après confirmation, le trading commence.
    - Représentation du Trading en Cours
    +	La page affiche le prix d'ouverture (opening price), le prix actuel du marché (current price), les fonds investis, le nombre de lots en cours de trading, la marge et le profit/la perte.
    +	La valeur du profit/la perte change dynamiquement en fonction de la variation du marché.
    +	Le solde du compte est automatiquement mis à jour en fonction des gains ou des pertes.
    - Historique du Trading
    +	L'historique affiche les transactions passées, y compris le prix d'ouverture, le prix de clôture, le nombre de lots, le profit/la perte et la date/heure.
    - Points d'Ombre
    - Variation du Marché
    +	Le marché est contrôlé par l'administrateur qui fait varier les prix de manière positive ou négative.
    +	La variation du marché doit être réaliste et cohérente avec les fluctuations du marché réel.
    - Gestion des Risques
    +	Des limites doivent être définies pour empêcher les pertes excessives.
    +	Des mécanismes de protection, tels que le stop-loss et le take-profit, doivent être implémentés pour permettre aux utilisateurs de limiter leurs pertes et de sécuriser leurs gains.
    - Sécurité
    +	L'application doit implémenter des mesures de sécurité robustes pour protéger les fonds et les données des utilisateurs.
    +	L'authentification et l'autorisation doivent être correctement implémentées pour limiter l'accès aux fonctionnalités de trading.
    - Spécifications de la Partie Administrateur
    - Fonctionnalités
    +	Gestion des utilisateurs:
    +	Création, modification et suppression de comptes utilisateurs.
    +	Affichage de la liste des utilisateurs avec leurs informations respectives (solde, historique des trades, etc.).
    +	Gestion des rôles et des permissions pour les utilisateurs.
    +	Gestion du marché:
    +	Contrôle des prix du marché en temps réel.
    +	Simulation de fluctuations réalistes du marché.
    +	Définition de paramètres de marché tels que les spreads, les marges et les limites de trading.
    +	Surveillance des trades:
    +	Visualisation en temps réel des trades en cours.
    +	Accès à l'historique des trades de tous les utilisateurs.
    +	Analyse des performances de trading des utilisateurs.
    +	Gestion des finances:
    +	Visualisation du solde total des fonds de l'application.
    +	Ajout et retrait de fonds pour les besoins de l'application.
    +	Gestion des transactions financières liées aux trades des utilisateurs.
    +	Configuration générale:
    +	Paramétrage des règles de trading (stop-loss, take-profit, marges, etc.).
    +	Définition des limites de trading pour les utilisateurs.
    +	Gestion des notifications et des alertes.
    - Points d'Attention
    +	Sécurité:
    +	Implémentation de mesures de sécurité robustes pour protéger les fonds et les données des utilisateurs.
    +	Authentification et autorisation strictes pour l'accès aux fonctionnalités d'administration.
    +	Protection contre les intrusions et les accès non autorisés.
    +	Transparence:
    +	Mise en place d'un système transparent pour la gestion du marché et des trades.
    +	Communication claire des règles et des paramètres de trading aux utilisateurs.
    +	Disponibilité d'outils de suivi et d'analyse pour les utilisateurs.
    +	Gestion des risques:
    +	Surveillance constante des activités de trading pour détecter les comportements suspects.
    +	Mise en place de mécanismes de contrôle pour limiter les risques financiers.
    +	Protection des intérêts des utilisateurs et de la plateforme de trading.


AppUser
- fullnames, 
- email*, 
- password*

AppClient
- nomComplet, 
- email*, 
- password*, 
- etat [en attente de vérification de mail, en attente de validation, en attente de complément d'information, actif, rejeter, désactiver ]
- statut [en ligne, offligne]
- position [en trading... / Hors trading]
- solde
- token_mail
- token_user


AppValidationOperationCompte
- idCompteUser
- motifValidationOperation
- opération [en attente de vérification de mail, en attente de validation, en attente de complément d'information, actif, rejeter, désactiver ]

AppIdentité
- type
- numero
- dateEmission
- dateExpiration
- photoIdentiteFace
- photoIdentiteVerso

AppTransaction
type: [DEPOT, RETRAIT]
idCompteClient
montant
référence
motif
statut [en attente, Valider, rejeter]

AppValidationTransaction
type: [DEPOT, RETRAIT]
idValideur
idTransaction  
motif
operation [Valider, rejeter]


AppTrading
prixOuverture
prixCourant
marge
statut	[ENCOURS, ARRET]

Ordre
id : Long
utilisateur : Utilisateur
type : TypeOrdre (ACHAT, VENTE)
cryptomonnaie : Cryptomonnaie
quantite : double
prix : double
dateExecution : LocalDateTime

Cryptomonnaie
id : Long
nom : String
symbole : String
prixActuel : double

Transaction
id : Long
ordre : Ordre
prixOuverture : double
prixFermeture : double
profit : double
dateOuverture : LocalDateTime
dateFermeture : LocalDateTime

MarketData
id : Long
cryptomonnaie : Cryptomonnaie
prix : double
dateHeure : LocalDateTime

Repositories
UtilisateurRepository
OrderRepository
CryptomonnaieRepository
TransactionRepository
MarketDataRepository

Services
UtilisateurService
creerUtilisateur(Utilisateur)
modifierUtilisateur(Utilisateur)
supprimerUtilisateur(Long)
getUtilisateur(Long)
getAllUtilisateurs()

OrderService
creerOrdre(Ordre)
modifierOrdre(Ordre)
supprimerOrdre(Long)
getOrdre(Long)
getAllOrdres()
getAllOrdresParUtilisateur(Utilisateur)

CryptomonnaieService
creerCryptomonnaie(Cryptomonnaie)
modifierCryptomonnaie(Cryptomonnaie)
supprimerCryptomonnaie(Long)
getCryptomonnaie(Long)
getAllCryptomonnaies()
updatePrixCryptomonnaie(Cryptomonnaie, double)

TransactionService
creerTransaction(Transaction)
getTransaction(Long)
getAllTransactions()
getAllTransactionsParUtilisateur(Utilisateur)

MarketDataService
enregistrerMarketData(MarketData)
getMarketData(Long)
getAllMarketData()
getAllMarketDataParCryptomonnaie(Cryptomonnaie)

Contrôleurs
UtilisateurController
creerUtilisateur(UtilisateurDto)
modifierUtilisateur(Long, UtilisateurDto)
supprimerUtilisateur(Long)
getUtilisateur(Long)
getAllUtilisateurs()
OrderController
creerOrdre(OrderDto)
modifierOrdre(Long, OrderDto)
supprimerOrdre(Long)
getOrdre(Long)
getAllOrdres()
getAllOrdresParUtilisateur(Long)

CryptomonnaieController
creerCryptomonnaie(CryptomonnaieDto)
modifierCryptomonnaie(Long, CryptomonnaieDto)
supprimerCryptomonnaie(Long)
getCryptomonnaie(Long)
getAllCryptomonnaies()

TransactionController
creerTransaction(TransactionDto)
getTransaction(Long)
getAllTransactions()
getAllTransactionsParUtilisateur(Long)

MarketDataController
enregistrerMarketData(MarketDataDto)
getMarketData(Long)
getAllMarketData()
getAllMarketDataParCryptomonnaie(Long)

UserService:
- createUser(User user): Crée un nouvel utilisateur et envoie un e-mail de validation
- validateEmail(String token): Valide l'adresse e-mail en vérifiant le jeton
- activateAccount(Long userId): Active le compte d'un utilisateur
- verifyIdentity(Long userId, String facePhoto, String backPhoto): Vérifie l'identité de l'utilisateur en examinant les photos de la pièce d'identité
- suspendAccount(Long userId): Suspend le compte d'un utilisateur
- activateAccount(Long userId): Active le compte d'un utilisateur
- findUserById(Long userId): Recherche un utilisateur par son ID
- updateUser(User user): Met à jour les informations d'un utilisateur
-deleteUser(Long userId): Supprime un utilisateur (avec précautions)

AdminService:

- validateAccountRequest(Long userId): Valide la demande d'ouverture de compte d'un utilisateur
- findUsersForApproval(): Recherche les utilisateurs en attente de validation

