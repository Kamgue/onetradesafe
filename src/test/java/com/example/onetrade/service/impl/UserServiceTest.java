package com.example.onetrade.service.impl;

import com.example.onetrade.handler.NotFoundException;
import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.AppUser;
import com.example.onetrade.model.entities.UserRoleEntity;
import com.example.onetrade.model.entities.enums.ClientPositionEnum;
import com.example.onetrade.model.entities.enums.ClientStateEnum;
import com.example.onetrade.model.entities.enums.ClientStatusEnum;
import com.example.onetrade.model.entities.enums.UserRoleEnum;
import com.example.onetrade.model.repostiory.AppClientRepository;
import com.example.onetrade.model.repostiory.UserRepository;
import com.example.onetrade.model.service.SignUpServiceModel;
import com.example.onetrade.service.UserRoleService;
import com.example.onetrade.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

;


@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    private UserService userServiceToTest;
    private UserRepository mockUserRepository;
    private AppClientRepository mockAppClientRepository;
    private UserRoleService mockUserRoleService;
    private AppUser appUser;
    private AppClient appClient;
    private SignUpServiceModel signUpServiceModel;



    @BeforeEach
    public void setUp() {
        mockUserRepository = mock(UserRepository.class);
        mockAppClientRepository = mock(AppClientRepository.class);
        PasswordEncoder mockPasswordEncoder = mock(BCryptPasswordEncoder.class);
        mockUserRoleService = mock(UserRoleService.class);
        ModelMapper modelMapper = new ModelMapper();
        // userServiceToTest = new UserServiceImpl(modelMapper, mockUserRepository, mockPasswordEncoder, mockAppClientRepository,
        //         mockUserRoleService);
        // prepare data client
        signUpServiceModel = new SignUpServiceModel();
        signUpServiceModel.setPassword("topsecret");
        signUpServiceModel.setConfirmPassword("topsecret");
        signUpServiceModel.setEmail("testemail@gmail.com");
        signUpServiceModel.setFullname("full name");
        signUpServiceModel.setPosition(ClientPositionEnum.EXCLUDING_TRADING);
        signUpServiceModel.setState(ClientStateEnum.ACTIVE);
        signUpServiceModel.setStatus(ClientStatusEnum.OFFLINE);
        signUpServiceModel.setSolde(0);
        UserRoleEntity roleUser = new UserRoleEntity();
        roleUser.setRole(UserRoleEnum.USER);
        UserRoleEntity roleAdmin = new UserRoleEntity();
        roleAdmin.setRole(UserRoleEnum.ADMIN);
        appClient = modelMapper.map(signUpServiceModel, AppClient.class);
        appClient.setRoles(List.of(roleUser, roleAdmin));


        // prepare data user Entity
        appUser = new AppUser();
        appUser.setEmail("email@gmail");
        appUser.setFullName("username");
        appUser.setPassword("topsecret");
        UserRoleEntity role= new UserRoleEntity();
        role.setRole(UserRoleEnum.USER);
        appUser.setRoles(List.of(role));
        appUser.setId(1L);


        // configure mocks
        when(mockPasswordEncoder.encode("topsecret"))
                .thenReturn("topsecret");
        when(mockUserRepository.save(Mockito.any(AppUser.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        when(mockAppClientRepository.save(Mockito.any(AppClient.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        when(mockUserRoleService.saveRole(Mockito.any(UserRoleEntity.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        when(mockUserRoleService.getUserRoleByEnumName(UserRoleEnum.USER))
                .thenReturn(new UserRoleEntity() {{
                    setRole(UserRoleEnum.USER);
                }});
        when(mockUserRoleService.getUserRoleByEnumName(UserRoleEnum.ADMIN))
                .thenReturn(new UserRoleEntity() {{
                    setRole(UserRoleEnum.ADMIN);
                }});

    }

    @Test
    void testUserNotFound() {
        Assertions.assertThrows(
                NotFoundException.class, () -> {
                    userServiceToTest.findUserById(null);
                    userServiceToTest.findUserByFullname("can-not-find-user");
                    userServiceToTest.deleteAppClient(null);
                    userServiceToTest.findAppClientById(null);
                    mockAppClientRepository.findByFullName("can-not-find-user");
                }


        );
    }



    @Test
    void register_should_work() {

        when(mockAppClientRepository.findByFullName("user")).
                thenReturn(Optional.of(appClient));

        AppClient register = userServiceToTest.register(signUpServiceModel);
        assertEquals(appClient.getFullName(), register.getFullName());


    }


    @Test
    void findUserById_should_Work() {

        when(mockUserRepository.findById(1L)).
                thenReturn(Optional.of(appUser));
        Assertions.assertEquals(appUser, userServiceToTest.findUserById(1L) );
    }

    //
    @Test
    void findUserByUsername_should_work() {
        when(mockUserRepository.findByFullName("user")).
                thenReturn(Optional.of(appUser));
        Assertions.assertEquals(appUser, userServiceToTest.findUserByFullname("user"));
    }


    @Test
    void userExists_should_work() {
        when(mockUserRepository.findByFullName("user")).
                thenReturn(Optional.of(appUser));

        Assertions.assertTrue(userServiceToTest.userExists("user", "email"));

    }
    @Test
    void seedUsersAndUserRoles_should_work() {

        userServiceToTest.seedUsersAndUserRoles();
        assertEquals(3, userServiceToTest.seedUsersAndUserRoles().size());
    }


    @Test
    void deleteUser_should_work() {
        when(mockUserRepository.findById(1L))
                .thenReturn(Optional.of(appUser));

        userServiceToTest.deleteUser(appUser.getId());

        Mockito.verify(mockUserRepository, times(1)).delete(appUser);
    }

    @Test
    void deleteAppClient_should_work() {
        when(mockAppClientRepository.findById(1L))
                .thenReturn(Optional.of(appClient));

        userServiceToTest.deleteAppClient(1L);

        Mockito.verify(mockAppClientRepository, times(1)).delete(appClient);
    }


    @Test
    void findAppClientById_should_work() {
        when(mockAppClientRepository.findById(1L)).
                thenReturn(Optional.of(appClient));
        Assertions.assertEquals(appClient, userServiceToTest.findAppClientById(1L));
    }


    @Test
    void expireUserSessions_should_work() {

        Authentication authentication = mock(Authentication.class);

        userServiceToTest.expireUserSessions();
        Assertions.assertFalse(authentication.isAuthenticated());
    }


}