package com.example.onetrade.service.impl;

import com.example.onetrade.model.repostiory.TestRepository;
import com.example.onetrade.model.service.TestServiceModel;
import com.example.onetrade.service.TestService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

class TestServiceImplTest {
    private TestRepository mockTestRepository = Mockito.mock(TestRepository.class);
    private TestService mockTestService = Mockito.mock(TestService.class);
    @Test
    void saveTest() {

        TestServiceModel testServiceModel = new TestServiceModel();

        when(mockTestRepository.save(Mockito.any( com.example.onetrade.model.entities.Test.class)))
                .thenAnswer(i -> i.getArguments()[0]);
         mockTestService.saveTest(testServiceModel);

        assertNotNull(mockTestRepository.findById(1L));

    }
}