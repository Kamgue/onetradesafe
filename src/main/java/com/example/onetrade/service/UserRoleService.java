package com.example.onetrade.service;

import com.example.onetrade.model.entities.UserRoleEntity;
import com.example.onetrade.model.entities.enums.UserRoleEnum;

public interface UserRoleService {
    UserRoleEntity getUserRoleByEnumName(UserRoleEnum userRoleEnum);

    UserRoleEntity saveRole(UserRoleEntity userRoleEntity);
}
