package com.example.onetrade.service.impl;

import com.example.onetrade.handler.NotFoundException;
import com.example.onetrade.model.binding.request.NetworkModel;
import com.example.onetrade.model.binding.response.NetworkResponseModel;
import com.example.onetrade.model.entities.Network;
import com.example.onetrade.model.repostiory.NetworkRepository;
import com.example.onetrade.service.BaseConfigService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/août/2024 -- 05:58
 *
 * @author name :  Franky Brice on 07/08/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.service.impl
 **/
@Service
public class BaseConfigServiceImpl implements BaseConfigService {

    private final NetworkRepository networkRepository;

    public BaseConfigServiceImpl(NetworkRepository networkRepository) {
        this.networkRepository = networkRepository;
    }

    @Override
    public NetworkResponseModel addOrUpdateNetwork(NetworkModel model){

        Network networkToSave;
        if (Objects.nonNull(model.getId()) && model.getId()>0){ // cas de la mise a jours
         Network network = findNetWorkById(model.getId());
         if (!network.getName().equals(model.getName()) && networkRepository.existsByName(model.getName()))
             throw new RuntimeException("A network already exists by name : "+model.getName());
            networkToSave = NetworkModel.buildFromUpdate(network, model);
        }else{ // cas de l'ajout
            if (networkRepository.existsByName(model.getName())){
                throw new RuntimeException("A network already exists by name : "+model.getName());
            }
            networkToSave = NetworkModel.buildFromCreate(model);
        }
        return NetworkResponseModel.buildFromEntity(networkRepository.save(networkToSave));
    }

    @Override
    public Network findNetWorkById(Long id){
        return networkRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Network Not found with ID "+id));
    }

    @Override
    public Page<NetworkResponseModel> findAllNetwork(String token, Pageable pageable){
        return NetworkResponseModel.buildFromEntityPage(networkRepository.findByToken("%"+token+"%", pageable));
    }
    @Override
    public List<NetworkResponseModel> findAllNetwork(){
        return NetworkResponseModel.buildFromEntityList(networkRepository.findAll());
    }

    @Transactional
    @Override
    public void deleteNetwork(long id) {
        networkRepository.delete(findNetWorkById(id));
    }
}
