package com.example.onetrade.service.impl;

import com.example.onetrade.handler.NotFoundException;
import com.example.onetrade.model.binding.request.FileAttachment;
import com.example.onetrade.model.binding.request.MessageModel;
import com.example.onetrade.model.binding.response.ClientResponseModel;
import com.example.onetrade.model.binding.response.ConversationResponseModel;
import com.example.onetrade.model.binding.response.MessageResponseModel;
import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.Conversation;
import com.example.onetrade.model.entities.Message;
import com.example.onetrade.model.entities.enums.TypePieceJointeEnum;
import com.example.onetrade.model.repostiory.ConversationRepository;
import com.example.onetrade.model.repostiory.MessageRepository;
import com.example.onetrade.service.ChatService;
import com.example.onetrade.service.PieceJointeService;
import com.example.onetrade.service.UserService;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Base64;
import java.util.List;
import java.util.Objects;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 19/juillet/2024 -- 06:57
 *
 * @author name :  Franky Brice on 19/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.service.impl
 **/
@Service
public class ChatServiceImpl implements ChatService {

    private final MessageRepository messageRepository;
    private final PieceJointeService pieceJointeService;

    private final ConversationRepository conversationRepository;
    private final UserService userService;

    public ChatServiceImpl(MessageRepository messageRepository, PieceJointeService pieceJointeService, ConversationRepository conversationRepository, UserService userService) {
        this.messageRepository = messageRepository;
        this.pieceJointeService = pieceJointeService;
        this.conversationRepository = conversationRepository;
        this.userService = userService;
    }

    @Override
    public Conversation createConversation(AppClient admin) {
        Conversation conversation = new Conversation();
        conversation.setUser(admin);
        return conversationRepository.save(conversation);
    }
    @Override
    public List<ConversationResponseModel> findAllConversations() {
        return ConversationResponseModel.buildFromEntityList(conversationRepository.findAll(), pieceJointeService);
    }
    @Override
    public List<ClientResponseModel> findClientForNewConversation() {
        return ClientResponseModel.buildFromEntityList(conversationRepository.findClientForNewConversation());
    }
    @Override
    public ConversationResponseModel findOneConversation(Long idConversation) {
        Conversation conversation = findConversationById(idConversation);
        List<Message> messages = messageRepository.findByConversationIdOrderByCreatedAtAsc(idConversation);
        return ConversationResponseModel.buildFromEntity(conversation, messages, pieceJointeService);
    }
    public Conversation findConversationById(Long idConversation) {
        return conversationRepository.findById(idConversation).orElseThrow(() -> new NotFoundException("Can not find conversation by ID " + idConversation));
    }

    @Override
    public Message saveMessage(long idConversation, AppClient sender, AppClient receiver, String content) {
        Conversation conversation = conversationRepository.findById(idConversation).orElse(null);
        return messageRepository.save(MessageModel.buildFormEntity(conversation, sender, receiver, content));
    }
    @Transactional
    @Override
    public MessageResponseModel sendMessage(MessageModel messageModel) {
        Conversation conversation = findConversationById(messageModel.getIdConversation());
        AppClient sender = userService.findAppClientById(messageModel.getIdUserConnected());
        AppClient receiver = userService.findAppClientById(conversation.getUser().getId());
        Message messageSave = messageRepository.save(MessageModel.buildFormEntity(conversation, sender, receiver, messageModel.getContent()));

        if (messageModel.getFiles() != null) {
            for (FileAttachment file : messageModel.getFiles()) {
                pieceJointeService.uploadPiecesJointe(conversation.getId(), messageSave.getId(), getMultipartFile(file), TypePieceJointeEnum.CHAT_FILE);
            }
        }

        //List<PieceJointeModel> pieceJointes = pieceJointeService.findListPieceJointeByTypeEntiteAndId(TypePieceJointeEnum.CHAT_FILE, messageSave.getId());

        return MessageResponseModel.buildFromEntity(messageSave, pieceJointeService);
    }

    private MockMultipartFile getMultipartFile(FileAttachment file) {
        try {
            String base64Data = file.getData();
            byte[] data = decodeBase64(base64Data);
            return new MockMultipartFile(file.getName(), file.getName(), file.getType(), data);
        } catch (IllegalArgumentException e) {
            // Loggez les données problématiques pour le diagnostic
            System.err.println("Base64 data is invalid: " + file.getData());
            throw e; // Propager l'exception ou gérer l'erreur comme vous le souhaitez
        }
    }
    public static byte[] decodeBase64(String base64String) {
        if (base64String == null || base64String.trim().isEmpty()) {
            throw new IllegalArgumentException("Base64 data is empty or null");
        }

        // Retirer les préfixes de type MIME
        String cleanedBase64String = base64String;

        if (cleanedBase64String.startsWith("data:")) {
            int commaIndex = cleanedBase64String.indexOf(',');
            if (commaIndex != -1) {
                cleanedBase64String = cleanedBase64String.substring(commaIndex + 1);
            }
        }

        cleanedBase64String = cleanedBase64String.replaceAll("\\s", "");

        if (cleanedBase64String.isEmpty()) {
            throw new IllegalArgumentException("Base64 data is invalid after cleaning");
        }

        return Base64.getDecoder().decode(cleanedBase64String);
    }
    public static byte[] decodeBase641(String base64String) {
        // Retirer les préfixes et les espaces
        //String cleanedBase64String = base64String.replaceAll("^(data:\\w+\\/\\w+;base64,)?", "").replaceAll("\\s", "");

        // Retirer les préfixes de type MIME et les espaces
        String cleanedBase64String = base64String.replaceFirst("^data:[a-zA-Z0-9+/]+;base64,", "").replaceAll("\\s", "");
        return Base64.getDecoder().decode(cleanedBase64String);
    }
    @Override
    public List<Message> findAllMessageUser(long idUser) {
        return messageRepository.findBySenderIdOrReceiverId(idUser);
    }

    @Override
    public ConversationResponseModel findConversationClient(long idClient) {

        Conversation conversation = conversationRepository.findByUserId(idClient).orElse(null);

        if (Objects.isNull(conversation))
            conversation = addConversation(idClient);

        if (Objects.nonNull(conversation)) {
            List<Message> messages = messageRepository.findByConversationIdOrderByCreatedAtAsc(conversation.getId());
            return ConversationResponseModel.buildFromEntity(conversation, messages, pieceJointeService);
        }else return null;
    }


    @Override
    public Conversation addConversation(long idUser) {
        AppClient user = userService.findAppClientById(idUser);
        if (!conversationRepository.existsByUserId(idUser))
            return conversationRepository.save(Conversation.ConversationBuilder.aConversation().user(user).build());
        else return null;
    }
}
