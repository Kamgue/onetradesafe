package com.example.onetrade.service.impl;

import com.example.onetrade.config.AppProperties;
import com.example.onetrade.handler.NotFoundException;
import com.example.onetrade.model.binding.PieceJointeModel;
import com.example.onetrade.model.entities.PieceJointe;
import com.example.onetrade.model.entities.enums.TypePieceJointeEnum;
import com.example.onetrade.model.repostiory.PieceJointeRepository;
import com.example.onetrade.service.PieceJointeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.io.Files.getFileExtension;
/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/juillet/2024 -- 03:35
 *
 * @author name :  Franky Brice on 07/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.service.impl
 **/
@Slf4j
@Service
public class PieceJointeServiceImpl implements PieceJointeService {

    private final PieceJointeRepository pieceJointeRepository;
    private final AppProperties appProperties;
    private final ResourceLoader resourceLoader;

    public PieceJointeServiceImpl(PieceJointeRepository pieceJointeRepository, AppProperties appProperties, ResourceLoader resourceLoader) {
        this.pieceJointeRepository = pieceJointeRepository;
        this.appProperties = appProperties;
        this.resourceLoader = resourceLoader;
    }
    @Transactional
    @Override
    public PieceJointe ajouterPieceJointe(MultipartFile file, PieceJointeModel dto) throws IOException {
        String extension;
        String originalName;
        String storageName;

        boolean operationClient = Arrays.stream(new TypePieceJointeEnum[]{TypePieceJointeEnum.AVATAR_USERS,TypePieceJointeEnum.ID_CARD_BACK,TypePieceJointeEnum.ID_CARD_FRONT,TypePieceJointeEnum.SELFY_USERS})
                .anyMatch(a->a.equals(TypePieceJointeEnum.valueOf(dto.getTypePj())));
        boolean operationChat = TypePieceJointeEnum.CHAT_FILE.equals(TypePieceJointeEnum.valueOf(dto.getTypePj()));
        String baseDir;
        if (operationClient) {
            baseDir = (appProperties.getFileClientsDir());
        } else if (operationChat) baseDir = appProperties.getFileConversationDir();
        else baseDir = appProperties.getUploadDir();

        baseDir = baseDir.concat("%s").concat(dto.getTypePj());
        if (operationClient) {
            baseDir = String.format(baseDir,dto.getIdSourcePj()+"/");
        } else if (operationChat && Objects.nonNull(dto.getIdConversation()))
                baseDir = String.format(baseDir, dto.getIdConversation() + "/" + dto.getIdSourcePj() + "/");
            else baseDir = String.format(baseDir, "");

        Path resourcePath = Paths.get(appProperties.getResourcesDir()+baseDir);
        String ressourceDir = resourcePath.toFile().getAbsolutePath()+"/";
        Path dataPath = Paths.get(ressourceDir);
        if (Files.notExists(dataPath)) {
            Files.createDirectories(dataPath);
        }
        PieceJointe pieceJointeToSaved;
        extension = getFileExtension(Objects.requireNonNull(file.getOriginalFilename()));
        if (dto.getId() > 0) {
            PieceJointe pieceJointe = pieceJointeRepository.findById(dto.getId())
                    .orElseThrow(() -> new NotFoundException("Piece jointe not exist with this ID"));
            originalName = Objects.requireNonNull(file.getOriginalFilename());
            storageName = pieceJointe.getStorageName();
            dto.setChemin(baseDir+ "/"+storageName);
            dto.setExtensionFichier(extension);
            dto.setLibelle(Objects.nonNull(dto.getLibelle()) ? dto.getLibelle() : file.getOriginalFilename());
            dto.setStorageName(storageName);
            pieceJointeToSaved = PieceJointeModel.buildToUpdate(pieceJointe, dto, originalName);
        } else {
            originalName = Objects.requireNonNull(file.getOriginalFilename());
            storageName = UUID.randomUUID().toString()+"."+extension;
            dto.setChemin(baseDir+ "/"+storageName);
            dto.setExtensionFichier(extension);
            dto.setLibelle(Objects.nonNull(dto.getLibelle()) ? dto.getLibelle() : file.getOriginalFilename());
            dto.setStorageName(storageName);
            pieceJointeToSaved = PieceJointeModel.buildFromDTO(dto, originalName);
        }
        Files.write(Paths.get(ressourceDir+ "/"+storageName), file.getBytes());

        return pieceJointeRepository.save(pieceJointeToSaved);
    }

    @Override
    public PieceJointe uploadPiecesJointe(long idSourcePj, MultipartFile pieceJointe, TypePieceJointeEnum typePj) {
        if (pieceJointe != null && !pieceJointe.isEmpty()) {
            try {
                return this.ajouterPieceJointe(pieceJointe,
                        PieceJointeModel.builder()
                                .id(0L)
                                .idSourcePj(idSourcePj)
                                .typePj(typePj.name())
                                .datePj(LocalDate.now())
                                .build());
            } catch (IOException e) {
                log.error(e.getMessage());
                throw new NotFoundException(e.getMessage());
            }
        }
        return null;
    }
    @Override
    public PieceJointe uploadPiecesJointe(long idConversation, long idSourcePj, MultipartFile pieceJointe, TypePieceJointeEnum typePj) {
        if (pieceJointe != null && !pieceJointe.isEmpty()) {
            try {
                return this.ajouterPieceJointe(pieceJointe,
                        PieceJointeModel.builder()
                                .id(0L)
                                .idSourcePj(idSourcePj)
                                .idConversation(idConversation)
                                .typePj(typePj.name())
                                .datePj(LocalDate.now())
                                .build());
            } catch (IOException e) {
                log.error(e.getMessage());
                throw new NotFoundException(e.getMessage());
            }
        }
        return null;
    }
    @Override
    public List<PieceJointeModel> findListPieceJointeByTypeEntiteAndId(TypePieceJointeEnum typePieceJointe, Long id) {
        return PieceJointeModel
                .buildFromEntityList(  pieceJointeRepository.findAllByTypeEntiteAndId(typePieceJointe, id));
    }

    @Override
    public List<PieceJointeModel> findListPieceJointeByTypeEntite(TypePieceJointeEnum typePieceJointe) {
        return pieceJointeRepository.findAllByTypeEntite(typePieceJointe).stream()
                .map(PieceJointeModel::buildFromEntity).collect(Collectors.toList());
    }



    @Override
    public PieceJointe findById(Long id) {
        Optional<PieceJointe> pieceJointe = pieceJointeRepository.findById(id);
        if (pieceJointe.isEmpty()) {
            throw new NotFoundException("Piece jointe not exist with this ID");
        }
        return pieceJointe.get();
    }

    @Override
    public void delete( Long id){
        PieceJointe pieceJointe = findById(id);
        try {
            File file = new File(pieceJointe.getChemin());
            if (file.delete()) {
                pieceJointeRepository.delete(pieceJointe);
            }
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }

}
