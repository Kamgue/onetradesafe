package com.example.onetrade.service.impl;

import com.example.onetrade.model.entities.AppClient;
import org.apache.commons.lang.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Locale;
import java.util.Objects;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 29/juin/2024 -- 13:17
 *
 * @author name :  Franky Brice on 29/06/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.service.impl
 **/
@Service
public class MailServiceImpl {
    private final Logger log = LoggerFactory.getLogger(MailServiceImpl.class);
    private static final String BASE_URL = "baseUrl";
    @Value("${app.base_url}")
    private String baseUrlApp;
    @Value("${app.mail.from}")
    private String mailFrom;

    private final MessageSource messageSource;
    private final SpringTemplateEngine templateEngine;

    private final JavaMailSender mailSender;

    public MailServiceImpl(MessageSource messageSource, SpringTemplateEngine templateEngine, JavaMailSender mailSender) {
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
        this.mailSender = mailSender;
    }



    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml, File file) {
        log.debug("Send e-mail[multipart " + isMultipart + " and html " + isHtml + "] to " + to + " with subject " + subject + " and content=" + content);
        // Prepare message using a Spring helper
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
            message.setFrom(new InternetAddress(mailFrom,"Exusvate"));
            message.setTo(to);
            message.setSubject(subject);
            message.setText(content, isHtml);
            if (isMultipart && Objects.nonNull(file)) {
                message.addAttachment(file.getName(), file);
            }
            mailSender.send(mimeMessage);
            log.debug("Sent e-mail to User '{}'", to);
        } catch (Exception e) {
            // e.printStackTrace();
            log.warn("E-mail could not be sent to user '{}', exception is: {}", to, e.getMessage());
        }
    }

    @Async
    public void sendMailAfterCreateAccount(AppClient user, Locale locale) {

        log.debug("Sending creation e-mail to '{}'", user.getEmail());
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("urlActiveAccount", baseUrlApp+"/users/active-account/"+user.getTokenMail());
        String content = templateEngine.process("mails/accountCreationEmail", context);
        String subject = messageSource.getMessage("email.accountCreation.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, null);
    }
    @Async
    public void sendMailAfterProcessForgottenPasswordEmail(AppClient user, String token, Locale locale) {
        log.debug("Sending forgot password e-mail to '{}'", user.getEmail());
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("urlActiveAccount", baseUrlApp+"/users/reset/password/set2/"+token);
        String content = templateEngine.process("mails/accountForgottenPasswordEmail", context);
        String subject = messageSource.getMessage("email.accountForgottenPassword.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, null);
    }
}
