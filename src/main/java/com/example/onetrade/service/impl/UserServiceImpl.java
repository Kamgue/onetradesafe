package com.example.onetrade.service.impl;

import com.example.onetrade.handler.NotFoundException;
import com.example.onetrade.model.binding.EntityResponse;
import com.example.onetrade.model.binding.UpdateClientBindingModel;
import com.example.onetrade.model.binding.response.ClientResponseModel;
import com.example.onetrade.model.entities.*;
import com.example.onetrade.model.entities.enums.*;
import com.example.onetrade.model.repostiory.AppClientRepository;
import com.example.onetrade.model.repostiory.UserRepository;
import com.example.onetrade.model.repostiory.VariableGlobaleRepository;
import com.example.onetrade.model.repostiory.VerificationTokenRepository;
import com.example.onetrade.model.service.SignUpServiceModel;
import com.example.onetrade.service.PieceJointeService;
import com.example.onetrade.service.UserRoleService;
import com.example.onetrade.service.UserService;
import com.example.onetrade.utils.SecurityUtils;
import com.example.onetrade.utils.StringsUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

import static com.example.onetrade.utils.StringsUtils.*;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AppClientRepository appClientRepository;
    private final VerificationTokenRepository verificationTokenRepository;
    private final UserRoleService userRoleService;
    private final VariableGlobaleRepository variableGlobaleRepository;
    private final PieceJointeService pieceJointeService;
    @Autowired
    private MailServiceImpl mailService;


    @Autowired
    public UserServiceImpl(ModelMapper modelMapper, UserRepository userRepository,
                           PasswordEncoder passwordEncoder, AppClientRepository appClientRepository, VerificationTokenRepository verificationTokenRepository,
                           UserRoleService userRoleService, VariableGlobaleRepository variableGlobaleRepository, PieceJointeService pieceJointeService) {
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.appClientRepository = appClientRepository;
        this.verificationTokenRepository = verificationTokenRepository;
        this.userRoleService = userRoleService;
        this.variableGlobaleRepository = variableGlobaleRepository;
        this.pieceJointeService = pieceJointeService;
    }

    @Override
    public List<AppUser> seedUsersAndUserRoles() {
        List<AppUser> seededUsers = new ArrayList<>();

        //simple user
        if (appClientRepository.count() == 0) {
            UserRoleEntity userRoleEntity = new UserRoleEntity();
            userRoleEntity.setRole(UserRoleEnum.USER);
            UserRoleEntity userRole = this.userRoleService.saveRole(userRoleEntity);
            UserRoleEntity userRoleEntity2 = new UserRoleEntity();
            userRoleEntity2.setRole(UserRoleEnum.ADMIN);
            UserRoleEntity adminRole = this.userRoleService.saveRole(userRoleEntity2);
            AppClient user = new AppClient();
            user.setEmail("n13@gmail.com");
            user.setPassword(this.passwordEncoder.encode(SecurityUtils.DEFAULT_PASSWORD));
            user.setRoles(List.of(userRole));
            user.setFullName("Nikoleta Doykova");

            user.setPosition(ClientPositionEnum.EXCLUDING_TRADING);
            user.setState(ClientStateEnum.ACTIVE);
            user.setStatus(ClientStatusEnum.OFFLINE);
            user.setSolde(0);
            // user.setNumCompte(RandomString.make(12));

            //admin
            AppClient admin = new AppClient();
            admin.setEmail("n11@gamil.com");
            admin.setPassword(this.passwordEncoder.encode(SecurityUtils.DEFAULT_PASSWORD));
            admin.setRoles(List.of(adminRole, userRole));
            admin.setFullName("Full name of admin here");

            admin.setPosition(ClientPositionEnum.EXCLUDING_TRADING);
            admin.setState(ClientStateEnum.ACTIVE);
            admin.setStatus(ClientStatusEnum.OFFLINE);
            admin.setSolde(0);
            // admin.setNumCompte(RandomString.make(12));
            appClientRepository.save(user);
            appClientRepository.save(admin);
            seededUsers.add(user);
            seededUsers.add(admin);
        }


        return seededUsers;
    }

    @Override
    public AppClient register(SignUpServiceModel signUpServiceModel) {
        UserRoleEntity userRole = this.userRoleService.getUserRoleByEnumName(UserRoleEnum.USER);
        AppClient appClient = SignUpServiceModel.buildFormEntity(signUpServiceModel, userRole, this.passwordEncoder.encode(signUpServiceModel.getPassword()));

        return appClientRepository.save(appClient);
    }


    @Override
    public AppClient updatedUserClient(UpdateClientBindingModel model) {
        AppClient userToUpdate = findAppClientById(model.getId());
        PieceJointe pjAvatar = pieceJointeService.uploadPiecesJointe(userToUpdate.getId(), model.getAvatar(), TypePieceJointeEnum.AVATAR_USERS);
        PieceJointe pjFrontIdCard = pieceJointeService.uploadPiecesJointe(userToUpdate.getId(), model.getFrontIdCard(), TypePieceJointeEnum.ID_CARD_FRONT);
        PieceJointe pjBackIdCard = pieceJointeService.uploadPiecesJointe(userToUpdate.getId(), model.getBackIdCard(), TypePieceJointeEnum.ID_CARD_BACK);
        PieceJointe pjSelfy = pieceJointeService.uploadPiecesJointe(userToUpdate.getId(), model.getSelfy(), TypePieceJointeEnum.SELFY_USERS);

        if (!userToUpdate.getFullName().equals(model.getFullname()) && this.fullNameExists(model.getFullname())){
            throw new NotFoundException("fullname already exist");
        }
        AppClient clientToUpdate = UpdateClientBindingModel.buildFromUpdateClient(userToUpdate, model, pjAvatar, pjFrontIdCard, pjBackIdCard, pjSelfy);

        // Enregistrer le mot de passe
        if (Objects.nonNull(model.getPassword())) {
            clientToUpdate.setPassword(this.passwordEncoder.encode(model.getPassword()));
        }
        return this.appClientRepository.save(clientToUpdate);
    }
    @Override
    public AppClient saveUpdatedUserClient(AppClient appClient) {
        return this.appClientRepository.save(appClient);
    }
    @Override
    public AppUser saveUpdatedUser(AppUser user) {
        return this.userRepository.save(user);
    }

    @Override
    public AppUser findUserById(Long userId) {
        Optional<AppUser> byId = this.userRepository.findById(userId);

        if (byId.isPresent()) {
            return byId.get();
        } else {
            throw new NotFoundException("User not found");
        }
    }

    @Override
    public AppUser findUserByFullname(String fullname) {
        Optional<AppUser> byFullName = this.userRepository.findByFullName(fullname);

        if (byFullName.isPresent()) {
            return byFullName.get();
        } else {
            throw new NotFoundException("Can not find user with this username");
        }
    }
    @Override
    public EntityResponse<AppClient> findClientByEmail(String email) {
        Optional<AppClient> appClient = this.appClientRepository.findByEmail(email);

        return appClient.map(client -> EntityResponse.buildResponse(true, SUCCESS_MESSAGE, client))
                .orElseGet(() -> EntityResponse.buildResponse(false, "Can not find user with this email", null));
    }

    @Override
    public boolean userExists(String fullname, String email) {
        Optional<AppUser> byUsername = this.userRepository.findByFullName(fullname);
        Optional<AppUser> byEmail = this.userRepository.findByEmail(email);

        return byUsername.isPresent() || byEmail.isPresent();

    }

    @Override
    public void deleteUser(Long id) {
        AppUser user = findUserById(id);
        expireUserSessions();
        userRepository.delete(user);
    }

    @Override
    public void deleteAppClient(Long id) {
        Optional<AppClient> user = this.appClientRepository.findById(id);
        if (user.isPresent()) {
            this.appClientRepository.save(user.get());
            expireUserSessions();

            appClientRepository.delete(user.get());
        } else {
            throw new NotFoundException("Can not find current user");
        }
    }

    @Override
    public AppClient findAppClientById(Long clientId) {
        Optional<AppClient> user = this.appClientRepository.findById(clientId);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new NotFoundException("Can not find user by ID "+clientId);
        }
    }
    @Override
    public AppClient changeStateClient(Long clientId, String state) {
        AppClient client = this.findAppClientById(clientId);
        client.setState(ClientStateEnum.valueOf(state));
        return appClientRepository.save(client);
    }

    @Override
    public void expireUserSessions() {
        SecurityContextHolder.clearContext();
    }

    @Override
    public AppClient findCurrentUserAppClient() {
        Optional<AppClient> user = this.appClientRepository.findByEmail(SecurityUtils.findCurrentEmailUser());
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new NotFoundException("Can not find current user");
        }
    }
    @Override
    public EntityResponse<AppClient> activeAccount(String tokenMail) {
        Optional<AppClient> appClient = this.appClientRepository.findByTokenMail(tokenMail);

        if (appClient.isPresent()) {

            if (!appClient.get().getState().equals(ClientStateEnum.WAITING_FOR_EMAIL_VERIFICATION)){
                return EntityResponse.buildResponse(false, OPERATION_NOT_SUPPORTED,null);
            }

            if (appClient.get().getTokenDeliveryDate().plusMinutes(Integer.parseInt(getValueGlobalVariable(KEY_EXPIRATION_DATE_TOKEN))).isBefore(LocalDateTime.now())){
                return EntityResponse.buildResponse(false, TOKEN_EMAIL_EXPIRED,null);
            }

            appClient.get().setState(ClientStateEnum.WAITING_FOR_ADDITIONAL_INFORMATION);
            appClientRepository.save(appClient.get());
            return EntityResponse.buildResponse(true, SUCCESS_MESSAGE, appClient.get());
        } else {
            return EntityResponse.buildResponse(false, UNRECOGNIZED_EMAIL_TOKEN,null);
        }
    }


    @Override
    public boolean emailExists(String email) {
        Optional<AppUser> byEmail = this.userRepository.findByEmail(email);

        return byEmail.isPresent();
    }
    @Override
    public boolean fullNameExists(String fullName) {
        Optional<AppUser> user = this.userRepository.findByFullName(fullName);

        return user.isPresent();
    }


    @Override
    public EntityResponse<String> sendMailAfterCreateAccount(AppClient appClient){
        if (Objects.isNull(appClient.getTokenMail()) || Objects.isNull(appClient.getTokenDeliveryDate())){
            return new EntityResponse<>(false,UNABLE_TO_SEND_E_MAIL_INFORMATION_INCOMPLETE, null,new Date());
        }

        // generation d'un nouveau token
        appClient.setTokenMail(UUID.randomUUID().toString());
        appClient.setTokenDeliveryDate(LocalDateTime.now());
        AppClient appClientUpdate = appClientRepository.save(appClient);

        mailService.sendMailAfterCreateAccount(appClientUpdate, Locale.forLanguageTag("en"));
        return new EntityResponse<>(true,SUCCESS_MESSAGE, "OK",new Date());

    }
    //@Override
    public EntityResponse<String> sendMailTokenResetPassword(AppClient appClient){
        // generation d'un nouveau token
        appClient.setTokenMail(UUID.randomUUID().toString());
        appClient.setTokenDeliveryDate(LocalDateTime.now());
        AppClient appClientUpdate = appClientRepository.save(appClient);

        mailService.sendMailAfterCreateAccount(appClientUpdate, Locale.forLanguageTag("en"));
        return new EntityResponse<>(true,SUCCESS_MESSAGE, "OK",new Date());

    }
    @Transactional
    @Override
    public void createForgottenPasswordTokenForUser(AppUser user, String token) {
        final VerificationToken myToken = new VerificationToken(token, 1, user);
        verificationTokenRepository.save(myToken);
    }
    @Override
    public String validateForgottenPasswordToken(String token) {
        final VerificationToken verificationToken = verificationTokenRepository.findByTokenAndType(token, 1).orElse(null);
        if (verificationToken == null) {
            return StringsUtils.TOKEN_INVALID;
        }
        final Calendar calendar = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - calendar.getTime().getTime()) < 0) {
            verificationTokenRepository.delete(verificationToken);
            return StringsUtils.TOKEN_EXPIRED;
        }
        return StringsUtils.TOKEN_VALID;
    }
    @Override
    public AppUser getUserFromForgottenPasswordToken(String forgottenPasswordToken) {
        VerificationToken token = verificationTokenRepository.findByTokenAndType(forgottenPasswordToken, 1)
                .orElse(null);
        if (token != null) {
            verificationTokenRepository.delete(token);
            return token.getUser();
        }
        return null;
    }
    public String getValueGlobalVariable(String key){
        Optional<VariableGlobale> variableGlobale = variableGlobaleRepository.findByKey(key);
        if (variableGlobale.isPresent()) {
            return variableGlobale.get().getValue();
        } else {
            throw new NotFoundException(CAN_NOT_FIND_CONFIG_GLOBAL_VARIABLE_WITH_KEY+key);
        }
    }

    @Override
    public Page<ClientResponseModel> findAllClients(String token, Pageable pageable) {
        return ClientResponseModel.buildFromEntityPage(appClientRepository.findAllClientByToken("%"+token+"%", pageable));
    }
    @Override
    public Page<ClientResponseModel> findAllClientsState(ClientStateEnum states, Pageable pageable) {
        return ClientResponseModel.buildFromEntityPage(appClientRepository.findAllClientByState(states, pageable));
    }
    @Override
    public List<ClientResponseModel> findAllClients() {
        return ClientResponseModel.buildFromEntityList(appClientRepository.findAll());
    }
}
