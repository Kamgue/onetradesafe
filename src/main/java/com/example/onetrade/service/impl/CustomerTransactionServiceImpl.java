package com.example.onetrade.service.impl;

import com.example.onetrade.handler.NotFoundException;
import com.example.onetrade.model.binding.EntityResponse;
import com.example.onetrade.model.binding.request.CustomerTransactionModelResquest;
import com.example.onetrade.model.binding.request.QuickRechargeModel;
import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.CustomerTransaction;
import com.example.onetrade.model.entities.enums.ClientStateEnum;
import com.example.onetrade.model.entities.enums.TransactionOperationStateEnum;
import com.example.onetrade.model.entities.enums.TransactionTypeEnum;
import com.example.onetrade.model.repostiory.CustomerTransactionRepository;
import com.example.onetrade.service.CustomerTransactionService;
import com.example.onetrade.service.UserService;
import com.example.onetrade.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static com.example.onetrade.utils.StringsUtils.SUCCESS_MESSAGE;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 14/juillet/2024 -- 21:42
 *
 * @author name :  Franky Brice on 14/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.service.impl
 **/
@Service
@Slf4j
public class CustomerTransactionServiceImpl implements CustomerTransactionService {

    private final CustomerTransactionRepository customerTransactionRepository;
    private final UserService userService;

    public CustomerTransactionServiceImpl(CustomerTransactionRepository customerTransactionRepository, UserService userService) {
        this.customerTransactionRepository = customerTransactionRepository;
        this.userService = userService;
    }


    @Override
    public EntityResponse<CustomerTransaction> addCustomerTransaction(CustomerTransactionModelResquest model) {
        AppClient client = userService.findAppClientById(model.getIdClient());

        if (!client.getState().equals(ClientStateEnum.ACTIVE))
            return EntityResponse.buildResponse(false, "le compte selection ne peut pas effectuer de transaction", null);

        if ( model.getType().equals(TransactionTypeEnum.WITHDRAW) && client.getSolde() < model.getMontant())
            return EntityResponse.buildResponse(false, "solde insufissant dans le compte", null);


        TransactionOperationStateEnum state = SecurityUtils.isAdmin() ? TransactionOperationStateEnum.VALIDATE : TransactionOperationStateEnum.PENDING;

        CustomerTransaction transaction = customerTransactionRepository.save(CustomerTransactionModelResquest.buildFormEntity(model, client, state));
        // mettre a jours le solde su compte du client
        if (transaction.getState().equals(TransactionOperationStateEnum.VALIDATE)){
            double variation = transaction.getType().equals(TransactionTypeEnum.DEPOSIT) ? transaction.getMontant() : -transaction.getMontant();
            client.setSolde( client.getSolde()+variation);
            userService.saveUpdatedUserClient(client);
        }
        return EntityResponse.buildResponse(true, SUCCESS_MESSAGE, transaction);
    }

    @Override
    public CustomerTransaction getCustomerTransaction(Long id) {
        return customerTransactionRepository.findById(id).orElseThrow(()->new NotFoundException("Transaction Not found with ID "+id));
    }

    @Override
    public Page<CustomerTransaction> getAllCustomerTransaction(long idClient, String token, Pageable pageable) {

        if (idClient>0) {
            AppClient client = userService.findAppClientById(idClient);
            return customerTransactionRepository.findCustomerTransactionByClient(client.getId(), "%"+token+"%", pageable);
        }else{

            return customerTransactionRepository.findCustomerTransaction("%"+token+"%", pageable);
        }
    }

    @Override
    public void saveQuickRecharge(QuickRechargeModel quickRechargeModel) {
        addCustomerTransaction(CustomerTransactionModelResquest.builder()
                .montant(quickRechargeModel.getMontant())
                .type(TransactionTypeEnum.DEPOSIT)
                .idClient(userService.findCurrentUserAppClient().getId())
                .motif("Quick recharge")
                .reference(quickRechargeModel.getAddress())
                .build());
    }


}
