package com.example.onetrade.service;

import com.example.onetrade.model.binding.EntityResponse;
import com.example.onetrade.model.binding.UpdateClientBindingModel;
import com.example.onetrade.model.binding.response.ClientResponseModel;
import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.AppUser;
import com.example.onetrade.model.entities.enums.ClientStateEnum;
import com.example.onetrade.model.service.SignUpServiceModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserService {
    List<AppUser> seedUsersAndUserRoles();

    AppClient register(SignUpServiceModel signUpServiceModel);

    AppClient findCurrentUserAppClient();

    EntityResponse<AppClient> activeAccount(String tokenMail);


    boolean fullNameExists(String fullName);

    EntityResponse<String> sendMailAfterCreateAccount(AppClient appClient);

    AppClient updatedUserClient(UpdateClientBindingModel updateClientBindingModel);

    AppClient saveUpdatedUserClient(AppClient appClient);

    AppUser saveUpdatedUser(AppUser user);

    AppUser findUserById(Long userId);

    void deleteUser(Long id);

    AppUser findUserByFullname(String username);

    EntityResponse<AppClient> findClientByEmail(String email);

    boolean userExists(String username, String email);


    void deleteAppClient(Long id);

    AppClient findAppClientById(Long clientId);

    AppClient changeStateClient(Long clientId, String state);

    void expireUserSessions();


    boolean emailExists(String email);

    @Transactional
    void createForgottenPasswordTokenForUser(AppUser user, String token);

    String validateForgottenPasswordToken(String token);

    AppUser getUserFromForgottenPasswordToken(String forgottenPasswordToken);

    Page<ClientResponseModel> findAllClients(String token, Pageable pageable);

    Page<ClientResponseModel> findAllClientsState(ClientStateEnum state, Pageable pageable);

    List<ClientResponseModel> findAllClients();
}


