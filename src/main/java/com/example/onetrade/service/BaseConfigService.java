package com.example.onetrade.service;

import com.example.onetrade.model.binding.request.NetworkModel;
import com.example.onetrade.model.binding.response.NetworkResponseModel;
import com.example.onetrade.model.entities.Network;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/août/2024 -- 05:58
 *
 * @author name :  Franky Brice on 07/08/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.service
 **/
public interface BaseConfigService {
    NetworkResponseModel addOrUpdateNetwork(NetworkModel model);

    Network findNetWorkById(Long id);

    Page<NetworkResponseModel> findAllNetwork(String token, Pageable pageable);

    List<NetworkResponseModel> findAllNetwork();

    void deleteNetwork(long id);
}
