package com.example.onetrade.service;

import com.example.onetrade.model.binding.PieceJointeModel;
import com.example.onetrade.model.entities.PieceJointe;
import com.example.onetrade.model.entities.enums.TypePieceJointeEnum;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/juillet/2024 -- 03:34
 *
 * @author name :  Franky Brice on 07/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.service
 **/
public interface PieceJointeService {
    @Transactional
    PieceJointe ajouterPieceJointe(MultipartFile file, PieceJointeModel dto) throws IOException;

    PieceJointe uploadPiecesJointe(long idSourcePj, MultipartFile pieceJointe, TypePieceJointeEnum typePj);

    PieceJointe uploadPiecesJointe(long idConversation, long idSourcePj, MultipartFile pieceJointe, TypePieceJointeEnum typePj);

    List<PieceJointeModel> findListPieceJointeByTypeEntiteAndId(TypePieceJointeEnum typePieceJointe, Long id);

    List<PieceJointeModel> findListPieceJointeByTypeEntite(TypePieceJointeEnum typePieceJointe);

    PieceJointe findById(Long id);

    void delete(Long id);
}
