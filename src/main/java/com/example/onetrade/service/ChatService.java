package com.example.onetrade.service;

import com.example.onetrade.model.binding.request.MessageModel;
import com.example.onetrade.model.binding.response.ClientResponseModel;
import com.example.onetrade.model.binding.response.ConversationResponseModel;
import com.example.onetrade.model.binding.response.MessageResponseModel;
import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.Conversation;
import com.example.onetrade.model.entities.Message;

import java.util.List;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 19/juillet/2024 -- 06:57
 *
 * @author name :  Franky Brice on 19/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.service
 **/
public interface ChatService {
    Conversation createConversation(AppClient admin);

    List<ConversationResponseModel> findAllConversations();

    List<ClientResponseModel> findClientForNewConversation();

    ConversationResponseModel findOneConversation(Long idConversation);

    Message saveMessage(long idConversation, AppClient sender, AppClient receiver, String content);

    List<Message> findAllMessageUser(long idUser);

    MessageResponseModel sendMessage(MessageModel model);

    ConversationResponseModel findConversationClient(long idClient);

    Conversation addConversation(long idUser);
}
