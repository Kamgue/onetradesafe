package com.example.onetrade.service;

import com.example.onetrade.model.binding.EntityResponse;
import com.example.onetrade.model.binding.request.CustomerTransactionModelResquest;
import com.example.onetrade.model.binding.request.QuickRechargeModel;
import com.example.onetrade.model.entities.CustomerTransaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 14/juillet/2024 -- 21:42
 *
 * @author name :  Franky Brice on 14/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.service
 **/
public interface CustomerTransactionService {
    EntityResponse<CustomerTransaction> addCustomerTransaction(CustomerTransactionModelResquest model);

    CustomerTransaction getCustomerTransaction(Long id);

    Page<CustomerTransaction> getAllCustomerTransaction(long idClient, String token, Pageable pageable);

    void saveQuickRecharge(QuickRechargeModel quickRechargeModel);
}
