package com.example.onetrade.security;

import com.example.onetrade.handler.NotFoundException;
import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.AppUser;
import com.example.onetrade.model.entities.enums.ClientStateEnum;
import com.example.onetrade.model.repostiory.AppClientRepository;
import com.example.onetrade.model.repostiory.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class OntradeUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;
    private final AppClientRepository clientRepository;

    public OntradeUserDetailsService(UserRepository userRepository, AppClientRepository clientRepository) {
        this.userRepository = userRepository;
        this.clientRepository = clientRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        AppUser appUser = userRepository.findByEmail(email).orElseThrow(
                () -> new UsernameNotFoundException("User with email " + email + " was not found."));

        checkAccountClientIfExist(email);

        return mapToUserDetails(appUser);
    }

    private void checkAccountClientIfExist(String email) {
        Optional<AppClient> client = clientRepository.findByEmail(email);
        if (client.isPresent()) {
            if (client.get().getState().equals(ClientStateEnum.WAITING_FOR_EMAIL_VERIFICATION))
                throw new NotFoundException("User account is not activated");
            if (client.get().getState().equals(ClientStateEnum.REJECT))
                throw new NotFoundException("User account is rejeted");
            if (client.get().getState().equals(ClientStateEnum.DEACTIVATE))
                throw new NotFoundException("User account is desabled");
        }
    }

    private UserDetails mapToUserDetails(AppUser appUser) {

        List<GrantedAuthority> authorities = appUser.getRoles().stream()
                .map(ur -> new SimpleGrantedAuthority("ROLE_" + ur.getRole().name())).collect(Collectors.toList());
        return new User(
                appUser.getEmail(),
                appUser.getPassword(),
                authorities
        );
    }
}
