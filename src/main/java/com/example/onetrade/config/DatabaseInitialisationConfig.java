package com.example.onetrade.config;

import com.example.onetrade.model.entities.VariableGlobale;
import com.example.onetrade.model.repostiory.VariableGlobaleRepository;
import com.example.onetrade.utils.StringsUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 06/juillet/2024 -- 14:32
 *
 * @author name :  Franky Brice on 06/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.config
 **/

@Configuration
public class DatabaseInitialisationConfig {

    @Autowired
    private VariableGlobaleRepository variableGlobaleRepository;

    @Bean
    public ApplicationRunner databaseCleaner() {
        return args -> {
            createVariableGlobalIfNotExiste(StringsUtils.KEY_EXPIRATION_DATE_TOKEN,1, "15",
                    "config du delai d''expiration du token d'activation du compte en Jours");
            /*createVariableGlobalIfNotExiste(StringsUtils.KEY_DEFAULT_NUM_COMPTE_PRINCIPAL,1, StringsUtils.VALUE_DEFAULT_NUM_COMPTE_PRINCIPAL,
                    "config du code du compte principal");*/
           };
    }

    private void createVariableGlobalIfNotExiste(String key, int type, String value, String description) {
        if (!variableGlobaleRepository.existsByKey(key))
            variableGlobaleRepository.save(VariableGlobale.VariableGlobaleBuilder.aVariableGlobale()
                    .dataType(type)
                    .key(key)
                    .value(value)
                    .description(description)
                    .build());
    }

}
