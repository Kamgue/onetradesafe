package com.example.onetrade.config;

import com.example.onetrade.websocket.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    private final SocketConnectionHandler socketConnectionHandler;
    private final SocketDataConnectionHandler socketDataConnectionHandler;
    private final SocketDataTradingHandler socketDataTradingHandler;

    private final SocketDataTradingConvertHandler socketDataTradingConvertHandler;

    private final SocketDataConnection2Handler socketDataConnection2Handler;

    private final SocketDataConnectionHomeHandler socketDataConnectionHomeHandler;
    public WebSocketConfig(SocketConnectionHandler socketConnectionHandler, SocketDataConnectionHandler socketDataConnectionHandler, SocketDataTradingHandler socketDataTradingHandler, SocketDataTradingConvertHandler socketDataTradingConvertHandler, SocketDataConnection2Handler socketDataConnection2Handler, SocketDataConnectionHomeHandler socketDataConnectionHomeHandler) {
        this.socketConnectionHandler = socketConnectionHandler;
        this.socketDataConnectionHandler = socketDataConnectionHandler;
        this.socketDataTradingHandler = socketDataTradingHandler;
        this.socketDataTradingConvertHandler = socketDataTradingConvertHandler;
        this.socketDataConnection2Handler = socketDataConnection2Handler;
        this.socketDataConnectionHomeHandler = socketDataConnectionHomeHandler;
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(socketDataConnectionHandler, "/v1/ws")
                .setAllowedOrigins("*");
        registry.addHandler(socketDataConnection2Handler, "/v1/ws2")
                .setAllowedOrigins("*");
        registry.addHandler(socketDataConnectionHomeHandler, "/v1/ws3/home")
                .setAllowedOrigins("*");

        registry.addHandler(socketDataTradingHandler, "/v1/trading")
                .setAllowedOrigins("*");
        registry.addHandler(socketConnectionHandler, "/v1/chat")
                .setAllowedOrigins("*");
    }
}