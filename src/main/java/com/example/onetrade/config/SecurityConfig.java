package com.example.onetrade.config;

import com.example.onetrade.handler.CustomAuthenticationFailureHandler;
import com.example.onetrade.handler.CustomAuthenticationSuccessHandler;
import com.example.onetrade.security.OntradeUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final OntradeUserDetailsService ontradeUserDetailsService;
    private final PasswordEncoder passwordEncoder;
    @Value("${app.base_url}")
    private String baseUrl;

    @Autowired
    public SecurityConfig(OntradeUserDetailsService ontradeUserDetailsService, PasswordEncoder passwordEncoder) {
        this.ontradeUserDetailsService = ontradeUserDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(ontradeUserDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors() // Permet la configuration CORS
                .and()
                .authorizeRequests()
                .antMatchers("/css/**", "/img/**","/files/**", "/piece-jointes/telechager/**","/js/**", "/fonts/**")
                .permitAll()
                .antMatchers("https://exusvate.com","http://exusvate.com","https://www.exusvate.com","exusvate.com/","/","/about-us-site", "/users/login","/users/reset/password/**", "/home", "/users/signup", "/users/active-account/**", "/v1/ws3/home","/v1/**","/site")
                .permitAll()
                .antMatchers("/**").authenticated()
                .and()
                .formLogin().loginPage("/users/login")
                .usernameParameter("email")
                .passwordParameter("password")
                .successHandler(customAuthenticationSuccessHandler())
                .failureHandler(customAuthenticationFailureHandler())
                .and()
                .logout()
                .logoutSuccessUrl(baseUrl+"/home")
                // remove the session from the server
                .invalidateHttpSession(true).
                // delete the session cookie
                        deleteCookies("JSESSIONID");

    }

    @Bean
    public CustomAuthenticationFailureHandler customAuthenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }
    @Bean
    public CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler() {
        return new CustomAuthenticationSuccessHandler();
    }
}
