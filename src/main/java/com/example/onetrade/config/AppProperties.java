package com.example.onetrade.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/juillet/2024 -- 03:44
 *
 * @author name :  Franky Brice on 07/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.config
 **/
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "app")
public class AppProperties {
    private String baseUrl;
    private String baseWs;
    private String uploadDir;
    private String resourcesDir;
    private String fileClientsDir;
    private String fileConversationDir;
    private String reportDir;
}
