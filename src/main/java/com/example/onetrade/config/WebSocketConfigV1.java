package com.example.onetrade.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 01/août/2024 -- 17:48
 *
 * @author name :  Franky Brice on 01/08/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.config
 **/
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfigV1 implements WebSocketMessageBrokerConfigurer {

    @Autowired
    AppProperties appProperties;

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/v1/chat").setAllowedOrigins(appProperties.getBaseUrl()).withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/v1/topic");
//        registry.setApplicationDestinationPrefixes("/app");
    }
    @Override
    public void configureWebSocketTransport(WebSocketTransportRegistration registry) {
        // Augmenter la taille du buffer de messages WebSocket
        registry.setMessageSizeLimit(10485760); // 10 MB, ajustez selon vos besoins
        registry.setSendBufferSizeLimit(10485760); // 10 MB, ajustez selon vos besoins
    }
}
