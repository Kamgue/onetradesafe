package com.example.onetrade.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class TwelveDataConfig {
    public static final String API_KEY = "204124f5c0614f58a86ac8181a9ee64b";
    public static final String API_URL = "https://api.twelvedata.com/quote?symbol={symbol}&apikey={apiKey}";
}
