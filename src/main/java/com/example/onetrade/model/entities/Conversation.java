package com.example.onetrade.model.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 19/juillet/2024 -- 06:46
 *
 * @author name :  Franky Brice on 19/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.entities
 **/
@Getter
@Setter
@Entity
@ToString
@Table(name = "app_conversation")
public class Conversation extends BaseEntity {




    /*@JoinColumn(name = "id_admin_user", foreignKey = @ForeignKey(name = "FK_ADMIN_ON_APP_CONVERSATION"))
    @ManyToOne(targetEntity = AppClient.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private AppClient admin;*/

    @JoinColumn(name = "id_user", foreignKey = @ForeignKey(name = "FK_USER_ON_APP_CONVERSATION"))
    @ManyToOne(targetEntity = AppClient.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private AppClient user;

    @OneToMany(mappedBy = "conversation", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Set<Message> messages;

    public static final class ConversationBuilder {
        private AppClient user;

        private ConversationBuilder() {
        }

        public static ConversationBuilder aConversation() {
            return new ConversationBuilder();
        }

        public ConversationBuilder user(AppClient user) {
            this.user = user;
            return this;
        }

        public Conversation build() {
            Conversation conversation = new Conversation();
            conversation.setUser(user);
            return conversation;
        }
    }
}
