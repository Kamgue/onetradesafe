package com.example.onetrade.model.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/août/2024 -- 06:01
 *
 * @author name :  Franky Brice on 07/08/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.entities
 **/
@Getter
@Setter
@Entity
@ToString
@Table(name = "app_network", indexes = {
        @Index(name = "UK_name_network", columnList = "name", unique = true)
})
public class Network extends BaseEntity {


    @Column(nullable = false)
    private String name;

    private String description;

    public static final class NetworkBuilder {
        private Long id;
        private String name;
        private String description;

        private NetworkBuilder() {
        }

        public static NetworkBuilder aNetwork() {
            return new NetworkBuilder();
        }

        public NetworkBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public NetworkBuilder name(String name) {
            this.name = name;
            return this;
        }

        public NetworkBuilder description(String description) {
            this.description = description;
            return this;
        }

        public Network build() {
            Network network = new Network();
            network.setId(id);
            network.setName(name);
            network.setDescription(description);
            return network;
        }
    }


}
