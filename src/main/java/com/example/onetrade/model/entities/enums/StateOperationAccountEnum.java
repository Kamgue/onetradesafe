package com.example.onetrade.model.entities.enums;

public enum StateOperationAccountEnum {
    WAITING_FOR_EMAIL_VERIFICATION, WAITING_FOR_VALIDATION, WAITING_FOR_ADDITIONAL_INFORMATION, ACTIVE, REJECT, DEACTIVATE;
}
