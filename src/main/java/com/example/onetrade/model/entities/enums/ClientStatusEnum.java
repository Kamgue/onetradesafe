package com.example.onetrade.model.entities.enums;

public enum ClientStatusEnum {
    ONLINE, OFFLINE;
}
