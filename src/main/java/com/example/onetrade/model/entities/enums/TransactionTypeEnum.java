package com.example.onetrade.model.entities.enums;

import lombok.Getter;

@Getter
public enum TransactionTypeEnum {
    DEPOSIT("Deposit"), WITHDRAW("Withdrawal");
    private String value;


    TransactionTypeEnum(String value) {
        this.value = value;
    }
}
