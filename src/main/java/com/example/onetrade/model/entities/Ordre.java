package com.example.onetrade.model.entities;

import com.example.onetrade.model.entities.enums.StateOrderEnum;
import com.example.onetrade.model.entities.enums.TypeOrdreEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "ordres")
public class Ordre extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "utilisateur_id", nullable = false)
    private AppClient utilisateur;

    @Enumerated(EnumType.STRING)
    private TypeOrdreEnum type;

    @ManyToOne
    @JoinColumn(name = "cryptomonnaie_id", nullable = false)
    private Cryptomonnaie cryptomonnaie;

    private double quantite;
    private double prix;
    private double leverage;
    @Column(name = "token", unique = true)
    private String token;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private StateOrderEnum state;

    @Column(name = "date_execution")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime dateExecution;

    @Column(name = "stop_loss")
    private double stopLoss; //  Stop loss

    @Column(name = "take_profit")
    private double takeProfit; // Take profit

    // Getters, setters, constructeurs
}
