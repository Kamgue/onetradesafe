package com.example.onetrade.model.entities;

import lombok.Data;

import java.util.List;

@Data
public class SubscriptionRequest {
    private List<String> symbols;
}
