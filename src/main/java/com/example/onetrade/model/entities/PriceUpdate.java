package com.example.onetrade.model.entities;

import lombok.Data;

@Data
public class PriceUpdate {
    private String symbol;
    private double price;
    private String currency;
    private String currencyBase;
    private String currencyQuote;
    private String type;
    private long timestamp;
}
