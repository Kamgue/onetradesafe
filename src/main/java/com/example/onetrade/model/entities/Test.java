package com.example.onetrade.model.entities;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "test_results")
public class Test extends BaseEntity {
    private AppClient appClient;
    public Test() {
    }

    @OneToOne
    public AppClient getAppClient() {
        return appClient;
    }

    public void setAppClient(AppClient appClient) {
        this.appClient = appClient;
    }
}
