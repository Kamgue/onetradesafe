package com.example.onetrade.model.entities.enums;

public enum UserRoleEnum {
    ADMIN, USER;
}
