package com.example.onetrade.model.entities;

import com.example.onetrade.model.entities.enums.UserRoleEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "roles")
public class UserRoleEntity extends BaseEntity implements Serializable {
    @Column(unique=true)
    private UserRoleEnum role;

    @ManyToMany(mappedBy = "roles")
    private List<AppUser> users = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    public UserRoleEnum getRole() {
        return role;
    }

}
