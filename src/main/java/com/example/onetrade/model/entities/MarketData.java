package com.example.onetrade.model.entities;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
@Data
@Entity
@Table(name = "market_data")
public class MarketData extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "cryptomonnaie_id", nullable = false)
    private Cryptomonnaie cryptomonnaie;

    private double prix;

    @Column(name = "date_heure")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime dateHeure;

    // Getters, setters, constructeurs
}
