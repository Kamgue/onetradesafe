package com.example.onetrade.model.entities.enums;

import lombok.Getter;

@Getter
public enum ClientPositionEnum {
    IN_TRADING(" In trading"),
    EXCLUDING_TRADING("Excluding trading");
    private final String value;

    ClientPositionEnum(String value) {
        this.value = value;
    }
}
