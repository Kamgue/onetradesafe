package com.example.onetrade.model.entities;

import com.example.onetrade.model.entities.enums.TypePieceJointeEnum;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/juillet/2024 -- 03:27
 *
 * @author name :  Franky Brice on 07/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.entities
 **/
@Table(name = "APP_PIECE_JOINTE")
@Entity
@Data
public class PieceJointe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.ORDINAL)
    private TypePieceJointeEnum typePj;
    private LocalDateTime datePj;
    private Long idSourcePj; // on garde ici l'id de l'entite concerne par la pièce jointe
    private String chemin;
    private String extensionFichier;
    private String libelle;
    private String originalName;
    private String storageName;

    public static final class PieceJointeBuilder {
        private Long id;
        private TypePieceJointeEnum typePj;
        private LocalDateTime datePj;
        private Long idSourcePj;
        private String chemin;
        private String extensionFichier;
        private String libelle;
        private String originalName;
        private String storageName;

        private PieceJointeBuilder() {
        }

        public static PieceJointeBuilder aPieceJointe() {
            return new PieceJointeBuilder();
        }

        public PieceJointeBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public PieceJointeBuilder typePj(TypePieceJointeEnum typePj) {
            this.typePj = typePj;
            return this;
        }

        public PieceJointeBuilder datePj(LocalDateTime datePj) {
            this.datePj = datePj;
            return this;
        }

        public PieceJointeBuilder idSourcePj(Long idSourcePj) {
            this.idSourcePj = idSourcePj;
            return this;
        }

        public PieceJointeBuilder chemin(String chemin) {
            this.chemin = chemin;
            return this;
        }

        public PieceJointeBuilder extensionFichier(String extensionFichier) {
            this.extensionFichier = extensionFichier;
            return this;
        }

        public PieceJointeBuilder libelle(String libelle) {
            this.libelle = libelle;
            return this;
        }

        public PieceJointeBuilder originalName(String originalName) {
            this.originalName = originalName;
            return this;
        }

        public PieceJointeBuilder storageName(String storageName) {
            this.storageName = storageName;
            return this;
        }

        public PieceJointe build() {
            PieceJointe pieceJointe = new PieceJointe();
            pieceJointe.setId(id);
            pieceJointe.setTypePj(typePj);
            pieceJointe.setDatePj(datePj);
            pieceJointe.setIdSourcePj(idSourcePj);
            pieceJointe.setChemin(chemin);
            pieceJointe.setExtensionFichier(extensionFichier);
            pieceJointe.setLibelle(libelle);
            pieceJointe.setOriginalName(originalName);
            pieceJointe.setStorageName(storageName);
            return pieceJointe;
        }
    }
}
