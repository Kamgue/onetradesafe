package com.example.onetrade.model.entities;

import com.example.onetrade.model.entities.enums.TransactionOperationStateEnum;
import com.example.onetrade.model.entities.enums.TransactionTypeEnum;
import com.example.onetrade.utils.JsonUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 14/juillet/2024 -- 17:20
 *
 * @author name :  Franky Brice on 14/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.entities
 **/
@Getter
@Setter
@Entity
@ToString
@Table(name = "app_customer_transaction")
public class CustomerTransaction extends BaseEntity {

    @Enumerated(EnumType.STRING)
    private TransactionTypeEnum type;
    @Enumerated(EnumType.STRING)
    private TransactionOperationStateEnum state;

    @JoinColumn(name = "id_client", foreignKey = @ForeignKey(name = "FK_APP_CLIENT_ON_APP_CUSTOMER_TRANSACTION"))
    @ManyToOne(targetEntity = AppClient.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private AppClient client;

    private double montant;
    private String reference;
    private String motif;

    public static final class CustomerTransactionBuilder {
        private TransactionTypeEnum type;
        private TransactionOperationStateEnum state;
        private AppClient client;
        private double montant;
        private String reference;
        private String motif;

        private CustomerTransactionBuilder() {
        }

        public static CustomerTransactionBuilder aCustomerTransaction() {
            return new CustomerTransactionBuilder();
        }

        public CustomerTransactionBuilder type(TransactionTypeEnum type) {
            this.type = type;
            return this;
        }

        public CustomerTransactionBuilder state(TransactionOperationStateEnum state) {
            this.state = state;
            return this;
        }

        public CustomerTransactionBuilder client(AppClient client) {
            this.client = client;
            return this;
        }

        public CustomerTransactionBuilder montant(double montant) {
            this.montant = montant;
            return this;
        }

        public CustomerTransactionBuilder reference(String reference) {
            this.reference = reference;
            return this;
        }

        public CustomerTransactionBuilder motif(String motif) {
            this.motif = motif;
            return this;
        }

        public CustomerTransaction build() {
            CustomerTransaction customerTransaction = new CustomerTransaction();
            customerTransaction.setType(type);
            customerTransaction.setState(state);
            customerTransaction.setClient(client);
            customerTransaction.setMontant(montant);
            customerTransaction.setReference(reference);
            customerTransaction.setMotif(motif);
            return customerTransaction;
        }
    }

    public String toJson(){
        return JsonUtil.getJson(this);
    }
}
