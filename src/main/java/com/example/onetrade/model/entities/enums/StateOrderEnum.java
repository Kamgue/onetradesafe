package com.example.onetrade.model.entities.enums;

public enum StateOrderEnum {
    START("start"),
    IN_PROGRESS("In progress"),
    END("end"),
    PAID("paid");
    private final String value;

    StateOrderEnum(String value) {
        this.value = value;
    }
}
