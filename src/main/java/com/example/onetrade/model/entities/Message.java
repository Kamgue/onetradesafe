package com.example.onetrade.model.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 19/juillet/2024 -- 06:48
 *
 * @author name :  Franky Brice on 19/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.entities
 **/
@Getter
@Setter
@Entity
@ToString
@Table(name = "app_message")
public class Message extends BaseEntity{

    @JoinColumn(name = "id_conversation", foreignKey = @ForeignKey(name = "FK_APP_CONVERSATION_ON_APP_MESSAGE"))
    @ManyToOne(targetEntity = Conversation.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Conversation conversation;

    @JoinColumn(name = "id_sender", foreignKey = @ForeignKey(name = "FK_SENDER_ON_APP_MESSAGE"))
    @ManyToOne(targetEntity = AppClient.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private AppClient sender;

    @JoinColumn(name = "id_receiver", foreignKey = @ForeignKey(name = "FK_RECEIVER_ON_APP_MESSAGE"))
    @ManyToOne(targetEntity = AppClient.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private AppClient receiver;


    @Column(length = 1000)
    private String content;
    private LocalDateTime timestamp;

    public static final class MessageBuilder {
        private Conversation conversation;
        private AppClient sender;
        private AppClient receiver;
        private String content;
        private LocalDateTime timestamp;

        private MessageBuilder() {
        }

        public static MessageBuilder aMessage() {
            return new MessageBuilder();
        }

        public MessageBuilder conversation(Conversation conversation) {
            this.conversation = conversation;
            return this;
        }

        public MessageBuilder sender(AppClient sender) {
            this.sender = sender;
            return this;
        }

        public MessageBuilder receiver(AppClient receiver) {
            this.receiver = receiver;
            return this;
        }

        public MessageBuilder content(String content) {
            this.content = content;
            return this;
        }

        public MessageBuilder timestamp(LocalDateTime timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Message build() {
            Message message = new Message();
            message.setConversation(conversation);
            message.setSender(sender);
            message.setReceiver(receiver);
            message.setContent(content);
            message.setTimestamp(timestamp);
            return message;
        }
    }
}
