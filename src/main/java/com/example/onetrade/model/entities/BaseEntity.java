package com.example.onetrade.model.entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    protected Long id;
    @Version
    protected Long version = 0L;
    @Column(name = "created_at", nullable = false, updatable = false)
    @CreatedDate
    private LocalDateTime createdAt = LocalDateTime.now();
    @Column(name = "updated_at", nullable = false)
    @LastModifiedDate
    private LocalDateTime updatedAt = LocalDateTime.now();
    @CreatedBy
    @Column(nullable = false)
    protected String createdBy = "SYSTEM";
    @LastModifiedBy
    @Column(nullable = false)
    protected String updatedBy = "SYSTEM";


    @Column(name = "last_accessed_by")
    private String lastAccessedBy;

    @Column(name = "last_accessed_at")
    private LocalDateTime lastAccessedAt;

    @Column(name = "created_ip")
    private String createdIp;

    @Column(name = "created_mac_address")
    private String createdMacAddress;

    @Column(name = "created_hostname")
    private String createdHostname;

    @Column(name = "created_country")
    private String createdCountry;

    @Column(name = "updated_ip")
    private String updatedIp;

    @Column(name = "updated_mac_address")
    private String updatedMacAddress;

    @Column(name = "updated_hostname")
    private String updatedHostname;

    @Column(name = "updated_country")
    private String updatedCountry;

    @Column(name = "last_accessed_ip")
    private String lastAccessedIp;

    @Column(name = "last_accessed_mac_address")
    private String lastAccessedMacAddress;

    @Column(name = "last_accessed_hostname")
    private String lastAccessedHostname;

    @Column(name = "last_accessed_country")
    private String lastAccessedCountry;

    @Column(name = "client_hostname")
    private String clientHostname;
    @Column(name = "user_agent")
    private String userAgent;

    @Column(name = "browser_name")
    private String browserName;

    @Column(name = "browser_version")
    private String browserVersion;

    @Column(name = "device_type")
    private String deviceType;

    @Column(name = "operating_system")
    private String operatingSystem;

    @Column(name = "language")
    private String language;

    @Column(name = "client_country")
    private String clientCountry;

    public BaseEntity() {
        this.createdBy = "Default User";
        this.createdAt = LocalDateTime.now();
        this.updatedBy = "Default User";
        this.updatedAt = LocalDateTime.now();
    }

    public void setId(Long id) {
        this.id = id;
    }

    // Getters and setters for id, createdBy, createdAt, updatedBy, updatedAt
    // Getters and setters for lastAccessedBy, lastAccessedAt
    // Getters and setters for createdIp, createdMacAddress, createdHostname, createdCountry
    // Getters and setters for updatedIp, updatedMacAddress, updatedHostname, updatedCountry
    // Getters and setters for lastAccessedIp, lastAccessedMacAddress, lastAccessedHostname, lastAccessedCountry

    // Pre-persist and pre-update methods to set audit fields
    @PrePersist
    public void prePersist() {
        this.createdBy = getCreatedBy();
        this.createdAt = LocalDateTime.now();
        this.createdIp = getCreatedIp();
        this.createdMacAddress = getCreatedMacAddress();
        this.createdHostname = getClientHostname();
        this.createdCountry = getClientCountry();
        this.userAgent = getUserAgent();
        this.browserName = getBrowserName();
        this.browserVersion = getBrowserVersion();
        this.deviceType = getDeviceType();
        this.operatingSystem = getOperatingSystem();
        this.language = getLanguage();
    }

    @PreUpdate
    public void preUpdate() {
        this.updatedBy = getCreatedBy();
        this.updatedAt = LocalDateTime.now();
        this.updatedIp = getCreatedIp();
        this.updatedMacAddress = getCreatedMacAddress();
        this.updatedHostname = getClientHostname();
        this.updatedCountry = getClientCountry();
        this.userAgent = getUserAgent();
    }

    // Methods to get current user, client IP address, MAC address, hostname, and country
    // (You'll need to implement these methods based on your specific application and environment)

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLastAccessedBy() {
        return lastAccessedBy;
    }

    public void setLastAccessedBy(String lastAccessedBy) {
        this.lastAccessedBy = lastAccessedBy;
    }

    public LocalDateTime getLastAccessedAt() {
        return lastAccessedAt;
    }

    public void setLastAccessedAt(LocalDateTime lastAccessedAt) {
        this.lastAccessedAt = lastAccessedAt;
    }

    public String getCreatedIp() {
        return createdIp;
    }

    public void setCreatedIp(String createdIp) {
        this.createdIp = createdIp;
    }

    public String getCreatedMacAddress() {
        return createdMacAddress;
    }

    public void setCreatedMacAddress(String createdMacAddress) {
        this.createdMacAddress = createdMacAddress;
    }

    public String getCreatedHostname() {
        return createdHostname;
    }

    public void setCreatedHostname(String createdHostname) {
        this.createdHostname = createdHostname;
    }

    public String getCreatedCountry() {
        return createdCountry;
    }

    public void setCreatedCountry(String createdCountry) {
        this.createdCountry = createdCountry;
    }

    public String getUpdatedIp() {
        return updatedIp;
    }

    public void setUpdatedIp(String updatedIp) {
        this.updatedIp = updatedIp;
    }

    public String getUpdatedMacAddress() {
        return updatedMacAddress;
    }

    public void setUpdatedMacAddress(String updatedMacAddress) {
        this.updatedMacAddress = updatedMacAddress;
    }

    public String getUpdatedHostname() {
        return updatedHostname;
    }

    public void setUpdatedHostname(String updatedHostname) {
        this.updatedHostname = updatedHostname;
    }

    public String getUpdatedCountry() {
        return updatedCountry;
    }

    public void setUpdatedCountry(String updatedCountry) {
        this.updatedCountry = updatedCountry;
    }

    public String getLastAccessedIp() {
        return lastAccessedIp;
    }

    public void setLastAccessedIp(String lastAccessedIp) {
        this.lastAccessedIp = lastAccessedIp;
    }

    public String getLastAccessedMacAddress() {
        return lastAccessedMacAddress;
    }

    public void setLastAccessedMacAddress(String lastAccessedMacAddress) {
        this.lastAccessedMacAddress = lastAccessedMacAddress;
    }

    public String getLastAccessedHostname() {
        return lastAccessedHostname;
    }

    public void setLastAccessedHostname(String lastAccessedHostname) {
        this.lastAccessedHostname = lastAccessedHostname;
    }

    public String getLastAccessedCountry() {
        return lastAccessedCountry;
    }

    public void setLastAccessedCountry(String lastAccessedCountry) {
        this.lastAccessedCountry = lastAccessedCountry;
    }

    public String getClientHostname() {
        return clientHostname;
    }

    public void setClientHostname(String clientHostname) {
        this.clientHostname = clientHostname;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getBrowserName() {
        return browserName;
    }

    public void setBrowserName(String browserName) {
        this.browserName = browserName;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getClientCountry() {
        return clientCountry;
    }

    public void setClientCountry(String clientCountry) {
        this.clientCountry = clientCountry;
    }

}
