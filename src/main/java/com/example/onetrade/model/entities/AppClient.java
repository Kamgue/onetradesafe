package com.example.onetrade.model.entities;

import com.example.onetrade.model.entities.enums.ClientPositionEnum;
import com.example.onetrade.model.entities.enums.ClientStateEnum;
import com.example.onetrade.model.entities.enums.ClientStatusEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@ToString
@Table(name = "app_clients", indexes = {
        @Index(name = "UK_TOKEN_MAIL", columnList = "token_mail", unique = true)
})
public class AppClient extends AppUser implements Serializable {

    @Enumerated(EnumType.STRING)
    @OneToOne(cascade = CascadeType.REMOVE)
    private Test testResults;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ClientStateEnum state;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ClientStatusEnum status;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ClientPositionEnum position;

    @Column(name = "token_mail", unique = true)
    private String tokenMail;
    @Column(name = "token_delivery_date")
    private LocalDateTime tokenDeliveryDate;

    @Column(name = "token_user")
    private String tokenUser;


    @JoinColumn(name = "id_avatar", foreignKey = @ForeignKey(name = "FK_AVATAR_CLIENT_ON_APP_CLIENT"))
    @ManyToOne(targetEntity = PieceJointe.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private PieceJointe avatar;
    @JoinColumn(name = "id_front_id_card", foreignKey = @ForeignKey(name = "FK_FRONT_ID_CARD_CLIENT_ON_APP_CLIENT"))
    @ManyToOne(targetEntity = PieceJointe.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private PieceJointe frontIdCard;
    @JoinColumn(name = "id_back_id_card", foreignKey = @ForeignKey(name = "FK_BACK_ID_CARD_CLIENT_ON_APP_CLIENT"))
    @ManyToOne(targetEntity = PieceJointe.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private PieceJointe backIdCard;
    @JoinColumn(name = "id_selfy", foreignKey = @ForeignKey(name = "FK_SELFY_CLIENT_ON_APP_CLIENT"))
    @ManyToOne(targetEntity = PieceJointe.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private PieceJointe selfy;
    private double solde;


    public AppClient() {
    }

    public AppClient(String username, String email, List<UserRoleEntity> roles, String password) {
        super(username, email, roles, password);
        this.state = ClientStateEnum.WAITING_FOR_EMAIL_VERIFICATION;
        this.status = ClientStatusEnum.OFFLINE;
        this.position = ClientPositionEnum.EXCLUDING_TRADING;
    }


    public static final class AppClientBuilder {
        private Test testResults;
        private ClientStateEnum state;
        private ClientStatusEnum status;
        private ClientPositionEnum position;
        private double solde;
        private String tokenMail;
        private LocalDateTime tokenDeliveryDate;
        private String tokenUser;
        private PieceJointe avatar;
        private PieceJointe frontIdCard;
        private PieceJointe backIdCard;
        private PieceJointe selfy;
        private String fullName;
        private @Email String email;
        private String password;
        private String numCompte;
        private List<UserRoleEntity> roles;
        private Long id;

        private AppClientBuilder() {
        }

        public static AppClientBuilder anAppClient() {
            return new AppClientBuilder();
        }

        public AppClientBuilder testResults(Test testResults) {
            this.testResults = testResults;
            return this;
        }

        public AppClientBuilder state(ClientStateEnum state) {
            this.state = state;
            return this;
        }

        public AppClientBuilder status(ClientStatusEnum status) {
            this.status = status;
            return this;
        }

        public AppClientBuilder position(ClientPositionEnum position) {
            this.position = position;
            return this;
        }

        public AppClientBuilder solde(double solde) {
            this.solde = solde;
            return this;
        }

        public AppClientBuilder tokenMail(String tokenMail) {
            this.tokenMail = tokenMail;
            return this;
        }

        public AppClientBuilder tokenDeliveryDate(LocalDateTime tokenDeliveryDate) {
            this.tokenDeliveryDate = tokenDeliveryDate;
            return this;
        }

        public AppClientBuilder tokenUser(String tokenUser) {
            this.tokenUser = tokenUser;
            return this;
        }

        public AppClientBuilder avatar(PieceJointe avatar) {
            this.avatar = avatar;
            return this;
        }

        public AppClientBuilder frontIdCard(PieceJointe frontIdCard) {
            this.frontIdCard = frontIdCard;
            return this;
        }

        public AppClientBuilder backIdCard(PieceJointe backIdCard) {
            this.backIdCard = backIdCard;
            return this;
        }
        public AppClientBuilder selfy(PieceJointe selfy) {
            this.selfy = selfy;
            return this;
        }

        public AppClientBuilder fullName(String fullName) {
            this.fullName = fullName;
            return this;
        }

        public AppClientBuilder email(String email) {
            this.email = email;
            return this;
        }

        public AppClientBuilder password(String password) {
            this.password = password;
            return this;
        }
        public AppClientBuilder numCompte(String numCompte) {
            this.numCompte = numCompte;
            return this;
        }

        public AppClientBuilder roles(List<UserRoleEntity> roles) {
            this.roles = roles;
            return this;
        }

        public AppClientBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public AppClient build() {
            AppClient appClient = new AppClient();
            appClient.setTestResults(testResults);
            appClient.setState(state);
            appClient.setStatus(status);
            appClient.setPosition(position);
            appClient.setSolde(solde);
            appClient.setTokenMail(tokenMail);
            appClient.setTokenDeliveryDate(tokenDeliveryDate);
            appClient.setTokenUser(tokenUser);
            appClient.setAvatar(avatar);
            appClient.setFrontIdCard(frontIdCard);
            appClient.setBackIdCard(backIdCard);
            appClient.setSelfy(selfy);
            appClient.setFullName(fullName);
            appClient.setEmail(email);
            appClient.setPassword(password);
            appClient.setRoles(roles);
            appClient.setId(id);
            return appClient;
        }
    }
}
