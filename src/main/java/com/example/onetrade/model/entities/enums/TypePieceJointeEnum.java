package com.example.onetrade.model.entities.enums;

import lombok.Getter;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/juillet/2024 -- 03:29
 *
 * @author name :  Franky Brice on 07/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.entities.enums
 **/
@Getter
public enum TypePieceJointeEnum {
    ALL("Rapport inconnu"),
    AVATAR_USERS("Profile picture"),
    ID_CARD_FRONT("ID card front"),
    ID_CARD_BACK("ID card back"),
    SELFY_USERS("Selfy"),
    CHAT_FILE("chat file"),
    QR_CODE_ADRESS("QR Code Adress"),
    ;

    private final String value;

    TypePieceJointeEnum(String value) {
        this.value = value;
    }
}
