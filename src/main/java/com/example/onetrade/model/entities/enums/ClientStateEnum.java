package com.example.onetrade.model.entities.enums;

import lombok.Getter;

@Getter
public enum ClientStateEnum {
    WAITING_FOR_EMAIL_VERIFICATION("Waiting for email verification"),
    WAITING_FOR_ADDITIONAL_INFORMATION("Waiting for additional information"),
    WAITING_FOR_VALIDATION("Waiting for validation admin"),
    ACTIVE("Active"),
    REJECT("Reject"),
    DEACTIVATE("Deactivate");
    private final String value;

    ClientStateEnum(String value) {
        this.value = value;
    }
}
