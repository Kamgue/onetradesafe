package com.example.onetrade.model.entities.enums;

import lombok.Getter;

@Getter
public enum TransactionOperationStateEnum {
    PENDING("Pending"), VALIDATE("Validate"), REJECT("Reject");
    private String value;
    TransactionOperationStateEnum(String value){
        this.value = value;
    }
}
