package com.example.onetrade.model.entities.enums;

public enum TypeOrdreEnum {
    ACHAT, VENTE;
}
