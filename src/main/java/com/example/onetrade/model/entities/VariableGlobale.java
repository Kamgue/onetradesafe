package com.example.onetrade.model.entities;

import lombok.Data;

import javax.persistence.*;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 06/juillet/2024 -- 12:49
 *
 * @author name :  Franky Brice on 06/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.entities
 **/
@Entity
@Data
@Table(name = "PRM_VARIABLE_GLOBALE",
        indexes = {@Index(columnList = "cle", name = "UK_KEY_GLOBALE_VARIABLE", unique = true)})
public class VariableGlobale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_variable_globale")
    private Long id;

    @Column(name = "cle", nullable = false, length = 50)
    private String key;

    @Column(name = "value", nullable = false, length = 100)
    private String value;


    @Column(name = "description", length = 500)
    private String description;


    @Column(name = "data_type")
    private int dataType; //   (0=Chaine, 1=Numérique, 2=Booleen, 3=Date, 4=Timestamp)

    public static final class VariableGlobaleBuilder {
        private Long id;
        private String key;
        private String value;
        private String description;
        private int dataType;

        private VariableGlobaleBuilder() {
        }

        public static VariableGlobaleBuilder aVariableGlobale() {
            return new VariableGlobaleBuilder();
        }

        public VariableGlobaleBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public VariableGlobaleBuilder key(String key) {
            this.key = key;
            return this;
        }

        public VariableGlobaleBuilder value(String value) {
            this.value = value;
            return this;
        }

        public VariableGlobaleBuilder description(String description) {
            this.description = description;
            return this;
        }

        public VariableGlobaleBuilder dataType(int dataType) {
            this.dataType = dataType;
            return this;
        }

        public VariableGlobale build() {
            VariableGlobale variableGlobale = new VariableGlobale();
            variableGlobale.setId(id);
            variableGlobale.setKey(key);
            variableGlobale.setValue(value);
            variableGlobale.setDescription(description);
            variableGlobale.setDataType(dataType);
            return variableGlobale;
        }
    }
}
