package com.example.onetrade.model.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "trading_config")
public class TradingConfig extends BaseEntity {
    @Column(name = "profit_direction")
    private String profitDirection;

    // Getters and setters
}
