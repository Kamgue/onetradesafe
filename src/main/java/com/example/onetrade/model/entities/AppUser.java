package com.example.onetrade.model.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "users", indexes = {
        @Index(name = "UK_EMAIL_USER", columnList = "email", unique = true),
        @Index(name = "UK_FULL_NAME_USER", columnList = "full_name", unique = true)
})
public class AppUser extends BaseEntity implements Serializable {

    @Column(name = "full_name", nullable = false, unique = true)
    private String fullName;

    @Email
    @Column(name = "email", nullable = false, unique = true)
    private String email;
    @Column(nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<UserRoleEntity> roles = new ArrayList<>();


    public AppUser(String fullname, String email, List<UserRoleEntity> roles, String password) {
        this.fullName = fullname;
        this.email = email;
        this.roles = roles;
        this.password = password;
    }

    public AppUser() {
    }
    public Map<String, Object> toMap() {
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("id", getId());
        map.put("email", this.email);
        map.put("fullName", this.fullName);
        map.put("password", "[PROTECTED]");
        return map;
    }
}
