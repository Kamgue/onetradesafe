package com.example.onetrade.model.entities;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
@Data
@Entity
@Table(name = "transactions")
public class Transaction extends BaseEntity {

    @OneToOne
    @JoinColumn(name = "ordre_id", nullable = false)
    private Ordre ordre;

    @Column(name = "prix_ouverture")
    private double prixOuverture;

    @Column(name = "prix_fermeture")
    private double prixFermeture;

    private double profit;

    @Column(name = "date_ouverture")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime dateOuverture;

    @Column(name = "date_fermeture")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime dateFermeture;

    // Getters, setters, constructeurs
}
