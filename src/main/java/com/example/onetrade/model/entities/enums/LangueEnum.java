package com.example.onetrade.model.entities.enums;

import lombok.Getter;

import java.util.Objects;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 03/août/2024 -- 15:38
 *
 * @author name :  Franky Brice on 03/08/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.entities.enums
 **/
@Getter
public enum LangueEnum {
    AMERICAN_ENGLISH("English", "en", "uk.png"),
   // END("British English ", "EN", "uk.png"),
    FRENCH("Français", "fr", "fr.png");
    private final String value;
    private final String symbole;
    private final String icon;

    LangueEnum(String value, String symbole, String icon) {
        this.value = value;
        this.symbole = symbole;
        this.icon = icon;
    }

    public static LangueEnum forValue(String value) {
        for(final LangueEnum langueEnum: values()) {
            if(Objects.equals(langueEnum.value, value)) {
                return langueEnum;
            }
        }
        throw new IllegalArgumentException("Unknown enum type -> " +value);
    }
}
