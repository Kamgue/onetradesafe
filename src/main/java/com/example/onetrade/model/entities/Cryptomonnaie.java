package com.example.onetrade.model.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "cryptomonnaies")
public class Cryptomonnaie extends BaseEntity {

    @Column(nullable = false)
    private String nom;

    @Column(nullable = false, unique = true)
    private String symbole;

    @Column(name = "prix_actuel")
    private BigDecimal prixActuel;

    @Column(name = "value_adresse")
    private String valueAdresse;
    @Column(name = "exchange")
    private String exchange;
    @Column(name = "micCode")
    private String micCode;
    @Column(name = "currency")
    private String currency;
    @Column(name = "datetimes")
    private  String datetime;
    @Column(name = "timestamps")
    private long timestamp;
    @Column(name = "open")
    private BigDecimal open;
    @Column(name = "high")
    private BigDecimal high;
    @Column(name = "low")
    private BigDecimal low;
    @Column(name = "close")
    private BigDecimal close;
    @Column(name = "volume")
    private Long volume;

    @Column(name = "previous_close")
    private BigDecimal previousClose;
    @Column(name = "changes")
    private BigDecimal change;
    @Column(name = "percent_change")
    private BigDecimal percentChange;
    @Column(name = "average_volume")
    private Long averageVolume;
    @Column(name = "is_market_open")
    private String isMarketOpen;

    @JoinColumn(name = "id_qr_code_adresse", foreignKey = @ForeignKey(name = "FK_QR_CODE_ADRESSE_ON_CRYPTOMONNAIES"))
    @ManyToOne(targetEntity = PieceJointe.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private PieceJointe qrCodeAdresse;

    public static final class CryptomonnaieBuilder {
        private Long id;
        private String nom;
        private String symbole;
        private BigDecimal prixActuel;
        private String valueAdresse;
        private PieceJointe qrCodeAdresse;

        private String exchange;

        private String micCode;

        private String currency;

        private  String datetime;

        private long timestamp;

        private BigDecimal open;

        private BigDecimal high;

        private BigDecimal low;

        private BigDecimal close;

        private Long volume;

        private BigDecimal previousClose;

        private BigDecimal change;

        private BigDecimal percentChange;

        private Long averageVolume;

        private String isMarketOpen;

        private CryptomonnaieBuilder() {
        }

        public static CryptomonnaieBuilder aCryptomonnaie() {
            return new CryptomonnaieBuilder();
        }

        public CryptomonnaieBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public CryptomonnaieBuilder nom(String nom) {
            this.nom = nom;
            return this;
        }

        public CryptomonnaieBuilder symbole(String symbole) {
            this.symbole = symbole;
            return this;
        }

        public CryptomonnaieBuilder prixActuel(BigDecimal prixActuel) {
            this.prixActuel = prixActuel;
            return this;
        }

        public CryptomonnaieBuilder valueAdresse(String valueAdresse) {
            this.valueAdresse = valueAdresse;
            return this;
        }

        public CryptomonnaieBuilder qrCodeAdresse(PieceJointe qrCodeAdresse) {
            this.qrCodeAdresse = qrCodeAdresse;
            return this;
        }

        public CryptomonnaieBuilder exchange(String exchange) {
            this.exchange=exchange;
            return this;
        }
        public CryptomonnaieBuilder micCode(String micCode) {
            this.micCode=micCode;
            return this;
        }
        public CryptomonnaieBuilder currency(String currency) {
            this.currency=currency;
            return this;
        }
        public CryptomonnaieBuilder datetime(String datetime) {
            this.datetime=datetime;
            return this;
        }
        public CryptomonnaieBuilder timestamp(long timestamp) {
            this.timestamp=timestamp;
            return this;
        }
        public CryptomonnaieBuilder open(BigDecimal open) {
            this.open=open;
            return this;
        }
        public CryptomonnaieBuilder high(BigDecimal high) {
            this.high=high;
            return this;
        }
        public CryptomonnaieBuilder low(BigDecimal low) {
            this.low=low;
            return this;
        }
        public CryptomonnaieBuilder close(BigDecimal close) {
            this.close=close;
            return this;
        }
        public CryptomonnaieBuilder volume(Long volume) {
            this.volume=volume;
            return this;
        }
        public CryptomonnaieBuilder previousClose(BigDecimal previousClose) {
            this.previousClose=previousClose;
            return this;
        }
        public CryptomonnaieBuilder change(BigDecimal change) {
            this.change=change;
            return this;
        }
        public CryptomonnaieBuilder percentChange(BigDecimal percentChange) {
            this.percentChange=percentChange;
            return this;
        }
        public CryptomonnaieBuilder averageVolume(Long averageVolume) {
            this.averageVolume=averageVolume;
            return this;
        }
        public CryptomonnaieBuilder isMarketOpen(String isMarketOpen) {
            this.isMarketOpen=isMarketOpen;
            return this;
        }
        public Cryptomonnaie build() {
            Cryptomonnaie cryptomonnaie = new Cryptomonnaie();
            cryptomonnaie.setId(id);
            cryptomonnaie.setNom(nom);
            cryptomonnaie.setSymbole(symbole);
            cryptomonnaie.setPrixActuel(prixActuel);
            cryptomonnaie.setValueAdresse(valueAdresse);
            cryptomonnaie.setQrCodeAdresse(qrCodeAdresse);
            cryptomonnaie.setExchange(exchange);
            cryptomonnaie.setMicCode(micCode);
            cryptomonnaie.setCurrency(currency);
            cryptomonnaie.setDatetime(datetime);
            cryptomonnaie.setTimestamp(timestamp);
            cryptomonnaie.setOpen(open);
            cryptomonnaie.setHigh(high);
            cryptomonnaie.setLow(low);
            cryptomonnaie.setClose(close);
            cryptomonnaie.setVolume(volume);
            cryptomonnaie.setPreviousClose(previousClose);
            cryptomonnaie.setChange(change);
            cryptomonnaie.setPercentChange(percentChange);
            cryptomonnaie.setAverageVolume(averageVolume);
            cryptomonnaie.setIsMarketOpen(isMarketOpen);
            return cryptomonnaie;
        }
    }

}
