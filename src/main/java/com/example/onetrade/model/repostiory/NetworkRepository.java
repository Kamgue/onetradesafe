package com.example.onetrade.model.repostiory;

import com.example.onetrade.model.entities.Network;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/août/2024 -- 06:05
 *
 * @author name :  Franky Brice on 07/08/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.repostiory
 **/
public interface NetworkRepository extends JpaRepository<Network, Long> {

    boolean existsByName(String name);

    @Query("select n from Network n where n.name like :token OR n.description like :token")
    Page<Network> findByToken(String token, Pageable pageable);
}
