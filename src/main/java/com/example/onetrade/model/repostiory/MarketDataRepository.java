package com.example.onetrade.model.repostiory;

import com.example.onetrade.model.entities.Cryptomonnaie;
import com.example.onetrade.model.entities.MarketData;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MarketDataRepository extends JpaRepository<MarketData, Long> {
    List<MarketData> findByCryptomonnaie(Cryptomonnaie cryptomonnaie);

    Slice<MarketData> findByCryptomonnaieOrderByDateHeureDesc(Cryptomonnaie cryptomonnaie, Pageable of);
}
