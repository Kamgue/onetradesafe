package com.example.onetrade.model.repostiory;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.Ordre;
import com.example.onetrade.model.entities.enums.StateOrderEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Ordre, Long> {
    List<Ordre> findByUtilisateur(AppClient utilisateur);
    Optional<Ordre> findByToken(String token);

    List<Ordre> findByUtilisateurAndState(AppClient utilisateur, StateOrderEnum stateOrderEnum);
}
