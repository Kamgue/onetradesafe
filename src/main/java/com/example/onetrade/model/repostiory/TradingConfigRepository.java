package com.example.onetrade.model.repostiory;

import com.example.onetrade.model.entities.TradingConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TradingConfigRepository extends JpaRepository<TradingConfig, Long> {
}
