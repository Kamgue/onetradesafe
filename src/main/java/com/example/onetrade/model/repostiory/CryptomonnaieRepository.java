package com.example.onetrade.model.repostiory;

import com.example.onetrade.model.entities.Cryptomonnaie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CryptomonnaieRepository extends JpaRepository<Cryptomonnaie, Long> {
    Optional<Cryptomonnaie> findBySymbole(String symbole);

    Optional<Cryptomonnaie> findByNom(String nom);
}
