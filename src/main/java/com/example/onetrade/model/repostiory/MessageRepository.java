package com.example.onetrade.model.repostiory;

import com.example.onetrade.model.entities.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 19/juillet/2024 -- 06:56
 *
 * @author name :  Franky Brice on 19/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.repostiory
 **/
public interface MessageRepository extends JpaRepository<Message, Long> {

    @Query("select sms from Message sms " +
            "left join AppClient sender on sender.id = sms.sender.id " +
            "left join AppClient receiv on receiv.id = sms.receiver.id " +
            "where (sender.id = :idUser or receiv.id = :idUser) " +
            "order by sms.createdAt desc ")
    List<Message> findBySenderIdOrReceiverId(Long idUser);
    List<Message> findByConversationIdOrderByCreatedAtAsc(Long idConversation);

}
