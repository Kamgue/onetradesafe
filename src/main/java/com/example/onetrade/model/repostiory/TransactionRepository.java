package com.example.onetrade.model.repostiory;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.Ordre;
import com.example.onetrade.model.entities.Transaction;
import com.example.onetrade.model.entities.enums.StateOrderEnum;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findByOrdreUtilisateur(AppClient utilisateur);

    Optional<Transaction> findByOrdre(Ordre order);

    @Query("select t from Transaction t " +
            " INNER JOIN Ordre o ON t.ordre.id = o.id" +
            " WHERE o.utilisateur.id = :utilisateur_id " +
            " AND o.state = :etatOrdre")
    List<Transaction>  findByOrdreUtilisateurEnd(long utilisateur_id, StateOrderEnum etatOrdre);
}
