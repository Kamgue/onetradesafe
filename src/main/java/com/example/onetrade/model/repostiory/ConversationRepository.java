package com.example.onetrade.model.repostiory;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.Conversation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 19/juillet/2024 -- 06:56
 *
 * @author name :  Franky Brice on 19/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.repostiory
 **/
public interface ConversationRepository extends JpaRepository<Conversation, Long> {

    boolean existsByUserId(long idUser);

    Optional<Conversation> findByUserId(long idUser);
    @Query("select acl from AppClient acl where acl.id not in (select conv.user.id from Conversation conv)")
    List<AppClient> findClientForNewConversation();
}
