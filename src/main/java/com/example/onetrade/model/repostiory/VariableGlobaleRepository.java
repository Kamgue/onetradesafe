package com.example.onetrade.model.repostiory;

import com.example.onetrade.model.entities.VariableGlobale;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 06/juillet/2024 -- 12:52
 *
 * @author name :  Franky Brice on 06/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.repostiory
 **/
public interface VariableGlobaleRepository extends JpaRepository<VariableGlobale, Long> {

    Optional<VariableGlobale> findByKey(String key);

    boolean existsByKey(String key);
}
