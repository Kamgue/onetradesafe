package com.example.onetrade.model.repostiory;


import com.example.onetrade.model.entities.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<AppUser, Long> {
    Optional<AppUser> findByFullName(String fullname);

    Optional<AppUser> findByEmail(String email);

}
