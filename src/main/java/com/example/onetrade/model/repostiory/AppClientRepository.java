package com.example.onetrade.model.repostiory;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.enums.ClientStateEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppClientRepository extends JpaRepository<AppClient, Long> {
    Optional<AppClient> findByFullName(String fullname);
    Optional<AppClient> findByEmail(String email);
    Optional<AppClient> findByTokenMail(String tokenMail);

    @Query("SELECT ac FROM AppClient ac where ac.email like :token or ac.fullName like :token ")
    Page<AppClient> findAllClientByToken(String token, Pageable pageable);

    @Query("SELECT ac FROM AppClient ac where ac.state = :state or ac.status = :state ")
    Page<AppClient> findAllClientByState(ClientStateEnum state, Pageable pageable);
}
