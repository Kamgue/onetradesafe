package com.example.onetrade.model.repostiory;

import com.example.onetrade.model.entities.PieceJointe;
import com.example.onetrade.model.entities.enums.TypePieceJointeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/juillet/2024 -- 03:35
 *
 * @author name :  Franky Brice on 07/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.repostiory
 **/
public interface PieceJointeRepository extends JpaRepository<PieceJointe, Long> {
    @Query("SELECT PJ FROM PieceJointe PJ WHERE PJ.typePj =:typePj AND PJ.idSourcePj =:idType ORDER BY PJ.datePj DESC")
    List<PieceJointe> findAllByTypeEntiteAndId(TypePieceJointeEnum typePj, Long idType);

    @Query("SELECT PJ FROM PieceJointe PJ WHERE PJ.typePj =:typePj ORDER BY PJ.datePj DESC")
    List<PieceJointe> findAllByTypeEntite(@Param("typePj") TypePieceJointeEnum typePj);


}
