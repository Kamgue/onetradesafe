package com.example.onetrade.model.repostiory;

import com.example.onetrade.model.entities.CustomerTransaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 14/juillet/2024 -- 17:38
 *
 * @author name :  Franky Brice on 14/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.repostiory
 **/
public interface CustomerTransactionRepository extends JpaRepository<CustomerTransaction, Long> {

    @Query("select ct from CustomerTransaction ct " +
            " LEFT JOIN AppClient ac ON ac.id = ct.client.id " +
            " WHERE ac.id = :idClient " +
            " AND (concat(ct.state,'') like :token OR concat(ct.type,'') like :token OR concat(ct.montant,'') like :token " +
            " OR ct.reference like :token OR ct.motif like :token)")
    Page<CustomerTransaction> findCustomerTransactionByClient(long idClient, String token, Pageable pageable);
    @Query("select ct from CustomerTransaction ct " +
            " LEFT JOIN AppClient ac ON ac.id = ct.client.id " +
            " WHERE (concat(ct.state,'') like :token OR concat(ct.type,'') like :token OR concat(ct.montant,'') like :token " +
            " OR ct.reference like :token OR ct.motif like :token " +
            " OR ac.fullName like :token OR ac.email like :token)")
    Page<CustomerTransaction> findCustomerTransaction(String token, Pageable pageable);
}
