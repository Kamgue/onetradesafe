package com.example.onetrade.model.service;

import com.example.onetrade.config.TwelveDataConfig;
import com.example.onetrade.model.binding.PriceResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class TwelveDataService {
    private final RestTemplate restTemplate;

    public TwelveDataService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public PriceResponse getPrice(String symbol) {
        //Map<String, String> params = new HashMap<>();
        Map<String, String> params = Map.of("symbol", symbol, "apiKey", TwelveDataConfig.API_KEY);
        //params.put("symbol", symbol);
        //params.put("apikey", TwelveDataConfig.API_KEY);
        return restTemplate.getForObject(TwelveDataConfig.API_URL, PriceResponse.class, params);
    }
}