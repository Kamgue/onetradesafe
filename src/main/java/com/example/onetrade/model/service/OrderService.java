package com.example.onetrade.model.service;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.Ordre;
import com.example.onetrade.model.entities.enums.StateOrderEnum;
import com.example.onetrade.model.repostiory.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Ordre creerOrdre(Ordre ordre) {
        return orderRepository.save(ordre);
    }

    public Ordre modifierOrdre(Ordre ordre) {
        return orderRepository.save(ordre);
    }

    public void supprimerOrdre(Long id) {
        orderRepository.deleteById(id);
    }

    public Optional<Ordre> getOrdre(Long id) {
        return orderRepository.findById(id);
    }

    public List<Ordre> getAllOrdres() {
        return orderRepository.findAll();
    }

    public List<Ordre> getAllOrdresParUtilisateur(AppClient utilisateur) {
        return orderRepository.findByUtilisateur(utilisateur);
    }
    public List<Ordre> getAllOrdresParUtilisateurEtState(AppClient utilisateur, StateOrderEnum stateOrderEnum) {
        return orderRepository.findByUtilisateurAndState(utilisateur,stateOrderEnum);
    }
    public Optional<Ordre> findByToken(String token){
        return orderRepository.findByToken(token);
    }
}
