package com.example.onetrade.model.service;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.UserRoleEntity;
import com.example.onetrade.model.entities.enums.ClientPositionEnum;
import com.example.onetrade.model.entities.enums.ClientStateEnum;
import com.example.onetrade.model.entities.enums.ClientStatusEnum;
import lombok.Getter;
import lombok.Setter;
import net.bytebuddy.utility.RandomString;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class SignUpServiceModel {
    private String fullname;
    private String email;
    private String password;
    private String confirmPassword;

    private ClientStateEnum state;
    private ClientStatusEnum status;
    private ClientPositionEnum position;
    private double solde;

    public SignUpServiceModel() {
    }

    public static AppClient buildFormEntity(SignUpServiceModel model, UserRoleEntity userRole, String encodedPassWord){
        return AppClient.AppClientBuilder.anAppClient()
                .email(model.getEmail())
                .fullName(model.getFullname())
                .state(ClientStateEnum.WAITING_FOR_EMAIL_VERIFICATION)
                .status(ClientStatusEnum.OFFLINE)
                .position(ClientPositionEnum.EXCLUDING_TRADING)
                .solde(0)
                .tokenMail(UUID.randomUUID().toString())
                .tokenDeliveryDate(LocalDateTime.now())
                .roles((List.of(userRole)))
                .password(encodedPassWord)
                .numCompte(RandomString.make(12))
                .build();
    }

}
