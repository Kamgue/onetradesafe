package com.example.onetrade.model.service;

import com.example.onetrade.model.entities.TradingConfig;
import com.example.onetrade.model.repostiory.TradingConfigRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TradingConfigService {

    private final TradingConfigRepository tradingConfigRepository;

    public TradingConfigService(TradingConfigRepository tradingConfigRepository) {
        this.tradingConfigRepository = tradingConfigRepository;
    }

    public TradingConfig getConfig() {
        return tradingConfigRepository.findById(1L)
                .orElseGet(() -> tradingConfigRepository.save(new TradingConfig()));
    }

    public TradingConfig updateConfig(TradingConfig config) {
        Optional<TradingConfig> existingConfig = tradingConfigRepository.findById(1L);

        if (existingConfig.isPresent()) {
            TradingConfig configToUpdate = existingConfig.get();

            // Update desired fields using the null-safe operator
            configToUpdate.setProfitDirection(config.getProfitDirection());

            // Update other fields as needed

            return tradingConfigRepository.save(configToUpdate);
        } else {
            // Handle case where the config doesn't exist (create a new one)
            config.setId(1L);
            return tradingConfigRepository.save(config); // Assuming config has required fields for creation
        }
    }


    public Optional<TradingConfig> findById(long l) {
        return tradingConfigRepository.findById(l);
    }
}
