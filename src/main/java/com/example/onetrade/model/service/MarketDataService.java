package com.example.onetrade.model.service;

import com.example.onetrade.model.entities.Cryptomonnaie;
import com.example.onetrade.model.entities.MarketData;
import com.example.onetrade.model.repostiory.MarketDataRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MarketDataService {
    private final MarketDataRepository marketDataRepository;

    public MarketDataService(MarketDataRepository marketDataRepository) {
        this.marketDataRepository = marketDataRepository;
    }

    public MarketData enregistrerMarketData(MarketData marketData) {
        return marketDataRepository.save(marketData);
    }

    public Optional<MarketData> getMarketData(Long id) {
        return marketDataRepository.findById(id);
    }

    public List<MarketData> getAllMarketData() {
        return marketDataRepository.findAll();
    }

    public List<MarketData> getAllMarketDataParCryptomonnaie(Cryptomonnaie cryptomonnaie) {
        return marketDataRepository.findByCryptomonnaie(cryptomonnaie);
    }

    public void supprimerMarketData(Long id) {
        marketDataRepository.deleteById(id);
    }

    public Slice<MarketData> findByCryptomonnaieOrderByDateHeureDesc(Cryptomonnaie cryptomonnaie, Pageable of) {
        return marketDataRepository.findByCryptomonnaieOrderByDateHeureDesc(cryptomonnaie,of);
    }
}
