package com.example.onetrade.model.service;

import com.example.onetrade.handler.NotFoundException;
import com.example.onetrade.model.binding.request.CryptomonnaieModel;
import com.example.onetrade.model.entities.Cryptomonnaie;
import com.example.onetrade.model.entities.PieceJointe;
import com.example.onetrade.model.entities.enums.TypePieceJointeEnum;
import com.example.onetrade.model.repostiory.CryptomonnaieRepository;
import com.example.onetrade.service.PieceJointeService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CryptomonnaieService {
    private final CryptomonnaieRepository cryptomonnaieRepository;
    private final PieceJointeService pieceJointeService;

    public CryptomonnaieService(CryptomonnaieRepository cryptomonnaieRepository, PieceJointeService pieceJointeService) {
        this.cryptomonnaieRepository = cryptomonnaieRepository;
        this.pieceJointeService = pieceJointeService;
    }

    public Cryptomonnaie creerCryptomonnaie(CryptomonnaieModel cryptomonnaie) {
        Cryptomonnaie cryptomonnaieSave = cryptomonnaieRepository.save(CryptomonnaieModel.buildFromCreate(cryptomonnaie));
        PieceJointe qrCodeAdresse = pieceJointeService.uploadPiecesJointe(cryptomonnaieSave.getId(), cryptomonnaie.getQrCodeAdresse(), TypePieceJointeEnum.QR_CODE_ADRESS);
        cryptomonnaieSave.setQrCodeAdresse(qrCodeAdresse);
        return cryptomonnaieRepository.save(cryptomonnaieSave);
    }

    public Cryptomonnaie modifierCryptomonnaie(CryptomonnaieModel cryptomonnaie) {
        PieceJointe qrCodeAdresse = pieceJointeService.uploadPiecesJointe(cryptomonnaie.getId(), cryptomonnaie.getQrCodeAdresse(), TypePieceJointeEnum.QR_CODE_ADRESS);
        return cryptomonnaieRepository.save(CryptomonnaieModel.buildFromUpdate(getCryptomonnaieByid(cryptomonnaie.getId()), cryptomonnaie, qrCodeAdresse));
    }

    public void supprimerCryptomonnaie(Long id) {
        cryptomonnaieRepository.deleteById(id);
    }

    public Optional<Cryptomonnaie> getCryptomonnaie(Long id) {
        return cryptomonnaieRepository.findById(id);
    }
    public Cryptomonnaie getCryptomonnaieByid(Long id) {
        return cryptomonnaieRepository.findById(id).orElseThrow(()->new NotFoundException("Cryptomannaie not found"));
    }

    public Optional<Cryptomonnaie> getCryptomonnaieParSymbole(String symbole) {
        return cryptomonnaieRepository.findBySymbole(symbole);
    }

    public List<Cryptomonnaie> getAllCryptomonnaies() {
        return cryptomonnaieRepository.findAll();
    }

    public void updatePrixCryptomonnaie(Cryptomonnaie cryptomonnaie, double nouveauPrix) {
        cryptomonnaie.setPrixActuel(BigDecimal.valueOf(nouveauPrix));
        cryptomonnaieRepository.save(cryptomonnaie);
    }
}
