package com.example.onetrade.model.service;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TwelveDataServiceConvert {
    private final String API_KEY = "demo";
    private final String BASE_URL = "https://api.twelvedata.com";

    private final RestTemplate restTemplate;

    public TwelveDataServiceConvert(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getPrice(String symbol) {
        String url = BASE_URL + "/price?symbol=" + symbol + "&apikey=" + API_KEY;
        return restTemplate.getForObject(url, String.class);
    }

    public String getQuote(String symbol) {
        String url = BASE_URL + "/quote?symbol=" + symbol + "&apikey=" + API_KEY;
        return restTemplate.getForObject(url, String.class);
    }

    public String convertCurrency(String symbol, double amount) {
        String url = BASE_URL + "/currency_conversion?symbol=" + symbol + "&amount=" + amount + "&apikey=" + API_KEY;
        return restTemplate.getForObject(url, String.class);
    }
}
