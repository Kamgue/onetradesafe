package com.example.onetrade.model.service;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.Ordre;
import com.example.onetrade.model.entities.Transaction;
import com.example.onetrade.model.entities.enums.StateOrderEnum;
import com.example.onetrade.model.repostiory.TransactionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TransactionService {
    private final TransactionRepository transactionRepository;

    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public Transaction creerTransaction(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    public Optional<Transaction> getTransaction(Long id) {
        return transactionRepository.findById(id);
    }

    public List<Transaction> getAllTransactions() {
        return transactionRepository.findAll();
    }

    public List<Transaction> getAllTransactionsParUtilisateur(AppClient utilisateur) {
        return transactionRepository.findByOrdreUtilisateur(utilisateur);
    }
    public List<Transaction> getAllTransactionsParUtilisateurEtOrdreEnd(long utilisateur_id, StateOrderEnum etatOrdre) {
        return transactionRepository.findByOrdreUtilisateurEnd(utilisateur_id, etatOrdre);
    }

    public void supprimerTransaction(Long id) {
        transactionRepository.deleteById(id);
    }

    public Optional<Transaction> findByOrder(Ordre order) {
        return transactionRepository.findByOrdre(order);
    }
}
