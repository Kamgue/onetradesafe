package com.example.onetrade.model.service;


import com.example.onetrade.model.entities.EmailDetails;

// Interface
public interface EmailService {

    // Method
    // To send a simple email
    String sendSimpleMail(EmailDetails details);

}