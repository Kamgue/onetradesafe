package com.example.onetrade.model.binding;

import com.example.onetrade.model.entities.PieceJointe;
import com.example.onetrade.model.entities.enums.TypePieceJointeEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/juillet/2024 -- 03:37
 *
 * @author name :  Franky Brice on 07/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding
 **/
@Builder
@Getter
@Setter
public class PieceJointeModel {
    // @ApiModelProperty(dataType = "Long", name = "id", value = "Identifiant numérique de la pièce jointe", required = false)
    private Long id;
    // @Schema( type = "string", allowableValues = { "ALL" },description = "Les type de pieces jointes acceptable dans la plateforme" )
    private String typePj;
    // @ApiModelProperty(dataType = "Long", name = "idSourcePj", value = "Identifiant source de la pièce jointe", required = false)
    private Long idSourcePj; // on garde ici l'id de l'entite concerne par la pièce jointe
    // @ApiModelProperty(dataType = "String", name = "chemin", value = "chemin de la pièce jointe", required = false)
    private String chemin;
    // @ApiModelProperty(dataType = "String", name = "extensionFichier", value = "Extension de la pièce jointe", required = false)
    private String extensionFichier;
    @NotBlank(message = "est obligatoire")
    private String libelle;
    private String storageName;
    private String originalName;
    private LocalDate datePj;
    private Long idConversation;
    public static PieceJointe buildFromDTO(PieceJointeModel dto, String originalName) {
        return PieceJointe.PieceJointeBuilder.aPieceJointe()
                .id(dto.getId())
                .typePj(TypePieceJointeEnum.valueOf(dto.getTypePj()))
                .idSourcePj(dto.getIdSourcePj())
                .chemin(dto.getChemin())
                .extensionFichier(dto.getExtensionFichier())
                .libelle(dto.getLibelle())
                .originalName(originalName)
                .storageName(dto.getStorageName())
                .datePj(LocalDateTime.now())
                .build();
    }

    public static PieceJointe buildToUpdate(PieceJointe pieceJointe, PieceJointeModel dto, String originalName) {
        pieceJointe.setTypePj(TypePieceJointeEnum.valueOf(dto.getTypePj()));
        pieceJointe.setDatePj(LocalDateTime.now());
        pieceJointe.setIdSourcePj(dto.getIdSourcePj());
        pieceJointe.setChemin(dto.getChemin());
        pieceJointe.setExtensionFichier(dto.getExtensionFichier());
        pieceJointe.setLibelle(dto.getLibelle());
        pieceJointe.setOriginalName(originalName);
        return pieceJointe;
    }

    public static PieceJointeModel buildFromEntity(PieceJointe entity) {
        return Objects.isNull(entity) ? null : PieceJointeModel.builder()
                .id(entity.getId())
                .idSourcePj(entity.getIdSourcePj())
                .typePj(entity.getTypePj().getValue())
                .datePj(Objects.nonNull(entity.getDatePj()) ? entity.getDatePj().toLocalDate() : null)
                .libelle(entity.getLibelle())
                .originalName(entity.getOriginalName())
                .extensionFichier(entity.getExtensionFichier())
                .build();
    }



    public static List<PieceJointeModel> buildFromEntityList(List<PieceJointe> entityList) {
        return entityList.isEmpty() ? new ArrayList<>() :
                entityList.stream().map(PieceJointeModel::buildFromEntity).collect(Collectors.toList());
    }

    public static Page<PieceJointeModel> buildFromEntityPage(Page<PieceJointe> entityList) {
        return entityList.map(PieceJointeModel::buildFromEntity);
    }
}
