package com.example.onetrade.model.binding;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/juillet/2024 -- 17:36
 *
 * @author name :  Franky Brice on 07/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding
 **/
@Builder
@Getter
@Setter
public class ResetPassWordModel {
    private String token;
    @NotNull
    @Size(min = 3, max = 20, message = " must be between 3 and 20 symbols.")
    private String password;
    @NotNull
    @Size(min = 3, max = 20, message = " must be between 3 and 20 symbols.")
    private String confirmPassword;

    public ResetPassWordModel(String token, String password, String confirmPassword) {
        this.token = token;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }
}
