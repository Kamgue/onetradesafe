package com.example.onetrade.model.binding.response;

import com.example.onetrade.model.entities.Conversation;
import com.example.onetrade.model.entities.Message;
import com.example.onetrade.service.PieceJointeService;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 31/juillet/2024 -- 04:22
 *
 * @author name :  Franky Brice on 31/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding.response
 **/
@Getter
@Setter
@Builder
public class ConversationResponseModel {
    private Long idConversation;
    private ClientResponseModel user;
    private List<MessageResponseModel> messages;
    public static ConversationResponseModel buildFromEntity(Conversation conversation, PieceJointeService pieceJointeService){
        if (Objects.isNull(conversation)) return null;
        return ConversationResponseModel.builder()
                .idConversation(conversation.getId())
                .user(ClientResponseModel.buildFromEntity(conversation.getUser()))
                .messages(MessageResponseModel.buildFromEntityList(new ArrayList<>(conversation.getMessages()), pieceJointeService))
                .build();
    }
    public static ConversationResponseModel buildFromEntity(Conversation conversation, List<Message> messages, PieceJointeService pieceJointeService){
        if (Objects.isNull(conversation)) return null;
        return ConversationResponseModel.builder()
                .idConversation(conversation.getId())
                .user(ClientResponseModel.buildFromEntity(conversation.getUser()))
                .messages(MessageResponseModel.buildFromEntityList(messages, pieceJointeService))
                .build();
    }

    public static List<ConversationResponseModel> buildFromEntityList(List<Conversation> entityList, PieceJointeService pieceJointeService) {
        return (Objects.isNull(entityList) || entityList.isEmpty()) ? new ArrayList<>() :
                entityList.stream().map(item-> ConversationResponseModel.buildFromEntity(item, pieceJointeService)).collect(Collectors.toList());
    }

    public static Page<ConversationResponseModel> buildFromEntityPage(Page<Conversation> entityPage, PieceJointeService pieceJointeService) {
        return (Objects.isNull(entityPage) || entityPage.isEmpty()) ? Page.empty() :
                entityPage.map(item-> ConversationResponseModel.buildFromEntity(item, pieceJointeService));
    }
}
