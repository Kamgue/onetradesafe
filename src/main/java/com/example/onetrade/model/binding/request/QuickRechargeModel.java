package com.example.onetrade.model.binding.request;

import lombok.*;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 08/août/2024 -- 07:39
 *
 * @author name :  Franky Brice on 08/08/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding.request
 **/
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class QuickRechargeModel {
    private double montant;
    private String address;

}
