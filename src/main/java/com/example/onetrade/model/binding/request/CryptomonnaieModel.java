package com.example.onetrade.model.binding.request;

import com.example.onetrade.model.entities.Cryptomonnaie;
import com.example.onetrade.model.entities.PieceJointe;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;

import static com.example.onetrade.utils.StringsUtils.valueExist;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 06/août/2024 -- 01:53
 *
 * @author name :  Franky Brice on 06/08/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding.request
 **/
@Getter
@Setter
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class CryptomonnaieModel {
    private Long id;
    private String nom;
    private String symbole;
    private double prixActuel;
    private String valueAdresse;
    private MultipartFile qrCodeAdresse;
    private long idQrCodeAdresse;
    private String namePjQrCodeAdresse;

    public static Cryptomonnaie buildFromCreate(CryptomonnaieModel model){
        return Cryptomonnaie.CryptomonnaieBuilder.aCryptomonnaie()
                .id(model.getId())
                .nom(model.getNom())
                .symbole(model.getSymbole())
                .prixActuel(BigDecimal.valueOf(model.getPrixActuel()))
                .valueAdresse(model.getValueAdresse())
                .build();
    }
    public static Cryptomonnaie buildFromUpdate(Cryptomonnaie cryptomonnaie, CryptomonnaieModel model, PieceJointe pjQrCodeAdress){
        cryptomonnaie.setNom(model.getNom());
        cryptomonnaie.setSymbole(model.getSymbole());
        cryptomonnaie.setPrixActuel(BigDecimal.valueOf(model.getPrixActuel()));
        cryptomonnaie.setValueAdresse(model.getValueAdresse());
        if(valueExist(pjQrCodeAdress)) cryptomonnaie.setQrCodeAdresse(pjQrCodeAdress);
        return cryptomonnaie;
    }
    public static CryptomonnaieModel buildFromModel(Cryptomonnaie cryptomonnaie){
        return CryptomonnaieModel.builder()
                .id(cryptomonnaie.getId())
                .nom(cryptomonnaie.getNom())
                .symbole(cryptomonnaie.getSymbole())
                .prixActuel(cryptomonnaie.getPrixActuel().doubleValue())
                .valueAdresse(cryptomonnaie.getValueAdresse())
                .idQrCodeAdresse(valueExist(cryptomonnaie.getQrCodeAdresse()) ? cryptomonnaie.getQrCodeAdresse().getId() : 0L)
                .namePjQrCodeAdresse(valueExist(cryptomonnaie.getQrCodeAdresse()) ? cryptomonnaie.getQrCodeAdresse().getOriginalName() : null)
                .build();
    }
}
