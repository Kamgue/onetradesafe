package com.example.onetrade.model.binding.request;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.CustomerTransaction;
import com.example.onetrade.model.entities.enums.TransactionOperationStateEnum;
import com.example.onetrade.model.entities.enums.TransactionTypeEnum;
import lombok.*;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 14/juillet/2024 -- 21:49
 *
 * @author name :  Franky Brice on 14/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding.request
 **/
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class CustomerTransactionModelResquest {
    private TransactionTypeEnum type;
    private long idClient;
    private double montant;
    private String reference;
    private String motif;

    public static CustomerTransaction buildFormEntity(CustomerTransactionModelResquest model, AppClient client, TransactionOperationStateEnum state){
        return CustomerTransaction.CustomerTransactionBuilder.aCustomerTransaction()
                .type(model.getType())
                .state(state)
                .client(client)
                .montant(model.getMontant())
                .reference(model.getReference())
                .motif(model.getMotif())
                .build();
    }
}
