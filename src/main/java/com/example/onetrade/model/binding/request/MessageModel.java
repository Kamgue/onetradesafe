package com.example.onetrade.model.binding.request;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.Conversation;
import com.example.onetrade.model.entities.Message;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 21/juillet/2024 -- 15:07
 *
 * @author name :  Franky Brice on 21/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding.request
 **/
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class MessageModel {
    @JsonProperty("idConversation")
    private long idConversation;
    @JsonProperty("idUserConnected")
    private long idUserConnected;
    @JsonProperty("content")
    private String content;
    private List<FileAttachment> files;
    public static Message buildFormEntity(Conversation conversation, AppClient sender, AppClient receiver, String content){
        return Message.MessageBuilder.aMessage()
                .conversation(conversation)
                .sender(sender)
                .receiver(receiver)
                .content(content)
                .timestamp(LocalDateTime.now())
                .build();
    }
}
