package com.example.onetrade.model.binding;


import com.example.onetrade.model.entities.CustomerTransaction;
import com.example.onetrade.model.entities.Ordre;
import com.example.onetrade.model.entities.Transaction;
import com.example.onetrade.model.entities.enums.StateOrderEnum;
import com.example.onetrade.model.entities.enums.TransactionOperationStateEnum;
import com.example.onetrade.model.entities.enums.TransactionTypeEnum;
import com.example.onetrade.model.repostiory.CustomerTransactionRepository;
import com.example.onetrade.model.service.OrderService;
import com.example.onetrade.model.service.TradingConfigService;
import com.example.onetrade.model.service.TransactionService;
import com.example.onetrade.service.UserService;

import java.time.LocalDateTime;
import java.util.Optional;


public class FinalizationTrading {

    public static void finalizationResult(Ordre order,
                                          TransactionService transactionService,
                                          OrderService orderService,
                                          CustomerTransactionRepository customerTransactionRepository,
                                          UserService userService,TradingConfigService tradingConfigService){
        TradingResults tr=TradingCalculator.calculateTradingValues(order,tradingConfigService);
        // Rechercher l'ordre dans la base de données
        Optional<Transaction> existingTrasaction = transactionService.findByOrder(order);

        if (existingTrasaction.isPresent()) {
            // Utiliser la transaction existante
            Transaction t=existingTrasaction.get();
            //mise à jours des valeurs
            t.setDateFermeture(LocalDateTime.now());
            tr.setDateExecution(LocalDateTime.now());
            tr.setCurrentValue(order.getCryptomonnaie().getPrixActuel().doubleValue());
            t.setPrixFermeture(order.getCryptomonnaie().getPrixActuel().doubleValue());
            t.setProfit(tr.getProfitValue());

            transactionService.creerTransaction(t);
        } else {
            // Créer une nouvelle transaction
            Transaction newTransaction = new Transaction();
            newTransaction.setOrdre(order);
            // Initialiser d'autres champs selon vos besoins
            newTransaction.setPrixOuverture(order.getPrix());
            newTransaction.setDateOuverture(order.getDateExecution());
            // Données a actualiser
            newTransaction.setDateFermeture(LocalDateTime.now());
            newTransaction.setPrixFermeture(order.getCryptomonnaie().getPrixActuel().doubleValue());
            newTransaction.setProfit(tr.getProfitValue());
            tr.setDateExecution(LocalDateTime.now());
            tr.setCurrentValue(order.getCryptomonnaie().getPrixActuel().doubleValue());
            // Enregistrer la nouvelle transaction
            transactionService.creerTransaction(newTransaction);
        }

        double stopLoss = tr.getStopLossValue();
        double takeProfit = tr.getTakeProfitValue();
        double profit = tr.getProfitValue();
        if (order.getCryptomonnaie().getPrixActuel().doubleValue() <= stopLoss || order.getCryptomonnaie().getPrixActuel().doubleValue() >= takeProfit) {
            // ... your code here ...
            order.setState(StateOrderEnum.END);
            orderService.creerOrdre(order);
            // ... Make Deposit or Withdraw
            TransactionOperationStateEnum state = TransactionOperationStateEnum.VALIDATE;

            CustomerTransaction transaction = new CustomerTransaction();
            transaction.setClient(order.getUtilisateur());
            if(profit>0){
                transaction.setType(TransactionTypeEnum.DEPOSIT);
            }else{
                transaction.setType(TransactionTypeEnum.WITHDRAW);
            }

            transaction.setMontant(profit);
            transaction.setReference(order.getToken());
            transaction.setMotif("Trading");
            transaction.setState(state);
            customerTransactionRepository.save(transaction);
            // mettre a jours le solde su compte du client
            if (transaction.getState().equals(TransactionOperationStateEnum.VALIDATE)){
                double variation = transaction.getType().equals(TransactionTypeEnum.DEPOSIT) ? transaction.getMontant() : -transaction.getMontant();
                order.getUtilisateur().setSolde( order.getUtilisateur().getSolde()+variation);
                userService.saveUpdatedUserClient(order.getUtilisateur());
            }
        }
    }
}
