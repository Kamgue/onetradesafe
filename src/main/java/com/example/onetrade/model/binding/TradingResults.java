package com.example.onetrade.model.binding;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TradingResults {
    private double profitValue;
    private double stopLossValue;
    private double takeProfitValue;
    private double marginValue;

    private LocalDateTime dateExecution;

    private double currentValue;

    public TradingResults(double profitValue, double stopLossValue, double takeProfitValue, double marginValue) {
        this.profitValue = profitValue;
        this.stopLossValue = stopLossValue;
        this.takeProfitValue = takeProfitValue;
        this.marginValue = marginValue;
    }

    // Getters
    public double getProfitValue() {
        return profitValue;
    }

    public double getStopLossValue() {
        return stopLossValue;
    }

    public double getTakeProfitValue() {
        return takeProfitValue;
    }

    public double getMarginValue() {
        return marginValue;
    }

    public void setDateExecution(LocalDateTime dateExecution) {
        this.dateExecution = dateExecution;
    }

    public LocalDateTime getDateExecution() {
        return dateExecution;
    }

    public double getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(double currentValue) {
        this.currentValue = currentValue;
    }
}

