package com.example.onetrade.model.binding;

import com.example.onetrade.model.entities.Ordre;
import com.example.onetrade.model.entities.TradingConfig;
import com.example.onetrade.model.entities.Transaction;
import com.example.onetrade.model.entities.enums.TypeOrdreEnum;
import com.example.onetrade.model.service.TradingConfigService;
import com.example.onetrade.model.service.TransactionService;

import java.util.Optional;

public class TradingCalculator {
    private static TransactionService transactionService;
    public static TradingResults calculateTradingValues(Ordre order, TradingConfigService tradingConfigService) {
        double quantite = order.getQuantite();
        double prixOuverture = order.getPrix();
        double prixFermeture = order.getCryptomonnaie().getPrixActuel().doubleValue();
        double leverage = order.getLeverage();
        double stopLoss = order.getStopLoss();
        double takeProfit = order.getTakeProfit();
        double profitValue = 0;

        // Calcul de la valeur de stop loss
        double stopLossValue = stopLoss;

        // Calcul de la valeur de take profit
        double takeProfitValue = takeProfit;

        // Calcul de la marge de variation du profit
        double marginValue = (quantite * leverage) * 0.05; // 5% de la valeur du trade

        double variation = (prixFermeture - prixOuverture) * quantite * leverage;
        //double tmpPrise = variation / marginValue;
        double tmpPrise = variation;

        System.out.println("po :"+prixOuverture);
        System.out.println("pf :"+prixFermeture);
        System.out.println("pr :"+tmpPrise);

        // Transaction
        try {
            Optional<Transaction> t = transactionService.findByOrder(order);
            if (t.isPresent()) {
                //profitValue = tmpPrise + t.get().getProfit();
                profitValue = tmpPrise;
            } else {
                profitValue = tmpPrise;
            }
        } catch (Exception e) {
            profitValue = tmpPrise;
        }

        // Ajuster le profit dans une marge de ±5%
        double ajustedProfit = profitValue * (1 + (Math.random() * 0.1 - 0.05));

        // Récupérer la configuration du trading
        TradingConfig tradingConfig = tradingConfigService.findById(1L)
                .orElseThrow(() -> new IllegalStateException("La configuration du trading n'a pas été trouvée."));

        if (tradingConfig.getProfitDirection().equalsIgnoreCase("positive")) {
            if(order.getType()==TypeOrdreEnum.VENTE){
                ajustedProfit=ajustedProfit*-1;
            }
        }
        if (tradingConfig.getProfitDirection().equalsIgnoreCase("negative")) {
            if(order.getType()==TypeOrdreEnum.ACHAT){
                ajustedProfit=ajustedProfit*-1;
            }
        }

        return new TradingResults(ajustedProfit, stopLossValue, takeProfitValue, marginValue);
    }
}

