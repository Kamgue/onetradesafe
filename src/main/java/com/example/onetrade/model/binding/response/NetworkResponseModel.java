package com.example.onetrade.model.binding.response;

import com.example.onetrade.model.entities.Conversation;
import com.example.onetrade.model.entities.Message;
import com.example.onetrade.model.entities.Network;
import com.example.onetrade.service.PieceJointeService;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/août/2024 -- 06:36
 *
 * @author name :  Franky Brice on 07/08/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding.response
 **/
@Getter
@Setter
@Builder
public class NetworkResponseModel {
    private Long id;
    private String name;
    private String description;
    private LocalDateTime createdAt;
    public static NetworkResponseModel buildFromEntity(Network network){
        if (Objects.isNull(network)) return null;
        return NetworkResponseModel.builder()
                .id(network.getId())
                .name(network.getName())
                .description(network.getDescription())
                .createdAt(network.getCreatedAt())
                .build();
    }

    public static List<NetworkResponseModel> buildFromEntityList(List<Network> entityList) {
        return (Objects.isNull(entityList) || entityList.isEmpty()) ? new ArrayList<>() :
                entityList.stream().map(NetworkResponseModel::buildFromEntity).collect(Collectors.toList());
    }

    public static Page<NetworkResponseModel> buildFromEntityPage(Page<Network> entityPage) {
        return (Objects.isNull(entityPage) || entityPage.isEmpty()) ? Page.empty() :
                entityPage.map(NetworkResponseModel::buildFromEntity);
    }
}
