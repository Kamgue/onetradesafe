package com.example.onetrade.model.binding.response;

import com.example.onetrade.model.binding.PieceJointeModel;
import com.example.onetrade.model.entities.Message;
import com.example.onetrade.model.entities.PieceJointe;
import com.example.onetrade.model.entities.enums.TypePieceJointeEnum;
import com.example.onetrade.service.PieceJointeService;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 31/juillet/2024 -- 04:23
 *
 * @author name :  Franky Brice on 31/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding.response
 **/
@Getter
@Setter
@Builder
public class MessageResponseModel {
    private Long id;
    private LocalDateTime createdAt;
    private String content;
    private LocalDateTime timestamp;
    private ClientResponseModel sender;
    private ClientResponseModel receiver;
    private List<PieceJointeModel> pieceJointes;

    public static MessageResponseModel buildFromEntity(Message message){
        if (Objects.isNull(message)) return null;
        return MessageResponseModel.builder()
                .id(message.getId())
                .content(message.getContent())
                .createdAt(message.getCreatedAt())
                .sender(ClientResponseModel.buildFromEntity(message.getSender()))
                .receiver(ClientResponseModel.buildFromEntity(message.getReceiver()))
                .build();
    }
    public static MessageResponseModel buildFromEntity(Message message, PieceJointeService pieceJointeService){
        if (Objects.isNull(message)) return null;
        return MessageResponseModel.builder()
                .id(message.getId())
                .content(message.getContent())
                .createdAt(message.getCreatedAt())
                .sender(ClientResponseModel.buildFromEntity(message.getSender()))
                .receiver(ClientResponseModel.buildFromEntity(message.getReceiver()))
                .pieceJointes(pieceJointeService.findListPieceJointeByTypeEntiteAndId(TypePieceJointeEnum.CHAT_FILE, message.getId()))
                .build();
    }


    public static List<MessageResponseModel> buildFromEntityList(List<Message> entityList) {
        return (Objects.isNull(entityList) || entityList.isEmpty()) ? new ArrayList<>() :
                entityList.stream().map(MessageResponseModel::buildFromEntity).collect(Collectors.toList());
    }
    public static List<MessageResponseModel> buildFromEntityList(List<Message> entityList, PieceJointeService pieceJointeService) {
        return (Objects.isNull(entityList) || entityList.isEmpty()) ? new ArrayList<>() :
                entityList.stream().map(item-> MessageResponseModel.buildFromEntity(item, pieceJointeService)).collect(Collectors.toList());
    }

    public static Page<MessageResponseModel> buildFromEntityPage(Page<Message> entityPage) {
        return (Objects.isNull(entityPage) || entityPage.isEmpty()) ? Page.empty() :
                entityPage.map(MessageResponseModel::buildFromEntity);
    }
}
