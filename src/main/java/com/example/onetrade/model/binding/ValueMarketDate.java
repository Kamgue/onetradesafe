package com.example.onetrade.model.binding;

import com.example.onetrade.model.entities.MarketData;
import lombok.Data;

import java.util.List;

@Data
public class ValueMarketDate {
    private List<MarketData> recentMarketData;
    private List<MarketData> compaireDefaultMarketData;

    public ValueMarketDate(List<MarketData> recentMarketData, List<MarketData> compaireDefaultMarketData) {
        this.recentMarketData=recentMarketData;
        this.compaireDefaultMarketData=compaireDefaultMarketData;
    }
}
