package com.example.onetrade.model.binding.request;

import lombok.Getter;
import lombok.Setter;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 03/août/2024 -- 07:50
 *
 * @author name :  Franky Brice on 03/08/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding.request
 **/
@Getter
@Setter
public class FileAttachment {
    private String name;
    private String type;
    private String data;
}
