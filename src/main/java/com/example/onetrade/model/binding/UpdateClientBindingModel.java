package com.example.onetrade.model.binding;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.PieceJointe;
import com.example.onetrade.model.entities.enums.ClientStateEnum;
import com.sun.istack.NotNull;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.example.onetrade.utils.StringsUtils.valueExist;

@Builder
@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
public class UpdateClientBindingModel {
    @Size(min = 3, max = 100, message = " must be between 3 and 100 symbols.")
    private String fullname;
    private long id;

    @NotNull
    @Pattern(regexp = ".+@.+\\..+", message = " must be valid.")
    private String email;
    @Size(min = 3, max = 20, message = " must be between 3 and 20 symbols.")
    private String password;
    @Size(min = 3, max = 20, message = " must be between 3 and 20 symbols.")
    private String confirmPassword;

    private MultipartFile avatar;
    private long idPjAvatar;
    private String namePjAvatar;
    private String cheminPjAvatar;
    private ClientStateEnum stateAccount;
    private MultipartFile frontIdCard;
    private long idPjFrontIdCard;
    private String namePjFrontIdCard;
    private String cheminPjFrontIdCard;
    private MultipartFile backIdCard;
    private long idPjBackIdCard;
    private String namePjBackIdCard;
    private String cheminPjBackIdCard;
    private MultipartFile selfy;
    private long idPjSelfy;
    private String namePjSelfy;
    private String cheminPjSelfy;


    public static AppClient buildFromUpdateClient(AppClient client, UpdateClientBindingModel dto, PieceJointe pjAvatar, PieceJointe pjFrontIdCard, PieceJointe pjBackIdCard, PieceJointe pjSelfy){
        client.setFullName(dto.getFullname());
        // client.setEmail(dto.getEmail());
        if(valueExist(pjAvatar)) client.setAvatar(pjAvatar);
        if(valueExist(pjBackIdCard)) client.setBackIdCard(pjBackIdCard);
        if(valueExist(pjFrontIdCard)) client.setFrontIdCard(pjFrontIdCard);
        if(valueExist(pjSelfy)) client.setSelfy(pjSelfy);
        boolean forValidation = (valueExist(pjAvatar) && valueExist(pjBackIdCard) && valueExist(pjFrontIdCard) && valueExist(pjSelfy)) || (dto.getIdPjAvatar()>0 && dto.getIdPjFrontIdCard()>0 && dto.getIdPjBackIdCard()>0 && dto.getIdPjSelfy()>0);
        if (forValidation && client.getState().equals(ClientStateEnum.WAITING_FOR_ADDITIONAL_INFORMATION))
            client.setState(ClientStateEnum.WAITING_FOR_VALIDATION);
        return client;
    }
    public static UpdateClientBindingModel buildFromModel(AppClient client){
        return UpdateClientBindingModel.builder()
                .id(client.getId())
                .fullname(client.getFullName())
                .email(client.getEmail())
                .stateAccount(client.getState())
                .idPjAvatar(valueExist(client.getAvatar()) ? client.getAvatar().getId() : 0L)
                .namePjAvatar(valueExist(client.getAvatar()) ? client.getAvatar().getOriginalName() : null)
                .cheminPjAvatar(valueExist(client.getAvatar()) ? client.getAvatar().getChemin() : null)
                .idPjFrontIdCard(valueExist(client.getFrontIdCard()) ? client.getFrontIdCard().getId() : 0L)
                .namePjFrontIdCard(valueExist(client.getFrontIdCard()) ? client.getFrontIdCard().getOriginalName() : null)
                .cheminPjFrontIdCard(valueExist(client.getFrontIdCard()) ? client.getFrontIdCard().getChemin() : null)
                .idPjBackIdCard(valueExist(client.getBackIdCard()) ? client.getBackIdCard().getId() : 0L)
                .namePjBackIdCard(valueExist(client.getBackIdCard()) ? client.getBackIdCard().getOriginalName() : null)
                .cheminPjBackIdCard(valueExist(client.getBackIdCard()) ? client.getBackIdCard().getChemin() : null)
                .idPjSelfy(valueExist(client.getSelfy()) ? client.getSelfy().getId() : 0L)
                .namePjSelfy(valueExist(client.getSelfy()) ? client.getSelfy().getOriginalName() : null)
                .cheminPjSelfy(valueExist(client.getSelfy()) ? client.getSelfy().getChemin() : null)
                .build();
    }
}
