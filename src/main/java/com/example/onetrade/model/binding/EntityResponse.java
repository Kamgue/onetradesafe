package com.example.onetrade.model.binding;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 06/juillet/2024 -- 13:51
 *
 * @author name :  Franky Brice on 06/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EntityResponse<T> {
    private boolean success;
    private String message;
    private T data;
    private Date date;

    public static <T> EntityResponse<T> buildResponse(boolean success, String message, T data){
        return new EntityResponse<>(success,message,data,new Date());
    }

}
