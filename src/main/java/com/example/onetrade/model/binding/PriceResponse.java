package com.example.onetrade.model.binding;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PriceResponse {
    private String symbol;
    private String name;
    private String exchange;
    private String micCode;
    private String currency;
    private String datetime;
    private long timestamp;
    private BigDecimal open;
    private BigDecimal high;
    private BigDecimal low;
    private BigDecimal close;
    private Long volume;

    private BigDecimal previousClose;
    private BigDecimal change;
    private BigDecimal percentChange;
    private Long averageVolume;
    private String isMarketOpen;
    private FiftyTwoWeek fiftyTwoWeek;

    @Data
    public static class FiftyTwoWeek {
        private BigDecimal low;
        private BigDecimal high;
        private BigDecimal lowChange;
        private BigDecimal highChange;
        private BigDecimal lowChangePercent;
        private BigDecimal highChangePercent;
        private String range;
    }
}