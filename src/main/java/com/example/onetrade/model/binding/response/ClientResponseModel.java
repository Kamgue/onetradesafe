package com.example.onetrade.model.binding.response;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.PieceJointe;
import com.example.onetrade.model.entities.enums.ClientPositionEnum;
import com.example.onetrade.model.entities.enums.ClientStateEnum;
import com.example.onetrade.model.entities.enums.ClientStatusEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 14/juillet/2024 -- 05:13
 *
 * @author name :  Franky Brice on 14/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding.response
 **/
@Getter
@Setter
@ToString
@Builder
public class ClientResponseModel {
    private Long id;
    private ClientStateEnum state;
    private ClientStatusEnum status;
    private ClientPositionEnum position;
    private double solde;
    private PieceJointe avatar;
    private PieceJointe frontIdCard;
    private PieceJointe backIdCard;
    private PieceJointe selfy;
    private String fullName;
    private String shortName;
    private String email;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public static ClientResponseModel buildFromEntity(AppClient client){
        if (Objects.isNull(client)) return null;
        return ClientResponseModel.builder()
                .id(client.getId())
                .state(client.getState())
                .status(client.getStatus())
                .position(client.getPosition())
                .solde(client.getSolde())
                .avatar(client.getAvatar())
                .frontIdCard(client.getFrontIdCard())
                .backIdCard(client.getBackIdCard())
                .selfy(client.getSelfy())
                .fullName(client.getFullName())
                .shortName(client.getFullName().split(" ")[0])
                .email(client.getEmail())
                .createdAt(client.getCreatedAt())
                .updatedAt(client.getUpdatedAt())
                .build();
    }

    public static List<ClientResponseModel> buildFromEntityList(List<AppClient> entityList) {
        return (Objects.isNull(entityList) || entityList.isEmpty()) ? new ArrayList<>() :
                entityList.stream().map(ClientResponseModel::buildFromEntity).collect(Collectors.toList());
    }

    public static Page<ClientResponseModel> buildFromEntityPage(Page<AppClient> entityPage) {
        return (Objects.isNull(entityPage) || entityPage.isEmpty()) ? Page.empty() :
                entityPage.map(ClientResponseModel::buildFromEntity);
    }
}
