package com.example.onetrade.model.binding.request;

import com.example.onetrade.model.entities.Network;
import lombok.*;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/août/2024 -- 06:20
 *
 * @author name :  Franky Brice on 07/08/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.model.binding.request
 **/
@Getter
@Setter
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class NetworkModel {
    private Long id;
    private String name;
    private String description;

    public static Network buildFromCreate(NetworkModel model){
        return Network.NetworkBuilder.aNetwork()
                .name(model.getName())
                .description(model.getDescription())
                .build();
    }
    public static Network buildFromUpdate(Network network, NetworkModel model){
        network.setName(model.getName());
        network.setDescription(model.getDescription());
        return network;
    }
}
