package com.example.onetrade.model.binding;

import com.sun.istack.NotNull;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SignUpBindingModel {
    private String fullname;
    private String email;
    private String password;
    private String confirmPassword;

    public SignUpBindingModel() {
    }

    @Size(min = 3, max = 20, message = " must be between 3 and 20 symbols.")
    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @NotNull
    @Pattern(regexp = ".+@.+\\..+", message = " must be valid.")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Size(min = 3, max = 20, message = " must be between 3 and 20 symbols.")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Size(min = 3, max = 20, message = " must be between 3 and 20 symbols.")
    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
