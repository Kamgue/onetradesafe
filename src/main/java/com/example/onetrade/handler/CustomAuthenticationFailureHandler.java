package com.example.onetrade.handler;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 06/juillet/2024 -- 16:42
 *
 * @author name :  Franky Brice on 06/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.handler
 **/
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {
    @Value("${app.base_url}")
    private String baseUrl;
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        String email = request.getParameter("email");
        String errorMessage = exception.getMessage();
        request.getSession().setAttribute("badCredentials", true);
        request.getSession().setAttribute("email", email);
        request.getSession().setAttribute("errorMessage", errorMessage);
        response.sendRedirect(baseUrl+"/users/login?error");
    }
}
