package com.example.onetrade.web;

import com.example.onetrade.model.entities.TradingConfig;
import com.example.onetrade.model.service.TradingConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin/trading-config")
public class TradingConfigController {

    private final TradingConfigService tradingConfigService;

    public TradingConfigController(TradingConfigService tradingConfigService) {
        this.tradingConfigService = tradingConfigService;
    }

    @GetMapping
    public String showConfigPage(Model model) {
        TradingConfig config = tradingConfigService.getConfig();
        model.addAttribute("config", config);
        return "home/trading-config";
    }

    @PostMapping("/update")
    public String updateConfig(@ModelAttribute TradingConfig config) {
        tradingConfigService.updateConfig(config);
        return "redirect:/admin/trading-config";
    }
}
