package com.example.onetrade.web;

import com.example.onetrade.model.binding.PriceResponse;
import com.example.onetrade.model.binding.request.MessageModel;
import com.example.onetrade.model.binding.response.MessageResponseModel;
import com.example.onetrade.model.entities.Cryptomonnaie;
import com.example.onetrade.model.entities.SubscriptionRequest;
import com.example.onetrade.model.repostiory.CryptomonnaieRepository;
import com.example.onetrade.model.service.TwelveDataService;
import com.example.onetrade.service.ChatService;
import com.example.onetrade.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

@Controller
public class WebSocketController {

    private final SimpMessagingTemplate messagingTemplate;
    private final UserService userService;
    private final ChatService chatService;
    private final CryptomonnaieRepository cryptomonnaieRepository;
    private final TwelveDataService twelveDataService;
    private final Map<String, StompSession> clientSessions = new ConcurrentHashMap<>();

    public WebSocketController(SimpMessagingTemplate messagingTemplate, UserService userService, ChatService chatService, CryptomonnaieRepository cryptomonnaieRepository, TwelveDataService twelveDataService) {
        this.messagingTemplate = messagingTemplate;
        this.userService = userService;
        this.chatService = chatService;
        this.cryptomonnaieRepository = cryptomonnaieRepository;
        this.twelveDataService = twelveDataService;
    }

    @MessageMapping("/v1/subscribe")
    public void subscribe(SubscriptionRequest subscriptionRequest, SimpMessageHeaderAccessor headerAccessor) throws ExecutionException, InterruptedException {
        String sessionId = headerAccessor.getSessionId();
        StompSession session = new WebSocketStompClient(new StandardWebSocketClient()).connect("wss://ws.twelvedata.com/v1/quotes/price?apikey=1f7f96e9f09644e99d47c50851290e18", new StompSessionHandlerAdapter() {}).get();
        clientSessions.put(sessionId, session);

        updatePrices(subscriptionRequest.getSymbols(), session);
    }

    @Scheduled(fixedRate = 5000)
    public void updatePriceData() {
        for (StompSession session : clientSessions.values()) {
            updatePrices(Arrays.asList("AAPL", "INFY", "TRP", "QQQ", "IXIC", "EUR/USD", "USD/JPY", "BTC/USD", "ETH/BTC"), session);
        }
    }

    private void updatePrices(List<String> symbols, StompSession session) {
        for (String symbol : symbols) {
            PriceResponse priceResponse = twelveDataService.getPrice(symbol);
            Cryptomonnaie cryptomonnaie = new Cryptomonnaie();
            cryptomonnaie.setSymbole(priceResponse.getSymbol());
            cryptomonnaie.setPrixActuel(priceResponse.getClose());
            cryptomonnaie.setExchange(priceResponse.getExchange());
            cryptomonnaie.setMicCode(priceResponse.getMicCode());
            cryptomonnaie.setCurrency(priceResponse.getCurrency());
            cryptomonnaie.setDatetime(priceResponse.getDatetime());
            cryptomonnaie.setTimestamp(priceResponse.getTimestamp());
            cryptomonnaie.setOpen(priceResponse.getOpen());
            cryptomonnaie.setHigh(priceResponse.getHigh());
            cryptomonnaie.setLow(priceResponse.getLow());
            cryptomonnaie.setClose(priceResponse.getClose());
            cryptomonnaie.setVolume(priceResponse.getVolume());
            cryptomonnaie.setPreviousClose(priceResponse.getPreviousClose());
            cryptomonnaie.setChange(priceResponse.getChange());
            cryptomonnaie.setPercentChange(priceResponse.getPercentChange());
            cryptomonnaie.setAverageVolume(priceResponse.getAverageVolume());
            cryptomonnaie.setIsMarketOpen(priceResponse.getIsMarketOpen());
            cryptomonnaieRepository.save(cryptomonnaie);

            session.send("/v1/topic/price", priceResponse);
        }
    }
    @MessageMapping("/v1/chat/message")
    @SendTo("/v1/topic/conversation")
    public MessageResponseModel sendMessage(@Payload MessageModel chatMessage) {
        return chatService.sendMessage(chatMessage);
    }



}