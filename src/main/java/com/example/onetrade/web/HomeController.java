package com.example.onetrade.web;

import com.example.onetrade.config.UserInterceptor;
import com.example.onetrade.model.binding.FinalizationTrading;
import com.example.onetrade.model.entities.*;
import com.example.onetrade.model.entities.enums.ClientStateEnum;
import com.example.onetrade.model.entities.enums.StateOrderEnum;
import com.example.onetrade.model.repostiory.CustomerTransactionRepository;
import com.example.onetrade.model.service.*;
import com.example.onetrade.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Controller
//@RequestMapping("")
public class HomeController {
    @Value("${app.base_url}")
    private String baseUrlApp;
    @Value("${app.base_ws}")
    private String baseUrlWs;
    private final UserService userService;

    private final OrderService orderService;
    private final MarketDataService marketService;
    private final TransactionService transactionservice;

    private final CryptomonnaieService cryptomonnaieService;

    private final TradingConfigService tradingConfigService;

    //
    private final TransactionService transactionService;

    private final CustomerTransactionRepository customerTransactionRepository;
    //


    public HomeController(UserService userService, OrderService orderService, MarketDataService marketService, TransactionService transactionservice, CryptomonnaieService cryptomonnaieService, TradingConfigService tradingConfigService, TransactionService transactionService, CustomerTransactionRepository customerTransactionRepository) {
        this.userService = userService;
        this.orderService = orderService;
        this.marketService = marketService;
        this.transactionservice = transactionservice;
        this.cryptomonnaieService = cryptomonnaieService;
        this.tradingConfigService = tradingConfigService;
        this.transactionService = transactionService;
        this.customerTransactionRepository = customerTransactionRepository;
    }
    @Autowired private EmailService emailService;


    //@Autowired
    //public HomeController(UserService userService) {
    //   this.userService = userService;
    //}
    @GetMapping("")
    String showHome1() {
        return "redirect:/site";
    }

    @GetMapping("/")
    String showHome2() {
        return "redirect:/site";
    }

    @GetMapping("/site")
    public ModelAndView showHome3(HttpServletRequest request,@AuthenticationPrincipal UserDetails principal) {
        ModelAndView mav = new ModelAndView("index_site");
        mav.addObject("baseUrlApp", baseUrlWs);
        if (UserInterceptor.isUserLogged()) {
            if (request.isUserInRole("ROLE_ADMIN")) {
                return this.adminShow(principal);
            } else if (request.isUserInRole("ROLE_USER")) {
                //return "redirect:/user_home";
                return mav;
            }
        }
        //return "home/index";
        return mav;
    }

    @GetMapping("/about-us-site")
    public ModelAndView showAboutUs(HttpServletRequest request,@AuthenticationPrincipal UserDetails principal) {
        ModelAndView mav = new ModelAndView("about_us_site");
        //return "home/index";
        return mav;
    }

    @RequestMapping("/home")
    public String showHomeRoot(HttpServletRequest request) {
        if (UserInterceptor.isUserLogged()) {
            AppClient currentUserAppClient = this.userService.findCurrentUserAppClient();
            if(currentUserAppClient.getState().name().equals(ClientStateEnum.ACTIVE.name())) {
                if (request.isUserInRole("ROLE_ADMIN")) {
                    return "redirect:/admin_home";
                } else if (request.isUserInRole("ROLE_USER")) {
                    return "redirect:/site";
                    //return "index_site";
                }
            }else{
                return "redirect:/users/account-info";
            }




            return "redirect:/site";
        }
        //return "home/index";
        return "redirect:/site";
    }

    @RequestMapping("/mail")
    @ResponseBody
    public String sendMailTest() {
        EmailDetails details=new EmailDetails();

        details.setRecipient("kamgueblondin@gmail.com");
        details.setMsgBody("Text");
        details.setSubject("TEST");

        String status = emailService.sendSimpleMail(details);

        return status;
    }

    @GetMapping("/admin_home")
    public ModelAndView adminShow(@AuthenticationPrincipal UserDetails principal) {
        if (UserInterceptor.isUserLogged()) {
            ModelAndView mav = new ModelAndView("home/admin_home");
            mav.addObject("user", principal);
            return mav;
        } else {
            ModelAndView mav = new ModelAndView("index_site");
            mav.addObject("baseUrlApp", baseUrlWs);
            return mav;
        }
    }

    @GetMapping("/site-lever")
    public ModelAndView secondUserTradingShow(@AuthenticationPrincipal UserDetails principal) {
        if (UserInterceptor.isUserLogged()) {
            AppClient user = userService.findCurrentUserAppClient();
            if(!user.getState().equals(ClientStateEnum.ACTIVE)){
                ModelAndView mav = new ModelAndView("index_site");
                mav.addObject("baseUrlApp", baseUrlWs);
                return mav;
            }
            boolean isEmpty = false;
            boolean hasNoResults = false;
            ModelAndView mav = new ModelAndView("lever_site");
            List<Transaction> transactionsInProgress = transactionService.getAllTransactionsParUtilisateurEtOrdreEnd(user.getId(), StateOrderEnum.IN_PROGRESS);
            for (Transaction transaction : transactionsInProgress) {
                FinalizationTrading.finalizationResult(transaction.getOrdre(),transactionService,orderService,customerTransactionRepository,userService,tradingConfigService);
            }

            //List<MarketData> markets = marketService.getAllMarketData();
            List<Ordre> openOrders = orderService.getAllOrdresParUtilisateurEtState(user,StateOrderEnum.IN_PROGRESS);
            List<Cryptomonnaie> cryptomonnaies = cryptomonnaieService.getAllCryptomonnaies();
            //String host = ServletUriComponentsBuilder.fromCurrentContextPath().path("/ws").build().toUri().getHost();
            //int port = ServletUriComponentsBuilder.fromCurrentContextPath().path("/ws").build().toUri().getPort();

            String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUri().toString();
            baseUrl = baseUrl.replaceFirst("^(http|https|ws|wss)://", "");
            TradingConfig config = tradingConfigService.getConfig();
            mav.addObject("config", config);

            mav.addObject("baseUrlApp", baseUrlWs);
            mav.addObject("user", user);
            mav.addObject("markets", null);
            mav.addObject("openOrders", openOrders);
            mav.addObject("order", new Ordre());
            mav.addObject("cryptomonnaies", cryptomonnaies);
            mav.addObject("leverages", new int[]{10, 20, 50, 100});

            AppClient currentUserAppClient = this.userService.findCurrentUserAppClient();
            if (currentUserAppClient.getTestResults() == null) {
                hasNoResults = true;
            }

            mav.addObject("hasNoMatches", isEmpty);
            mav.addObject("hasNoResults", hasNoResults);

            return mav;
        } else {
            ModelAndView mav = new ModelAndView("index_site");
            mav.addObject("baseUrlApp", baseUrlWs);
            return mav;
        }
    }

    @GetMapping("/admin_trading")
    public ModelAndView tradingShow(@AuthenticationPrincipal UserDetails principal) {
        if (UserInterceptor.isUserLogged()) {
            ModelAndView mav = new ModelAndView("home/admin_trading");
            String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUri().toString();
            baseUrl = baseUrl.replaceFirst("^(http|https|ws|wss)://", "");

            mav.addObject("baseUrlApp", baseUrlWs);
            mav.addObject("user", principal);
            return mav;
        } else {
            ModelAndView mav = new ModelAndView("index_site");
            mav.addObject("baseUrlApp", baseUrlWs);
            return mav;
        }
    }

    @GetMapping("/admin_trading_test")
    public ModelAndView tradingShowTest(@AuthenticationPrincipal UserDetails principal) {
        if (UserInterceptor.isUserLogged()) {
            ModelAndView mav = new ModelAndView("home/admin_trading_test");
            mav.addObject("user", principal);
            return mav;
        } else {
            ModelAndView mav = new ModelAndView("index_site");
            mav.addObject("baseUrlApp", baseUrlWs);
            return mav;
        }
    }
    @GetMapping("/config_url")
    public String getConfig() {
        return baseUrlApp;
    }

    @PostMapping("/trading/orders")
    public ModelAndView tradingOrdertest(@AuthenticationPrincipal UserDetails principal) {
        if (UserInterceptor.isUserLogged()) {
            ModelAndView mav = new ModelAndView("home/user_trading_details");
            mav.addObject("user", principal);
            return mav;
        } else {
            ModelAndView mav = new ModelAndView("index_site");
            mav.addObject("baseUrlApp", baseUrlWs);
            return mav;
        }
    }

    @GetMapping("/user_home")
    public ModelAndView userHomeShow(@AuthenticationPrincipal UserDetails principal) {
        if (UserInterceptor.isUserLogged()) {
            boolean isEmpty = false;
            boolean hasNoResults = false;
            ModelAndView mav = new ModelAndView("home/user_home_second");
            AppClient user = userService.findCurrentUserAppClient();
            //List<MarketData> markets = marketService.getAllMarketData();
            List<Ordre> openOrders = orderService.getAllOrdresParUtilisateur(user);
            List<Cryptomonnaie> cryptomonnaies = cryptomonnaieService.getAllCryptomonnaies();
            //String host = ServletUriComponentsBuilder.fromCurrentContextPath().path("/ws").build().toUri().getHost();
            //int port = ServletUriComponentsBuilder.fromCurrentContextPath().path("/ws").build().toUri().getPort();

            String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUri().toString();
            baseUrl = baseUrl.replaceFirst("^(http|https|ws|wss)://", "");
            TradingConfig config = tradingConfigService.getConfig();
            mav.addObject("config", config);

            mav.addObject("baseUrlApp", baseUrlWs);
            mav.addObject("user", user);
            mav.addObject("markets", null);
            mav.addObject("openOrders", openOrders);
            mav.addObject("order", new Ordre());
            mav.addObject("cryptomonnaies", cryptomonnaies);
            mav.addObject("leverages", new int[]{10, 20, 50, 100});

            AppClient currentUserAppClient = this.userService.findCurrentUserAppClient();
            if (currentUserAppClient.getTestResults() == null) {
                hasNoResults = true;
            }

            mav.addObject("hasNoMatches", isEmpty);
            mav.addObject("hasNoResults", hasNoResults);

            return mav;
        } else {
            ModelAndView mav = new ModelAndView("index_site");
            mav.addObject("baseUrlApp", baseUrlWs);
            return mav;
        }
    }

    @GetMapping("trading/{token}")
    public ModelAndView showTradingDetails(@PathVariable String token, @AuthenticationPrincipal UserDetails userDetails) {
        if (UserInterceptor.isUserLogged()) {
            AppClient user = userService.findCurrentUserAppClient();
            if(!user.getState().equals(ClientStateEnum.ACTIVE)){
                ModelAndView mav = new ModelAndView("index_site");
                mav.addObject("baseUrlApp", baseUrlWs);
                return mav;
            }
            Ordre order= orderService.findByToken(token).orElseThrow(() -> new RuntimeException("Pas de commande trouvée pour le token : " + token));

            if (order.getState().equals(StateOrderEnum.IN_PROGRESS)){
                FinalizationTrading.finalizationResult(order,transactionService,orderService,customerTransactionRepository,userService,tradingConfigService);
            }


            ModelAndView mav = new ModelAndView("home/user_trading_details");
            mav.addObject("baseUrlApp", baseUrlWs);
            mav.addObject("order", order);
            return mav;
        } else {
            ModelAndView mav = new ModelAndView("index_site");
            mav.addObject("baseUrlApp", baseUrlWs);
            return mav;
        }
    }
    @PostMapping("/trading/order")
    public String createOrder(@ModelAttribute("order") Ordre ordre,
                              @RequestParam("market") String market,
                              @RequestParam("leverage") double leverage,
                              @RequestParam("amount") double amount,
                              @RequestParam("takeProfit") double takeProfit,
                              @RequestParam("stopLoss") double stopLoss,
            @AuthenticationPrincipal UserDetails userDetails) {
        TradingConfig config = tradingConfigService.getConfig();
        // Récupérez l'utilisateur courant
        AppClient utilisateur = userService.findCurrentUserAppClient();
        ordre.setUtilisateur(utilisateur);

        // Récupérez la cryptomonnaie par son symbole
        Cryptomonnaie cryptomonnaie = cryptomonnaieService.getCryptomonnaieParSymbole(market)
                .orElseThrow(() -> new RuntimeException("Cryptomonnaie non trouvée pour le symbole : " + market));
        ordre.setCryptomonnaie(cryptomonnaie);

        // Remplissez les autres champs
        ordre.setQuantite(amount);
        ordre.setPrix(cryptomonnaie.getPrixActuel().doubleValue());
        ordre.setLeverage(leverage);
        ordre.setStopLoss(stopLoss);
        ordre.setTakeProfit(takeProfit);
        ordre.setDateExecution(LocalDateTime.now()); // ou une autre logique pour la date d'exécution
        ordre.setToken(UUID.randomUUID().toString());
        ordre.setState(StateOrderEnum.IN_PROGRESS);
        // Enregistrez l'ordre
        Ordre o=orderService.creerOrdre(ordre);

        // Redirigez vers l'URL souhaitée avec l'ID de l'ordre
        return "redirect:/trading/" + o.getToken();
    }
    @GetMapping("/market-site")
    public ModelAndView marketSite(@AuthenticationPrincipal UserDetails userDetails){
        if (UserInterceptor.isUserLogged()) {
            AppClient user = userService.findCurrentUserAppClient();
            if(!user.getState().equals(ClientStateEnum.ACTIVE)){
                ModelAndView mav = new ModelAndView("index_site");
                mav.addObject("baseUrlApp", baseUrlWs);
                return mav;
            }
            ModelAndView mav = new ModelAndView("market_site");
            mav.addObject("baseUrlApp", baseUrlWs);
            return mav;
        } else {
            ModelAndView mav = new ModelAndView("index_site");
            mav.addObject("baseUrlApp", baseUrlWs);
            return mav;
        }
    }
    @GetMapping("/exchange-site")
    public ModelAndView showTradingHistory(@AuthenticationPrincipal UserDetails userDetails) {
        if (UserInterceptor.isUserLogged()) {
            AppClient user = userService.findCurrentUserAppClient();
            if(!user.getState().equals(ClientStateEnum.ACTIVE)){
                ModelAndView mav = new ModelAndView("index_site");
                mav.addObject("baseUrlApp", baseUrlWs);
                return mav;
            }

            ModelAndView mav = new ModelAndView("home/user_trading_history");


            //List<Transaction> trades = transactionservice.getAllTransactionsParUtilisateur(user);
            List<Transaction> trades = transactionservice.getAllTransactionsParUtilisateurEtOrdreEnd(user.getId(), StateOrderEnum.END);
            //First list of transaction only end

            /*List<Transaction> filteredTransactionsEnds = trades.stream()
                    .filter(transaction -> StateOrderEnum.END.equals(transaction.getOrdre().getState()))
                    .collect(Collectors.toList());*/


            //All list of transaction that state is not end
            /*List<Transaction> filteredTransactionsNotEnd = trades.stream()
                    .filter(transaction -> !"END".equals(transaction.getOrdre().getState()))
                    .collect(Collectors.toList());*/

            //
            //All orders in progress
            List<Transaction> transactionsInProgress = transactionService.getAllTransactionsParUtilisateurEtOrdreEnd(user.getId(), StateOrderEnum.IN_PROGRESS);
            for (Transaction transaction : transactionsInProgress) {
                FinalizationTrading.finalizationResult(transaction.getOrdre(),transactionService,orderService,customerTransactionRepository,userService,tradingConfigService);
            }

            //
            mav.addObject("trades", trades);

            return mav;
        } else {
            ModelAndView mav = new ModelAndView("index_site");
            mav.addObject("baseUrlApp", baseUrlWs);
            return mav;
        }
    }

    //
    @GetMapping("/assets-site")
    public ModelAndView assetsSite(@AuthenticationPrincipal UserDetails userDetails){
        if (UserInterceptor.isUserLogged()) {
            AppClient user = userService.findCurrentUserAppClient();
            if(!user.getState().equals(ClientStateEnum.ACTIVE)){
                ModelAndView mav = new ModelAndView("index_site");
                mav.addObject("baseUrlApp", baseUrlWs);
                return mav;
            }
            List<Cryptomonnaie> cryptomonnaies = cryptomonnaieService.getAllCryptomonnaies();
            ModelAndView mav = new ModelAndView("assets_site");
            mav.addObject("user", user);
            mav.addObject("cryptomonnaies", cryptomonnaies);
            mav.addObject("baseUrlApp", baseUrlWs);
            return mav;
        } else {
            ModelAndView mav = new ModelAndView("index_site");
            mav.addObject("baseUrlApp", baseUrlWs);
            return mav;
        }
    }
}
