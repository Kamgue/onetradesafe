package com.example.onetrade.web;

import com.example.onetrade.model.binding.request.CryptomonnaieModel;
import com.example.onetrade.model.entities.Cryptomonnaie;
import com.example.onetrade.model.service.CryptomonnaieService;
import com.example.onetrade.service.BaseConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/cryptomonnaies")
public class CryptomonnaieController {
    private final CryptomonnaieService cryptomonnaieService;

    public CryptomonnaieController(CryptomonnaieService cryptomonnaieService) {
        this.cryptomonnaieService = cryptomonnaieService;
    }

    @GetMapping("/creer")
    public String afficherFormulaireCreation(Model model) {
        model.addAttribute("cryptomonnaie", CryptomonnaieModel.builder().build());
        return "cryptomonnaies/creer";
    }

    @PostMapping("/creer")
    public String creerCryptomonnaie(@ModelAttribute("cryptomonnaie") CryptomonnaieModel cryptomonnaieModel, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "cryptomonnaies/creer";
        }
        cryptomonnaieService.creerCryptomonnaie(cryptomonnaieModel);
        return "redirect:/cryptomonnaies";
    }

    @GetMapping("/{id}/modifier")
    public String afficherFormulaireModification(@PathVariable Long id, Model model) {
        Optional<Cryptomonnaie> cryptomonnaie = cryptomonnaieService.getCryptomonnaie(id);
        if (cryptomonnaie.isPresent()) {
            model.addAttribute("cryptomonnaie", CryptomonnaieModel.buildFromModel(cryptomonnaie.get()));
            return "cryptomonnaies/modifier";
        }
        return "redirect:/cryptomonnaies";
    }

    @PostMapping("/{id}/modifier")
    public String modifierCryptomonnaie(@PathVariable Long id, @ModelAttribute CryptomonnaieModel cryptomonnaieModel, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "cryptomonnaies/modifier";
        }
        cryptomonnaieModel.setId(id);
        cryptomonnaieService.modifierCryptomonnaie(cryptomonnaieModel);
        return "redirect:/cryptomonnaies";
    }

    @GetMapping("/{id}/supprimer")
    public String supprimerCryptomonnaie(@PathVariable Long id) {
        cryptomonnaieService.supprimerCryptomonnaie(id);
        return "redirect:/cryptomonnaies";
    }

    @GetMapping
    public String afficherListeCryptomonnaies(Model model) {
        List<Cryptomonnaie> cryptomonnaies = cryptomonnaieService.getAllCryptomonnaies();
        model.addAttribute("cryptomonnaies", cryptomonnaies);
        return "cryptomonnaies/liste";
    }
}
