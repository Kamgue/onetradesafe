package com.example.onetrade.web;

import com.example.onetrade.config.AppProperties;
import com.example.onetrade.config.UserInterceptor;
import com.example.onetrade.model.binding.EntityResponse;
import com.example.onetrade.model.binding.ResetPassWordModel;
import com.example.onetrade.model.binding.SignUpBindingModel;
import com.example.onetrade.model.binding.UpdateClientBindingModel;
import com.example.onetrade.model.binding.request.CustomerTransactionModelResquest;
import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.AppUser;
import com.example.onetrade.model.entities.enums.ClientStateEnum;
import com.example.onetrade.model.entities.enums.LangueEnum;
import com.example.onetrade.model.service.SignUpServiceModel;
import com.example.onetrade.service.PieceJointeService;
import com.example.onetrade.service.UserService;
import com.example.onetrade.service.impl.MailServiceImpl;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.utility.RandomString;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

@Slf4j
@Controller
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final PieceJointeService pieceJointeService;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;
    private final AppProperties appProperties;
    @Autowired
    private MailServiceImpl mailService;
    @Autowired
    public UserController(UserService userService, PieceJointeService pieceJointeService, ModelMapper modelMapper, PasswordEncoder passwordEncoder, AppProperties appProperties) {
        this.userService = userService;
        this.pieceJointeService = pieceJointeService;
        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
        this.appProperties = appProperties;
    }

    @GetMapping("/signup")
    public String showSignUp(Model model) {
        if (!model.containsAttribute("signUpBindingModel")) {
            model.addAttribute("signUpBindingModel", new SignUpBindingModel());
            model.addAttribute("isExists", false);
            model.addAttribute("dontMatch", false);
            model.addAttribute("emptyGender", false);
        }

        return "signup/signup";
    }
    @GetMapping("")
    public String showUsers(Model model,
                            @RequestParam(name = "token", defaultValue = "") String token,
                            @RequestParam(name = "page", defaultValue = "0") int page,
                            @RequestParam(name = "size", defaultValue = "10") int size,
                            @RequestParam(name = "orderBy", defaultValue = "createdAt") String orderBy,
                            @RequestParam(name = "sort", defaultValue = "DESC") String sort) {

        model.addAttribute("users", this.userService.findAllClients(token, PageRequest.of(page, size, Sort.Direction.fromString(sort), orderBy)));
        model.addAttribute("currentOrderBy", orderBy);
        model.addAttribute("currentSort", sort);
        model.addAttribute("currentPage", page);
        model.addAttribute("pageSize", size);
        model.addAttribute("customerTransactionModel", CustomerTransactionModelResquest.builder().build());
        model.addAttribute("clients", userService.findAllClients("", PageRequest.of(0,20)));
        model.addAttribute("baseUrl", appProperties.getBaseUrl());
        return "account/users";
    }

    @GetMapping("/{id}")
    public String showUsers(Model model, @PathVariable(name = "id") Long id) {
        if (UserInterceptor.isUserLogged()) {
            AppClient currentUserAppClient = this.userService.findAppClientById(id);
            UpdateClientBindingModel updateClientBindingModel = UpdateClientBindingModel.buildFromModel(currentUserAppClient); //this.modelMapper.map(currentUserAppClient, UpdateClientBindingModel.class);
            model.addAttribute("client", currentUserAppClient);
            model.addAttribute("userConnected", userService.findCurrentUserAppClient());
            model.addAttribute("updateClientBindingModel", updateClientBindingModel);
            model.addAttribute("dontMatch", false);
            model.addAttribute("isExists", false);
            return "account/account-info";
        }
        return "redirect:/site";
    }
    @GetMapping("/{id}/state/{state}")
    public String showUsers(@PathVariable(name = "id") Long id, @PathVariable(name = "state") String state, HttpSession session) {
        if (UserInterceptor.isUserLogged()) {
            if (Arrays.stream(ClientStateEnum.values()).anyMatch(item-> item.name().equals(state))){
                this.userService.changeStateClient(id, state);

            }
            return "redirect:/users/";
        }
        return "home/index";
    }

    @PostMapping("/signup")
    public String signup(@Valid SignUpBindingModel signUpBindingModel, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors() || !(signUpBindingModel.getPassword().equals(signUpBindingModel.getConfirmPassword()))) {
            redirectAttributes.addFlashAttribute("signUpBindingModel", signUpBindingModel);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.signUpBindingModel", bindingResult);
            if (!(signUpBindingModel.getPassword().equals(signUpBindingModel.getConfirmPassword()))) {
                redirectAttributes.addFlashAttribute("dontMatch", true);
            }

            return "redirect:/users/signup";
        } else if (this.userService.userExists(signUpBindingModel.getFullname(), signUpBindingModel.getEmail())) {
            redirectAttributes.addFlashAttribute("signUpBindingModel", signUpBindingModel);
            redirectAttributes.addFlashAttribute("isExists", true);
            return "redirect:/users/signup";
        }
        AppClient client = this.userService.register(this.modelMapper.map(signUpBindingModel, SignUpServiceModel.class));
        redirectAttributes.addAttribute("email", client.getEmail());
        return "redirect:/users/active-account/resent-code";
    }

    @GetMapping("/login")
    public String showLogin(Model model, HttpSession session) {
        if (session.getAttribute("badCredentials") != null) {
            model.addAttribute("badCredentials", session.getAttribute("badCredentials"));
            model.addAttribute("email", session.getAttribute("email"));
            model.addAttribute("errorMessage", session.getAttribute("errorMessage"));

            session.removeAttribute("badCredentials");
            session.removeAttribute("email");
            session.removeAttribute("errorMessage");
        }

//        model.addAttribute("langues", LangueEnum.values());
        return "login/login";
    }

    @Deprecated
    @PostMapping("/login-error")
    public ModelAndView failedLogin(@ModelAttribute("email") String email) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("badCredentials", true);
        modelAndView.addObject("email", email);

        modelAndView.setViewName("login/login");

        return modelAndView;
    }


    @GetMapping("/account-info")
    public String showAccountInfo(Model model) {
        if (UserInterceptor.isUserLogged()) {
            AppClient currentUserAppClient = this.userService.findCurrentUserAppClient();
            UpdateClientBindingModel updateClientBindingModel = UpdateClientBindingModel.buildFromModel(currentUserAppClient); //this.modelMapper.map(currentUserAppClient, UpdateClientBindingModel.class);
            model.addAttribute("client", this.userService.findCurrentUserAppClient());
            model.addAttribute("updateClientBindingModel", updateClientBindingModel);
            model.addAttribute("dontMatch", false);
            model.addAttribute("isExists", false);
            return "account/account-info";
        }
        return "redirect:/site";
    }
    @GetMapping("/active-account/{token}")
    public String showActiveAccount(@PathVariable("token") String token, Model model) {
        EntityResponse<AppClient> reponse = userService.activeAccount(token);
        if (!reponse.isSuccess()){
            model.addAttribute("tilte", "Account activation failure");
            model.addAttribute("contentMessage", reponse.getMessage());
            return "account/activate_account";
        }else{
            return "redirect:/site";
            // model.addAttribute("tilte", "Successful active account");
            // model.addAttribute("contentMessage", "Your account has been successfully activated. Please complete your identification in your profile.");
        }
    }

    @GetMapping("/active-account/resent-code")
    public String resentCodeActiveAccount(@RequestParam("email") String email, Model model) {
        model.addAttribute("email", email);
        EntityResponse<AppClient> dataClient = userService.findClientByEmail(email);
        if (dataClient.isSuccess()) {
            EntityResponse<String> reponse = userService.sendMailAfterCreateAccount(dataClient.getData());
            if (!reponse.isSuccess()){
                model.addAttribute("errorMessage", reponse.getMessage());
            }
        }else{
            model.addAttribute("errorMessage", dataClient.getMessage());
        }

        return "account/resend_code_activate_account";
    }


    @GetMapping("/update-user")
    public String showUpdateForm(Model model) {
        if (UserInterceptor.isUserLogged()) {
            if (!model.containsAttribute("updateClientBindingModel")) {
                AppClient currentUserAppClient = this.userService.findCurrentUserAppClient();
                UpdateClientBindingModel updateClientBindingModel = this.modelMapper.map(currentUserAppClient, UpdateClientBindingModel.class);
                model.addAttribute("updateClientBindingModel", updateClientBindingModel);
                model.addAttribute("dontMatch", false);
                model.addAttribute("emptyGender", false);
                model.addAttribute("isExists", false);
            }
            return "account/update-user";
        } else {
            return "redirect:/site";
        }
    }

    @PostMapping(value = {"/update-user"})
    public String updateUser(@ModelAttribute("updateClientBindingModel") @Valid UpdateClientBindingModel updateClientBindingModel,
                             BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (UserInterceptor.isUserLogged()) {
            String routerUrl = "redirect:/users/account-info";
            boolean dontMatch = Objects.nonNull(updateClientBindingModel.getPassword()) && !(updateClientBindingModel.getPassword().equals(updateClientBindingModel.getConfirmPassword()));
            if (bindingResult.hasErrors() || dontMatch) {
                redirectAttributes.addFlashAttribute("updateClientBindingModel", updateClientBindingModel);
                redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.updateClientBindingModel", bindingResult);
                if (dontMatch) {
                    redirectAttributes.addFlashAttribute("dontMatch", true);
                }
            } else {
                this.userService.updatedUserClient(updateClientBindingModel);
            }
            return routerUrl;
        } else {
            return "redirect:/site";
        }
    }


    @GetMapping("/reset/password/set1")
    public String resendTokenResetPassword(@RequestParam(name = "email", defaultValue = "",required = false) String email, Model model) {

        model.addAttribute("email", email);
        if (email.length()>1){
            EntityResponse<AppClient> dataClient = userService.findClientByEmail(email);
            if (dataClient.isSuccess()) {
                String token = RandomString.make(100);
                userService.createForgottenPasswordTokenForUser(dataClient.getData(), token);
                mailService.sendMailAfterProcessForgottenPasswordEmail(dataClient.getData(), token, Locale.forLanguageTag("en"));
                model.addAttribute("multipleSend", true);
            }else{
                model.addAttribute("errorMessage", dataClient.getMessage());
            }
        }else{
            model.addAttribute("multipleSend", false);
            model.addAttribute("errorMessage", "incorrect email");
        }

        return "login/reset-password-email";
    }
    @GetMapping("/reset/password/set2/{token}")
    public String showResetPassWordForm(@PathVariable("token") String token, Model model) {
        model.addAttribute("token", token);
        model.addAttribute("resetPassWordModel", ResetPassWordModel.builder().token(token).build());
        model.addAttribute("errorMessage", "");
        return "account/reset-password";
    }
    @PostMapping(value = {"/reset/password/set2"})
    public String resetPassword(@ModelAttribute("resetPassWordModel") @Valid ResetPassWordModel resetPassWordModel,
                                BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("resetPassWordModel", resetPassWordModel);
        if (UserInterceptor.isUserLogged()) {
            AppClient currentUserAppClient = this.userService.findCurrentUserAppClient();
            boolean dontMatch = Objects.nonNull(resetPassWordModel.getPassword()) && !(resetPassWordModel.getPassword().equals(resetPassWordModel.getConfirmPassword()));
            if (dontMatch) {
                redirectAttributes.addFlashAttribute("errorMessage", "Your Password don't match");
                return "redirect:/reset/password/set2";
            }else{
                currentUserAppClient.setPassword(this.passwordEncoder.encode(resetPassWordModel.getPassword()));
                this.userService.saveUpdatedUserClient(currentUserAppClient);
                return "redirect:/users/account-info";
            }
        } else {
            boolean dontMatch = !(resetPassWordModel.getPassword().equals(resetPassWordModel.getConfirmPassword()));
            if (dontMatch) {
                redirectAttributes.addFlashAttribute("token", resetPassWordModel.getToken());
                redirectAttributes.addFlashAttribute("resetPassWordModel", resetPassWordModel);
                redirectAttributes.addFlashAttribute("errorMessage", "Your Password don't match");
                return "account/reset-password";
            }
            String tokenValid = userService.validateForgottenPasswordToken(resetPassWordModel.getToken());
            if (tokenValid.equalsIgnoreCase("valid")) {
                AppUser utilisateur = userService.getUserFromForgottenPasswordToken(resetPassWordModel.getToken());
                AppClient client = userService.findClientByEmail(utilisateur.getEmail()).getData();
                String passwordCrypt = this.passwordEncoder.encode(resetPassWordModel.getPassword());
                client.setPassword(passwordCrypt);
                userService.saveUpdatedUserClient(client);
                // mailService.sendMailAfterResetPassword(utilisateur, Locale.forLanguageTag("fr"));
                return "login/login";
            }else {
                redirectAttributes.addFlashAttribute("resetPassWordModel", resetPassWordModel);
                redirectAttributes.addFlashAttribute("errorMessage", "invalid token");
                return "account/reset-password";
            }
        }
    }


}


