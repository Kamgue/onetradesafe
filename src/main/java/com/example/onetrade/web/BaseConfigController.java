package com.example.onetrade.web;

import com.example.onetrade.model.binding.EntityResponse;
import com.example.onetrade.model.binding.request.CustomerTransactionModelResquest;
import com.example.onetrade.model.binding.request.NetworkModel;
import com.example.onetrade.model.binding.response.ClientResponseModel;
import com.example.onetrade.model.binding.response.NetworkResponseModel;
import com.example.onetrade.model.entities.CustomerTransaction;
import com.example.onetrade.service.BaseConfigService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/août/2024 -- 05:55
 *
 * @author name :  Franky Brice on 07/08/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.web
 **/
@Controller
@RequestMapping("/config")
public class BaseConfigController {

    private final BaseConfigService baseConfigService;

    public BaseConfigController(BaseConfigService baseConfigService) {
        this.baseConfigService = baseConfigService;
    }



    @GetMapping("/networks")
    public String showNetwork(Model model,
                              @RequestParam(name = "token", defaultValue = "") String token,
                              @RequestParam(name = "page", defaultValue = "0") int page,
                              @RequestParam(name = "size", defaultValue = "10") int size,
                              @RequestParam(name = "orderBy", defaultValue = "createdAt") String orderBy,
                              @RequestParam(name = "sort", defaultValue = "DESC") String sort) {
        Page<NetworkResponseModel> networks = baseConfigService.findAllNetwork(token, PageRequest.of(page, size, Sort.Direction.fromString(sort), orderBy));
        model.addAttribute("networks", networks);
        model.addAttribute("currentOrderBy", orderBy);
        model.addAttribute("currentSort", sort);
        model.addAttribute("currentPage", page);
        model.addAttribute("pageSize", size);
        model.addAttribute("networkModel", NetworkModel.builder().build());
        return "network/network";
    }
    @PostMapping("/networks")
    public String saveNetwork(@ModelAttribute NetworkModel networkModel) {

        baseConfigService.addOrUpdateNetwork(networkModel);

        return "redirect:/config/networks";
    }
    @GetMapping("/networks/{id}/delete")
    public String deleteNetwork(@PathVariable(name = "id") long id ) {

        baseConfigService.deleteNetwork(id);

        return "redirect:/config/networks";
    }
}
