package com.example.onetrade.web;

import com.example.onetrade.model.binding.EntityResponse;
import com.example.onetrade.model.binding.request.CustomerTransactionModelResquest;
import com.example.onetrade.model.binding.request.QuickRechargeModel;
import com.example.onetrade.model.binding.response.ClientResponseModel;
import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.CustomerTransaction;
import com.example.onetrade.model.entities.enums.ClientStateEnum;
import com.example.onetrade.model.service.CryptomonnaieService;
import com.example.onetrade.service.BaseConfigService;
import com.example.onetrade.service.CustomerTransactionService;
import com.example.onetrade.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 15/juillet/2024 -- 05:17
 *
 * @author name :  Franky Brice on 15/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.web
 **/
@Controller
@RequestMapping("/reglements")
public class CustomerTransactionController {
    private final CustomerTransactionService customerTransactionService;
    private final UserService userService;
    private final CryptomonnaieService cryptomonnaieService;
    private final BaseConfigService baseConfigService;

    public CustomerTransactionController(CustomerTransactionService customerTransactionService, UserService userService, CryptomonnaieService cryptomonnaieService, BaseConfigService baseConfigService) {
        this.customerTransactionService = customerTransactionService;
        this.userService = userService;
        this.cryptomonnaieService = cryptomonnaieService;
        this.baseConfigService = baseConfigService;
    }
    @GetMapping("")
    public String afficherListeTransactions(Model model,
                                            @RequestParam(name = "idClient", defaultValue = "0") long idClient,
                                            @RequestParam(name = "token", defaultValue = "") String token,
                                            @RequestParam(name = "page", defaultValue = "0") int page,
                                            @RequestParam(name = "size", defaultValue = "10") int size,
                                            @RequestParam(name = "orderBy", defaultValue = "createdAt") String orderBy,
                                            @RequestParam(name = "sort", defaultValue = "DESC") String sort) {
        Page<CustomerTransaction> transactions = customerTransactionService.getAllCustomerTransaction(idClient, token, PageRequest.of(page, size, Sort.Direction.fromString(sort), orderBy));
        Page<ClientResponseModel> clients = userService.findAllClientsState(ClientStateEnum.ACTIVE, PageRequest.of(0,200));

        model.addAttribute("transactions", transactions);
        model.addAttribute("clients", clients);
        model.addAttribute("currentOrderBy", orderBy);
        model.addAttribute("currentSort", sort);
        model.addAttribute("currentPage", page);
        model.addAttribute("pageSize", size);
        model.addAttribute("customerTransactionModel", CustomerTransactionModelResquest.builder().build());
        return "customer-transactions/liste";
    }
    @GetMapping("/deposit")
    public String showDeposit(Model model) {
        model.addAttribute("cryptoMonnaies", cryptomonnaieService.getAllCryptomonnaies());
        model.addAttribute("networks", baseConfigService.findAllNetwork());
        return "customer-transactions/deposit";
    }
    @GetMapping("/deposit/recharge")
    public String showQuickRecharge(Model model) {
        model.addAttribute("quickRechargeModel", QuickRechargeModel.builder().build());
        return "customer-transactions/recharge";
    }
    @PostMapping("/deposit/recharge")
    public String saveQuickRecharge( @ModelAttribute QuickRechargeModel quickRechargeModel) {
        customerTransactionService.saveQuickRecharge(quickRechargeModel);
        return "redirect:/reglements/deposit/recharge";
    }
    @PostMapping("/add")
    public String creerTransaction( @RequestParam(name = "source", defaultValue = "reglement") String source, @ModelAttribute CustomerTransactionModelResquest transaction, RedirectAttributes redirectAttributes) {

        EntityResponse<CustomerTransaction> response = customerTransactionService.addCustomerTransaction(transaction);
        if (!response.isSuccess()) {
            redirectAttributes.addAttribute("errorMessage", response.getMessage());
        }
        if (source.equals("user")){
            return "redirect:/users";
        }
        return "redirect:/reglements";
    }
}
