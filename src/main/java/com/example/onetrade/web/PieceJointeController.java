package com.example.onetrade.web;

import com.example.onetrade.model.binding.PieceJointeModel;
import com.example.onetrade.model.entities.PieceJointe;
import com.example.onetrade.model.entities.enums.TypePieceJointeEnum;
import com.example.onetrade.service.PieceJointeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 07/juillet/2024 -- 03:33
 *
 * @author name :  Franky Brice on 07/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.web
 **/
@Slf4j
@Controller
@RequestMapping("/piece-jointes")
public class PieceJointeController {
    private final PieceJointeService pieceJointeService;

    public PieceJointeController(PieceJointeService pieceJointeService) {
        this.pieceJointeService = pieceJointeService;
    }

    @PostMapping(value = "/", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String ajouterPieceJointe(MultipartFile file,
                                     @RequestParam(value = "typePieceJointe", defaultValue = "ALL") String typePieceJointe,
                                     @RequestParam(value = "idSourcePj", defaultValue = "0") Long idSourcePj,
                                     @RequestParam(value = "idPj", defaultValue = "0") Long idPj) throws IOException {
        PieceJointeModel dto = PieceJointeModel.builder()
                .id(idPj)
                .typePj(typePieceJointe)
                .datePj(LocalDate.now())
                .build();
        dto.setIdSourcePj(idSourcePj);
        PieceJointe data = pieceJointeService.ajouterPieceJointe(file, dto);
        log.info("Ajouter une pièce jointe " + data.getLibelle() + " " + new Date().toString());
        return "redirect:/";
    }

    @GetMapping("/key/{idType}/type/{typePj}")
    public String consulterPJPieceJointeIdTypeEtType(@NotNull @PathVariable(name = "idType") Long idType,
                                                     @NotNull @PathVariable(name = "typePj") String typePj)  {

        List<PieceJointeModel> data = pieceJointeService.findListPieceJointeByTypeEntiteAndId(TypePieceJointeEnum.valueOf(typePj), idType);

        log.info("Consulter la liste des pieces jointes " + " " + new Date().toString());
        return "";
    }

    @GetMapping("/type/{typePj}")
    public String consulterPJPieceJointeType(@PathVariable(name = "typePj") String typePj)  {
        List<PieceJointeModel> data = pieceJointeService.findListPieceJointeByTypeEntite(TypePieceJointeEnum.valueOf(typePj));

        log.info("Consulter la liste des pieces jointes " + " " + new Date().toString());
        return "";
    }


    @GetMapping("/{id}")
    public String consulterPieceJointe( @NotNull @PathVariable(name = "id") Long id)  {
        PieceJointe pieceJointe = pieceJointeService.findById(id);
        PieceJointeModel data = PieceJointeModel.buildFromEntity(pieceJointe);

        log.info("Consulter une pièce jointe " + data.getLibelle() + " " + new Date().toString());
        return "";
    }

    @DeleteMapping("/piece-jointes/{id}/delete")
    public String supprimerPieceJointe(@NotNull @PathVariable(name = "id") Long id)  {
        pieceJointeService.delete(id);
        return "";
    }

    @GetMapping(value = "/telechager/{id}", produces = { "*/*"})
    public ResponseEntity<ByteArrayResource> loadImage(@NotNull  @PathVariable(name = "id") Long id) {
        try {
            PieceJointe pieceJointe = pieceJointeService.findById(id);
            File file = new File(pieceJointe.getChemin());
            Path path = Paths.get(file.toURI());
            log.info("Telechager une pièce jointe " + pieceJointe.getLibelle() + " " + new Date().toString());
            //return Files.readAllBytes(path);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachement;filename="+pieceJointe.getOriginalName())
                    .body(new ByteArrayResource(Files.readAllBytes(path)));
        } catch (Exception e) {
            return null;
        }
    }

}
