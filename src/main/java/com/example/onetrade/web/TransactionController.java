package com.example.onetrade.web;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.Transaction;
import com.example.onetrade.model.repostiory.AppClientRepository;
import com.example.onetrade.model.service.OrderService;
import com.example.onetrade.model.service.TransactionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/transactions")
public class TransactionController {
    private final TransactionService transactionService;
    private final AppClientRepository utilisateurService;

    private final OrderService orderService;

    public TransactionController(TransactionService transactionService, AppClientRepository utilisateurService, OrderService orderService) {
        this.transactionService = transactionService;
        this.utilisateurService = utilisateurService;
        this.orderService = orderService;
    }

    @GetMapping("/creer")
    public String afficherFormulaireCreation(Model model) {
        model.addAttribute("transaction", new Transaction());
        model.addAttribute("ordres", orderService.getAllOrdres());
        model.addAttribute("utilisateurs", utilisateurService.findAll());
        return "transactions/creer";
    }

    @PostMapping("/creer")
    public String creerTransaction(@ModelAttribute Transaction transaction, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "transactions/creer";
        }
        transactionService.creerTransaction(transaction);
        return "redirect:/transactions";
    }

    @GetMapping("/{id}")
    public String afficherTransaction(@PathVariable Long id, Model model) {
        Optional<Transaction> transaction = transactionService.getTransaction(id);
        if (transaction.isPresent()) {
            model.addAttribute("transaction", transaction.get());
            return "transactions/details";
        }
        return "redirect:/transactions";
    }

    @GetMapping("/{id}/modifier")
    public String afficherFormulaireModification(@PathVariable Long id, Model model) {
        Optional<Transaction> transaction = transactionService.getTransaction(id);
        if (transaction.isPresent()) {
            model.addAttribute("transaction", transaction.get());
            model.addAttribute("ordres", orderService.getAllOrdres());
            model.addAttribute("utilisateurs", utilisateurService.findAll());
            return "transactions/modifier";
        }
        return "redirect:/transactions";
    }

    @PostMapping("/{id}/modifier")
    public String modifierTransaction(@PathVariable Long id, @ModelAttribute Transaction transaction, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "transactions/modifier";
        }
        transaction.setId(id);
        transactionService.creerTransaction(transaction);
        return "redirect:/transactions";
    }

    @GetMapping("/utilisateur/{id}")
    public String afficherTransactionsUtilisateur(@PathVariable Long id, Model model) {
        Optional<AppClient> utilisateur = utilisateurService.findById(id);
        if (utilisateur.isPresent()) {
            List<Transaction> transactions = transactionService.getAllTransactionsParUtilisateur(utilisateur.get());
            model.addAttribute("transactions", transactions);
            model.addAttribute("utilisateur", utilisateur.get());
            return "transactions/liste-utilisateur";
        }
        return "redirect:/transactions";
    }

    @GetMapping
    public String afficherListeTransactions(Model model) {
        List<Transaction> transactions = transactionService.getAllTransactions();
        model.addAttribute("transactions", transactions);
        return "transactions/liste";
    }

    @GetMapping("/{id}/supprimer")
    public String supprimerTransaction(@PathVariable Long id) {
        transactionService.supprimerTransaction(id);
        return "redirect:/transactions";
    }
}
