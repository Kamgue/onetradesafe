package com.example.onetrade.web;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.Map;

@Controller
public class MyWebSocketController {
    //@Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Scheduled(fixedRate = 5000) // Envoie un message toutes les 5 secondes
    public void sendData() {
        Map<String, Object> data = new HashMap<>();
        data.put("message", "Hello from server!");
        data.put("timestamp", System.currentTimeMillis());
        messagingTemplate.convertAndSend("/topic/greetings", data);
    }
}

