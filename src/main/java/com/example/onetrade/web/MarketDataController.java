package com.example.onetrade.web;

import com.example.onetrade.model.entities.Cryptomonnaie;
import com.example.onetrade.model.entities.MarketData;
import com.example.onetrade.model.service.CryptomonnaieService;
import com.example.onetrade.model.service.MarketDataService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/market-data")
public class MarketDataController {
    private final MarketDataService marketDataService;
    private final CryptomonnaieService cryptomonnaieService;

    public MarketDataController(MarketDataService marketDataService, CryptomonnaieService cryptomonnaieService) {
        this.marketDataService = marketDataService;
        this.cryptomonnaieService = cryptomonnaieService;
    }

    @GetMapping("/creer")
    public String afficherFormulaireCreation(Model model) {
        model.addAttribute("marketData", new MarketData());
        model.addAttribute("cryptomonnaies", cryptomonnaieService.getAllCryptomonnaies());
        return "market-data/creer";
    }

    @PostMapping("/creer")
    public String creerMarketData(@ModelAttribute MarketData marketData, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "market-data/creer";
        }
        marketDataService.enregistrerMarketData(marketData);
        return "redirect:/market-data";
    }

    @GetMapping("/{id}")
    public String afficherMarketData(@PathVariable Long id, Model model) {
        Optional<MarketData> marketData = marketDataService.getMarketData(id);
        if (marketData.isPresent()) {
            model.addAttribute("marketData", marketData.get());
            return "market-data/details";
        }
        return "redirect:/market-data";
    }

    @GetMapping("/{id}/modifier")
    public String afficherFormulaireModification(@PathVariable Long id, Model model) {
        Optional<MarketData> marketData = marketDataService.getMarketData(id);
        if (marketData.isPresent()) {
            model.addAttribute("marketData", marketData.get());
            model.addAttribute("cryptomonnaies", cryptomonnaieService.getAllCryptomonnaies());
            return "market-data/modifier";
        }
        return "redirect:/market-data";
    }

    @PostMapping("/{id}/modifier")
    public String modifierMarketData(@PathVariable Long id, @ModelAttribute MarketData marketData, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "market-data/modifier";
        }
        marketData.setId(id);
        marketDataService.enregistrerMarketData(marketData);
        return "redirect:/market-data";
    }


    @GetMapping("/cryptomonnaie/{id}")
    public String afficherMarketDataCryptomonnaie(@PathVariable Long id, Model model) {
        Optional<Cryptomonnaie> cryptomonnaie = cryptomonnaieService.getCryptomonnaie(id);
        if (cryptomonnaie.isPresent()) {
            List<MarketData> marketData = marketDataService.getAllMarketDataParCryptomonnaie(cryptomonnaie.get());
            model.addAttribute("marketData", marketData);
            model.addAttribute("cryptomonnaie", cryptomonnaie.get());
            return "market-data/liste-cryptomonnaie";
        }
        return "redirect:/market-data";
    }

    @GetMapping
    public String afficherListeMarketData(Model model) {
        List<MarketData> marketData = marketDataService.getAllMarketData();
        model.addAttribute("marketData", marketData);
        return "market-data/liste";
    }

    @GetMapping("/{id}/supprimer")
    public String supprimerMarketData(@PathVariable Long id) {
        marketDataService.supprimerMarketData(id);
        return "redirect:/market-data";
    }
}
