package com.example.onetrade.web;

import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.model.entities.Ordre;
import com.example.onetrade.model.entities.enums.StateOrderEnum;
import com.example.onetrade.model.repostiory.AppClientRepository;
import com.example.onetrade.model.service.CryptomonnaieService;
import com.example.onetrade.model.service.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping("/ordres")
public class OrderController {
    private final OrderService orderService;
    private final AppClientRepository utilisateurService;
    private final CryptomonnaieService cryptomonnaieService;

    public OrderController(OrderService orderService, AppClientRepository utilisateurService, CryptomonnaieService cryptomonnaieService) {
        this.orderService = orderService;
        this.utilisateurService = utilisateurService;
        this.cryptomonnaieService = cryptomonnaieService;
    }

    @GetMapping("/creer")
    public String afficherFormulaireCreation(Model model) {
        model.addAttribute("ordre", new Ordre());
        model.addAttribute("utilisateurs", utilisateurService.findAll());
        model.addAttribute("cryptomonnaies", cryptomonnaieService.getAllCryptomonnaies());
        return "ordres/creer";
    }

    @PostMapping("/creer")
    public String creerOrdre(@ModelAttribute Ordre ordre, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "ordres/creer";
        }
        ordre.setToken(UUID.randomUUID().toString());
        ordre.setState(StateOrderEnum.IN_PROGRESS);
        orderService.creerOrdre(ordre);
        return "redirect:/ordres";
    }

    @GetMapping("/{id}/modifier")
    public String afficherFormulaireModification(@PathVariable Long id, Model model) {
        Optional<Ordre> ordre = orderService.getOrdre(id);
        if (ordre.isPresent()) {
            model.addAttribute("ordre", ordre.get());
            model.addAttribute("utilisateurs", utilisateurService.findAll());
            model.addAttribute("cryptomonnaies", cryptomonnaieService.getAllCryptomonnaies());
            return "ordres/modifier";
        }
        return "redirect:/ordres";
    }

    @PostMapping("/{id}/modifier")
    public String modifierOrdre(@PathVariable Long id, @ModelAttribute Ordre ordre, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "ordres/modifier";
        }
        ordre.setId(id);
        orderService.modifierOrdre(ordre);
        return "redirect:/ordres";
    }

    @GetMapping("/{id}/supprimer")
    public String supprimerOrdre(@PathVariable Long id) {
        orderService.supprimerOrdre(id);
        return "redirect:/ordres";
    }

    @GetMapping
    public String afficherListeOrdres(Model model) {
        List<Ordre> ordres = orderService.getAllOrdres();
        model.addAttribute("ordres", ordres);
        model.addAttribute("utilisateurs", utilisateurService.findAll());
        model.addAttribute("cryptomonnaies", cryptomonnaieService.getAllCryptomonnaies());
        return "ordres/liste";
    }
    @GetMapping("/utilisateur/{id}")
    public String afficherOrdresUtilisateur(@PathVariable Long id, Model model) {
        Optional<AppClient> utilisateur = utilisateurService.findById(id);
        if (utilisateur.isPresent()) {
            List<Ordre> ordres = orderService.getAllOrdresParUtilisateur(utilisateur.get());
            model.addAttribute("ordres", ordres);
            model.addAttribute("utilisateur", utilisateur.get());
            return "ordres/liste-utilisateur";
        }
        return "redirect:/ordres";
    }
}