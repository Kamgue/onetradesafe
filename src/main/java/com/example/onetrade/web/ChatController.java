package com.example.onetrade.web;

import com.example.onetrade.config.AppProperties;
import com.example.onetrade.config.UserInterceptor;
import com.example.onetrade.model.binding.request.MessageModel;
import com.example.onetrade.model.binding.response.ClientResponseModel;
import com.example.onetrade.model.binding.response.ConversationResponseModel;
import com.example.onetrade.model.binding.response.MessageResponseModel;
import com.example.onetrade.model.entities.AppClient;
import com.example.onetrade.service.ChatService;
import com.example.onetrade.service.UserService;
import com.example.onetrade.utils.StringsUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 21/juillet/2024 -- 12:58
 *
 * @author name :  Franky Brice on 21/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.web
 **/
@Slf4j
@Controller
@RequestMapping("/chat")
public class ChatController {
    private final UserService userService;
    private final ChatService chatService;
    private final AppProperties appProperties;

    public ChatController(UserService userService, ChatService chatService, AppProperties appProperties) {
        this.userService = userService;
        this.chatService = chatService;
        this.appProperties = appProperties;
    }


    @GetMapping("/user")
    public String showTradingHistory(Model model) {
        if (!UserInterceptor.isUserLogged())
            return "redirect:/users/login";
        AppClient user = userService.findCurrentUserAppClient();
        model.addAttribute("baseUrlWebSocket", appProperties.getBaseUrl());
        model.addAttribute("useConnected", user);
        model.addAttribute("fileUtil", StringsUtils.builder().build());

        ConversationResponseModel conversation = chatService.findConversationClient(user.getId());
        model.addAttribute("currentConversation", conversation);
        List<MessageResponseModel> messages = conversation.getMessages();
//        messages.sort((m1, m2) -> m2.getCreatedAt().compareTo(m1.getCreatedAt()));
        model.addAttribute("listMessages", messages);
        return "chat/chat_user";
    }
    @GetMapping("/admin")
    public String chat(Model model,
                       @RequestParam(name = "idConversation", defaultValue = "0") long idConversation) {

        if (!UserInterceptor.isUserLogged())
            return "redirect:/users/login";
        List<ClientResponseModel> clients = chatService.findClientForNewConversation();
        model.addAttribute("baseUrlWebSocket", appProperties.getBaseUrl());
        model.addAttribute("clients", clients);
        List<ConversationResponseModel> conversations = chatService.findAllConversations();
        model.addAttribute("conversations", conversations);
        model.addAttribute("fileUtil", StringsUtils.builder().build());

        model.addAttribute("client", null);
        model.addAttribute("currentConversation", null);
        if (idConversation >0){
            ConversationResponseModel conversation = chatService.findOneConversation(idConversation);
            model.addAttribute("client", conversation.getUser());
            model.addAttribute("currentConversation", conversation);
            model.addAttribute("useConnected", userService.findCurrentUserAppClient());

            model.addAttribute("listMessages", conversation.getMessages());
        }

        model.addAttribute("messageModel", MessageModel.builder().build());
        return "chat/chat";
    }

    @GetMapping("/conversation")
    public String sendMessage( @RequestParam(name = "idUser", defaultValue = "0") long idUser) {

        if (idUser>0){
            chatService.addConversation(idUser);
        }

        return "redirect:/chat/admin";
    }

}
