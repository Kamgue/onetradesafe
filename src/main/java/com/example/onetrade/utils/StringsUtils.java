package com.example.onetrade.utils;

import lombok.Builder;
import lombok.Getter;

import java.util.Objects;
import java.util.Set;

/**
 * Copyright (c) 2024, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 06/juillet/2024 -- 12:56
 *
 * @author name :  Franky Brice on 06/07/2024
 * @author email : kouamfranky@gmail.com /  franky.kouam@iforce5.com
 * Project : @project onetradesafe
 * Package : @package com.example.onetrade.utils
 **/
@Getter
@Builder
public class StringsUtils {
    public StringsUtils() {
    }

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";
    public static final String SUCCESS_MESSAGE = "Operation éffectuée avec succès";
    public static final String TOKEN_EMAIL_EXPIRED = "The used token has expired";
    public static final String UNRECOGNIZED_EMAIL_TOKEN = "unrecognized token, be sure to use the most recent token.";
    public static final String CAN_NOT_FIND_CONFIG_GLOBAL_VARIABLE_WITH_KEY = "Can not find config global variable with key  : ";
    public static final String UNABLE_TO_SEND_E_MAIL_INFORMATION_INCOMPLETE = "Unable to send e-mail: information incomplete";
    public static final String OPERATION_NOT_SUPPORTED = "Operation not supported";
    public static final String KEY_EXPIRATION_DATE_TOKEN = "KEY_EXPIRATION_DATE_TOKEN";
    public static final String KEY_DEFAULT_NUM_COMPTE_PRINCIPAL = "KEY_DEFAULT_NUM_COMPTE_PRINCIPAL";
    public static final String VALUE_DEFAULT_NUM_COMPTE_PRINCIPAL = "1000-1000-1000";
    public static final double VALUE_DEFAULT_SOLDE_COMPTE_PRINCIPAL = 10000D;

    public static final String TOKEN_INVALID = "invalidToken";
    public static final String TOKEN_EXPIRED = "expired";
    public static final String TOKEN_VALID = "valid";

    public static boolean valueExist(Object o){
        return Objects.nonNull(o);
    }

    private static final Set<String> VALID_IMAGE_EXTENSIONS = Set.of("png", "jpg", "jpeg", "svg");

    public static boolean isImageExtension(String extension) {
        return VALID_IMAGE_EXTENSIONS.contains(extension.toLowerCase());
    }
}
