package com.example.onetrade.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;

import java.io.IOException;

/**
 * Copyright (c) 2023, Iforce5, All Right Reserved.
 * https://iforce5.com
 * <p>
 * When: @created 09/mars/2023 -- 08:57
 * By :  @author Fabrice Luther on 09/03/2023
 * Project : @project authentification-service
 * Package : @package com.iforce5.authentification.utils
 */
@Slf4j
public class JsonUtil {
    /**
     * Instantiates a new Json util.
     */
    private JsonUtil(){}

    /**
     * Gets json.
     *
     * @param obj the obj
     * @return the json
     */
    public static String getJson(Object obj) {


        final ObjectMapper mapper = new ObjectMapper();


        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();

        String json = "";
        try {
            json = ow.writeValueAsString(obj);
        } catch (final JsonProcessingException e) {
            log.error("Serialisation du json :{}  Erreur: {}" ,json, e.getMessage());
        }
        return json;
    }

    /**
     * Gets pojo.
     *
     * @param <T>       the type parameter
     * @param json      the json
     * @param classType the class type
     * @return the pojo
     */
    @JsonInclude
    public static <T> T getPojo(String json, Class<T> classType) {

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        T emp = null;
        try {
            emp = mapper.readValue(json, classType);
        } catch (final IOException e) {
            log.error("Serialisation du json :{}  Erreur: {}" ,json, e.getMessage());
        }
        return emp;
    }

    /**
     * Encode object string.
     *
     * @param element the element
     * @return the string
     */
    public static String encodeObject(Object element){
        final String originalInput = getJson(element);
        return new String(Base64.encodeBase64(originalInput.getBytes()));
    }

    /**
     * Decode object t.
     *
     * @param <T>     the type parameter
     * @param clazz   the clazz
     * @param element the element
     * @return the t
     */
    public static <T> T decodeObject(Class<T> clazz,String element){
        final String decodedString = new String(Base64.decodeBase64(element.getBytes()));
        return getPojo(decodedString, clazz);
    }
}
