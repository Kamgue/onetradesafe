package com.example.onetrade.websocket;

import com.example.onetrade.model.binding.PriceResponse;
import com.example.onetrade.model.binding.TradingCalculator;
import com.example.onetrade.model.binding.TradingResults;
import com.example.onetrade.model.entities.*;
import com.example.onetrade.model.entities.enums.StateOrderEnum;
import com.example.onetrade.model.entities.enums.TransactionOperationStateEnum;
import com.example.onetrade.model.entities.enums.TransactionTypeEnum;
import com.example.onetrade.model.repostiory.CryptomonnaieRepository;
import com.example.onetrade.model.repostiory.CustomerTransactionRepository;
import com.example.onetrade.model.service.*;
import com.example.onetrade.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class SocketDataTradingConvertHandler extends TextWebSocketHandler {

    private final TwelveDataService twelveDataService;
    private final CryptomonnaieRepository cryptomonnaieRepository;
    private final ObjectMapper objectMapper;
    private final Map<String, WebSocketSession> webSocketSessions = new ConcurrentHashMap<>();
    private final TradingConfigService tradingConfigService;
    private final MarketDataService marketDataService;
    private final OrderService orderService;

    private final TransactionService transactionService;

    private final UserService userService;

    private final CustomerTransactionRepository customerTransactionRepository;

    public SocketDataTradingConvertHandler(TwelveDataService twelveDataService, CryptomonnaieRepository cryptomonnaieRepository, ObjectMapper objectMapper, TradingConfigService tradingConfigService, MarketDataService marketDataService, OrderService orderService, TransactionService transactionService, UserService userService, CustomerTransactionRepository customerTransactionRepository) {
        this.twelveDataService = twelveDataService;
        this.cryptomonnaieRepository = cryptomonnaieRepository;
        this.objectMapper = objectMapper;
        this.tradingConfigService = tradingConfigService;
        this.marketDataService = marketDataService;
        this.orderService = orderService;
        this.transactionService = transactionService;
        this.userService = userService;
        this.customerTransactionRepository = customerTransactionRepository;
    }
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
        System.out.println(session.getId() + " Connected");
        webSocketSessions.put(session.getId(), session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
        System.out.println(session.getId() + " Disconnected");
        webSocketSessions.remove(session.getId());
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        super.handleMessage(session, message);
        try {
            if (message.getPayload() instanceof String) {
                String payload = (String) message.getPayload();
                String chaineAvecGuillemets = payload;
                Pattern pattern = Pattern.compile("^\"(.*)\"$");
                Matcher matcher = pattern.matcher(chaineAvecGuillemets);
                if (matcher.find()) {
                    String chaineSansGuillemets = matcher.group(1);
                    System.out.println(chaineSansGuillemets); // Affiche : adb67920-7e1a-4693-8229-8276808ce130
                    payload=chaineSansGuillemets.toString();
                }

                Ordre order= orderService.findByToken(payload).orElseThrow(() -> new RuntimeException("Pas de commande trouvée pour le token "));
                while (order.getState()!=StateOrderEnum.END && order.getState()!=StateOrderEnum.PAID) {
                    System.out.println(payload);

                    // Récupérer la configuration du trading
                    TradingConfig tradingConfig = tradingConfigService.findById(1L)
                            .orElseThrow(() -> new IllegalStateException("La configuration du trading n'a pas été trouvée."));
                    generateMarketDataForCrypto(order.getCryptomonnaie(), tradingConfig);
                    if (tradingConfig.getProfitDirection().equalsIgnoreCase("normal")) {
                        updatePriceData();
                    }
                    //Data
                    //double quantite=order.getQuantite();
                    //double prixOuverture=order.getPrix();
                    //double prixFermeture=order.getCryptomonnaie().getPrixActuel();
                    //double leverage=order.getLeverage();
                    //controls
                    //double stopLossPercentage=order.getStopLossPercentage();
                    //double takeProfitPercentage=order.getTakeProfitPercentage();
                    //double marginPercentage=order.getMarginPercentage();
                    //calculate
                    //double profitValue;
                    //double stopLossValue;
                    //double takeProfitValue;
                    //double marginValue;
                    TradingResults tr=TradingCalculator.calculateTradingValues(order,tradingConfigService);

                    // Rechercher l'ordre dans la base de données
                    Optional<Transaction> existingTrasaction = transactionService.findByOrder(order);

                    if (existingTrasaction.isPresent()) {
                        // Utiliser la transaction existante
                        Transaction t=existingTrasaction.get();
                        //mise à jours des valeurs
                        t.setDateFermeture(LocalDateTime.now());
                        tr.setDateExecution(LocalDateTime.now());
                        tr.setCurrentValue(order.getCryptomonnaie().getPrixActuel().doubleValue());
                        t.setPrixFermeture(order.getCryptomonnaie().getPrixActuel().doubleValue());
                        t.setProfit(tr.getProfitValue());

                        transactionService.creerTransaction(t);
                    } else {
                        // Créer une nouvelle transaction
                        Transaction newTransaction = new Transaction();
                        newTransaction.setOrdre(order);
                        // Initialiser d'autres champs selon vos besoins
                        newTransaction.setPrixOuverture(order.getPrix());
                        newTransaction.setDateOuverture(order.getDateExecution());
                        //Données a actualiser
                        newTransaction.setDateFermeture(LocalDateTime.now());
                        newTransaction.setPrixFermeture(order.getCryptomonnaie().getPrixActuel().doubleValue());
                        newTransaction.setProfit(tr.getProfitValue());
                        tr.setDateExecution(LocalDateTime.now());
                        tr.setCurrentValue(order.getCryptomonnaie().getPrixActuel().doubleValue());
                        // Enregistrer la nouvelle transaction
                        transactionService.creerTransaction(newTransaction);
                    }
                    System.out.println(tr.toString());
                    session.sendMessage(new TextMessage(objectMapper.writeValueAsString(tr)));
                    // Check if profit is within the acceptable range
                    double stopLoss = tr.getStopLossValue();
                    double takeProfit = tr.getTakeProfitValue();
                    double profit = tr.getProfitValue();
                    if (order.getCryptomonnaie().getPrixActuel().doubleValue() <= stopLoss || order.getCryptomonnaie().getPrixActuel().doubleValue() >= takeProfit) {
                        // ... your code here ...
                        order.setState(StateOrderEnum.END);
                        orderService.creerOrdre(order);
                        // ... Make Deposit or Withdraw
                        TransactionOperationStateEnum state = TransactionOperationStateEnum.VALIDATE;

                        CustomerTransaction transaction = new CustomerTransaction();
                        transaction.setClient(order.getUtilisateur());
                        if(profit>0){
                            transaction.setType(TransactionTypeEnum.DEPOSIT);
                        }else{
                            transaction.setType(TransactionTypeEnum.WITHDRAW);
                        }

                        transaction.setMontant(profit);
                        transaction.setReference(order.getToken());
                        transaction.setMotif("Trading");
                        transaction.setState(state);
                        customerTransactionRepository.save(transaction);
                        // mettre a jours le solde su compte du client
                        if (transaction.getState().equals(TransactionOperationStateEnum.VALIDATE)){
                            double variation = transaction.getType().equals(TransactionTypeEnum.DEPOSIT) ? transaction.getMontant() : -transaction.getMontant();
                            order.getUtilisateur().setSolde( order.getUtilisateur().getSolde()+variation);
                            userService.saveUpdatedUserClient(order.getUtilisateur());
                        }
                        closeSessionWithError(session);
                        System.out.println("if ifififiififiififiif");
                    }
                    System.out.println("state : "+order.getState());

                    // Pause de 5 secondes
                    TimeUnit.SECONDS.sleep(5);
                }
                System.out.println("state 1 : "+order.getState());
                //handleSubscriptionRequest(session, payload);
                closeSessionWithError(session);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    private void closeSessionWithError(WebSocketSession session) {
        try {
            session.close(CloseStatus.BAD_DATA);
        } catch (IOException e) {
            System.err.println("Error closing WebSocket session: " + e.getMessage());
        }
    }
    private void generateMarketDataForCrypto(Cryptomonnaie cryptomonnaie, TradingConfig tradingConfig) {
        double prixActuel = cryptomonnaie.getPrixActuel().doubleValue();
        double variation = getRandomVariation(tradingConfig.getProfitDirection(), prixActuel);
        double nouveauPrix = prixActuel + variation;

        MarketData marketData = new MarketData();
        marketData.setCryptomonnaie(cryptomonnaie);
        marketData.setPrix(nouveauPrix);
        marketData.setDateHeure(LocalDateTime.now());

        marketDataService.enregistrerMarketData(marketData);

        // Mettre à jour le prix actuel de la cryptomonnaie
        cryptomonnaie.setPrixActuel(BigDecimal.valueOf(nouveauPrix));
        try {
            cryptomonnaieRepository.save(cryptomonnaie);
        } catch (ObjectOptimisticLockingFailureException e) {}
    }

    private double getRandomVariation(String profitDirection, double prixActuel) {
        Random random = new Random();
        double variation = (random.nextDouble() * 0.0001) * prixActuel; // Variation aléatoire entre 1% et 10%
        System.out.println("pa: "+prixActuel+" v: "+variation);
        if (profitDirection.equalsIgnoreCase("positive")) {
            return variation;
        }
        if (profitDirection.equalsIgnoreCase("negative")) {
            return -variation;
        }
        return 0;
    }
    @Scheduled(fixedRate = 5000)
    public void updatePriceData() {
        List<String> symbols = new ArrayList<>(Arrays.asList("ETH/USD", "BTC/USD", "LTC/USD", "USDC/USD", "SOL/USD", "TRX/USD", "LINK/USD", "ARB/USD", "BCH/USD"));
        for (WebSocketSession session : webSocketSessions.values()) {
            updatePrices(symbols, session);
        }
    }
    private void updatePrices(List<String> symbols, WebSocketSession session) {
        for (String symbol : symbols) {
            try {
                PriceResponse priceResponse = twelveDataService.getPrice(symbol);
                String nom = priceResponse.getName();

                // Rechercher la cryptomonnaie par son nom
                Optional<Cryptomonnaie> cryptoExistante = cryptomonnaieRepository.findByNom(nom);
                Cryptomonnaie cryptomonnaie;
                if (cryptoExistante.isPresent()) {
                    // Si la crypto existe, on la met à jour
                    cryptomonnaie = cryptoExistante.get();
                    cryptomonnaie.setSymbole(priceResponse.getSymbol());
                    cryptomonnaie.setPrixActuel(priceResponse.getClose());
                    cryptomonnaie.setExchange(priceResponse.getExchange());
                    cryptomonnaie.setMicCode(priceResponse.getMicCode());
                    cryptomonnaie.setCurrency(priceResponse.getCurrency());
                    cryptomonnaie.setDatetime(priceResponse.getDatetime());
                    cryptomonnaie.setTimestamp(priceResponse.getTimestamp());
                    cryptomonnaie.setOpen(priceResponse.getOpen());
                    cryptomonnaie.setHigh(priceResponse.getHigh());
                    cryptomonnaie.setLow(priceResponse.getLow());
                    cryptomonnaie.setClose(priceResponse.getClose());
                    cryptomonnaie.setVolume(priceResponse.getVolume());
                    cryptomonnaie.setPreviousClose(priceResponse.getPreviousClose());
                    cryptomonnaie.setChange(priceResponse.getChange());
                    cryptomonnaie.setPercentChange(priceResponse.getPercentChange());
                    cryptomonnaie.setAverageVolume(priceResponse.getAverageVolume());
                    cryptomonnaie.setIsMarketOpen(priceResponse.getIsMarketOpen());
                } else {
                    // Si la crypto n'existe pas, on la crée
                    cryptomonnaie = new Cryptomonnaie();
                    cryptomonnaie.setNom(nom);
                    cryptomonnaie.setSymbole(priceResponse.getSymbol());
                    cryptomonnaie.setSymbole(priceResponse.getSymbol());
                    cryptomonnaie.setPrixActuel(priceResponse.getClose());
                    cryptomonnaie.setExchange(priceResponse.getExchange());
                    cryptomonnaie.setMicCode(priceResponse.getMicCode());
                    cryptomonnaie.setCurrency(priceResponse.getCurrency());
                    cryptomonnaie.setDatetime(priceResponse.getDatetime());
                    cryptomonnaie.setTimestamp(priceResponse.getTimestamp());
                    cryptomonnaie.setOpen(priceResponse.getOpen());
                    cryptomonnaie.setHigh(priceResponse.getHigh());
                    cryptomonnaie.setLow(priceResponse.getLow());
                    cryptomonnaie.setClose(priceResponse.getClose());
                    cryptomonnaie.setVolume(priceResponse.getVolume());
                    cryptomonnaie.setPreviousClose(priceResponse.getPreviousClose());
                    cryptomonnaie.setChange(priceResponse.getChange());
                    cryptomonnaie.setPercentChange(priceResponse.getPercentChange());
                    cryptomonnaie.setAverageVolume(priceResponse.getAverageVolume());
                    cryptomonnaie.setIsMarketOpen(priceResponse.getIsMarketOpen());
                }

                // Sauvegarder ou mettre à jour la cryptomonnaie
                cryptomonnaieRepository.save(cryptomonnaie);

                /*try {
                    session.sendMessage(new TextMessage(objectMapper.writeValueAsString(cryptomonnaie)));
                } catch (IOException e) {
                    System.err.println("Error sending price update to session: " + e.getMessage());
                    closeSessionWithError(session);
                }*/
            }catch (Exception e){}
        }
    }
}
