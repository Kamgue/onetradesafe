package com.example.onetrade.websocket;

import com.example.onetrade.model.binding.PriceResponse;
import com.example.onetrade.model.binding.ValueMarketDate;
import com.example.onetrade.model.entities.Cryptomonnaie;
import com.example.onetrade.model.entities.MarketData;
import com.example.onetrade.model.entities.SubscriptionRequest;
import com.example.onetrade.model.entities.TradingConfig;
import com.example.onetrade.model.repostiory.CryptomonnaieRepository;
import com.example.onetrade.model.service.MarketDataService;
import com.example.onetrade.model.service.TradingConfigService;
import com.example.onetrade.model.service.TwelveDataService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@Component
public class SocketDataConnection2Handler extends TextWebSocketHandler {

    private final TwelveDataService twelveDataService;
    private final CryptomonnaieRepository cryptomonnaieRepository;
    private final ObjectMapper objectMapper;
    private final Map<String, WebSocketSession> webSocketSessions = new ConcurrentHashMap<>();
    private final TradingConfigService tradingConfigService;
    private final MarketDataService marketDataService;

    public SocketDataConnection2Handler(TwelveDataService twelveDataService, CryptomonnaieRepository cryptomonnaieRepository, ObjectMapper objectMapper, TradingConfigService tradingConfigService, MarketDataService marketDataService) {
        this.twelveDataService = twelveDataService;
        this.cryptomonnaieRepository = cryptomonnaieRepository;
        this.objectMapper = objectMapper;
        this.tradingConfigService = tradingConfigService;
        this.marketDataService = marketDataService;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
        System.out.println(session.getId() + " Connected");
        webSocketSessions.put(session.getId(), session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
        System.out.println(session.getId() + " Disconnected");
        webSocketSessions.remove(session.getId());
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        super.handleMessage(session, message);

        if (message.getPayload() instanceof String) {
            String payload = (String) message.getPayload();
            handleSubscriptionRequest(session, payload);
        }
    }

    private void handleSubscriptionRequest(WebSocketSession session, String payload) {
        try {
            /*SubscriptionRequest subscriptionRequest = objectMapper.readValue(payload, SubscriptionRequest.class);
            List<String> symbols = subscriptionRequest.getSymbols();
            //updatePrices(symbols, session);
            updatePriceData();
            generateMarketData();
            for (String symbol : symbols) {
                sendMarketDataToSessions(symbol, 5, session);
            }*/
            while (true) {
                // Le bloc de code existant
                SubscriptionRequest subscriptionRequest = objectMapper.readValue(payload, SubscriptionRequest.class);
                List<String> symbols = subscriptionRequest.getSymbols();
                // Récupérer la configuration du trading
                TradingConfig tradingConfig = tradingConfigService.findById(1L)
                        .orElseThrow(() -> new IllegalStateException("La configuration du trading n'a pas été trouvée."));
                //updatePrices(symbols, session);
                if (tradingConfig.getProfitDirection().equalsIgnoreCase("normal")) {
                    updatePriceData();
                }
                generateMarketData();
                for (String symbol : symbols) {
                    sendMarketDataToSessions(symbol, 13, session,"ETH/USD");
                }

                // Pause de 5 secondes
                TimeUnit.SECONDS.sleep(5);
            }
        } catch (IOException e) {
            System.err.println("Error deserializing SubscriptionRequest: " + e.getMessage());
            closeSessionWithError(session);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void updatePrices(List<String> symbols, WebSocketSession session) {
        for (String symbol : symbols) {
            try {
                PriceResponse priceResponse = twelveDataService.getPrice(symbol);
                String nom = priceResponse.getName();

                // Rechercher la cryptomonnaie par son nom
                Optional<Cryptomonnaie> cryptoExistante = cryptomonnaieRepository.findByNom(nom);
                Cryptomonnaie cryptomonnaie;
                if (cryptoExistante.isPresent()) {
                    // Si la crypto existe, on la met à jour
                    cryptomonnaie = cryptoExistante.get();
                    cryptomonnaie.setSymbole(priceResponse.getSymbol());
                    cryptomonnaie.setPrixActuel(priceResponse.getClose());
                    cryptomonnaie.setExchange(priceResponse.getExchange());
                    cryptomonnaie.setMicCode(priceResponse.getMicCode());
                    cryptomonnaie.setCurrency(priceResponse.getCurrency());
                    cryptomonnaie.setDatetime(priceResponse.getDatetime());
                    cryptomonnaie.setTimestamp(priceResponse.getTimestamp());
                    cryptomonnaie.setOpen(priceResponse.getOpen());
                    cryptomonnaie.setHigh(priceResponse.getHigh());
                    cryptomonnaie.setLow(priceResponse.getLow());
                    cryptomonnaie.setClose(priceResponse.getClose());
                    cryptomonnaie.setVolume(priceResponse.getVolume());
                    cryptomonnaie.setPreviousClose(priceResponse.getPreviousClose());
                    cryptomonnaie.setChange(priceResponse.getChange());
                    cryptomonnaie.setPercentChange(priceResponse.getPercentChange());
                    cryptomonnaie.setAverageVolume(priceResponse.getAverageVolume());
                    cryptomonnaie.setIsMarketOpen(priceResponse.getIsMarketOpen());
                } else {
                    // Si la crypto n'existe pas, on la crée
                    cryptomonnaie = new Cryptomonnaie();
                    cryptomonnaie.setNom(nom);
                    cryptomonnaie.setSymbole(priceResponse.getSymbol());
                    cryptomonnaie.setSymbole(priceResponse.getSymbol());
                    cryptomonnaie.setPrixActuel(priceResponse.getClose());
                    cryptomonnaie.setExchange(priceResponse.getExchange());
                    cryptomonnaie.setMicCode(priceResponse.getMicCode());
                    cryptomonnaie.setCurrency(priceResponse.getCurrency());
                    cryptomonnaie.setDatetime(priceResponse.getDatetime());
                    cryptomonnaie.setTimestamp(priceResponse.getTimestamp());
                    cryptomonnaie.setOpen(priceResponse.getOpen());
                    cryptomonnaie.setHigh(priceResponse.getHigh());
                    cryptomonnaie.setLow(priceResponse.getLow());
                    cryptomonnaie.setClose(priceResponse.getClose());
                    cryptomonnaie.setVolume(priceResponse.getVolume());
                    cryptomonnaie.setPreviousClose(priceResponse.getPreviousClose());
                    cryptomonnaie.setChange(priceResponse.getChange());
                    cryptomonnaie.setPercentChange(priceResponse.getPercentChange());
                    cryptomonnaie.setAverageVolume(priceResponse.getAverageVolume());
                    cryptomonnaie.setIsMarketOpen(priceResponse.getIsMarketOpen());
                }

                // Sauvegarder ou mettre à jour la cryptomonnaie
                cryptomonnaieRepository.save(cryptomonnaie);

                /*try {
                    session.sendMessage(new TextMessage(objectMapper.writeValueAsString(cryptomonnaie)));
                } catch (IOException e) {
                    System.err.println("Error sending price update to session: " + e.getMessage());
                    closeSessionWithError(session);
                }*/
            }catch (Exception e){}
        }
    }

    private void closeSessionWithError(WebSocketSession session) {
        try {
            session.close(CloseStatus.BAD_DATA);
        } catch (IOException e) {
            System.err.println("Error closing WebSocket session: " + e.getMessage());
        }
    }

    @Scheduled(fixedRate = 5000)
    public void updatePriceData() {
        List<String> symbols = new ArrayList<>(Arrays.asList("ETH/USD", "BTC/USD", "LTC/USD", "USDC/USD", "SOL/USD", "TRX/USD", "LINK/USD", "ARB/USD", "BCH/USD"));
        for (WebSocketSession session : webSocketSessions.values()) {
            updatePrices(symbols, session);
        }
    }
    @Scheduled(fixedRate = 5000)
    @Transactional
    public void generateMarketData() {
        // Récupérer toutes les cryptomonnaies
        List<Cryptomonnaie> cryptomonnaies = cryptomonnaieRepository.findAll();

        // Récupérer la configuration du trading
        TradingConfig tradingConfig = tradingConfigService.findById(1L)
                .orElseThrow(() -> new IllegalStateException("La configuration du trading n'a pas été trouvée."));

        // Générer les données du marché pour chaque cryptomonnaie
        for (Cryptomonnaie cryptomonnaie : cryptomonnaies) {
            generateMarketDataForCrypto(cryptomonnaie, tradingConfig);
        }
    }

    private void generateMarketDataForCrypto(Cryptomonnaie cryptomonnaie, TradingConfig tradingConfig) {
        if (cryptomonnaie == null) {
            System.err.println("Cryptomonnaie is null");
            return;
        }

        if (tradingConfig == null) {
            System.err.println("TradingConfig is null");
            return;
        }
        String profitDirection = tradingConfig.getProfitDirection();
        if (profitDirection == null) {
            System.err.println("ProfitDirection is null");
            return;
        }

        double prixActuel = cryptomonnaie.getPrixActuel().doubleValue();
        double variation = getRandomVariation(profitDirection, prixActuel);
        double nouveauPrix = prixActuel + variation;

        MarketData marketData = new MarketData();
        marketData.setCryptomonnaie(cryptomonnaie);
        marketData.setPrix(nouveauPrix);
        marketData.setDateHeure(LocalDateTime.now());

        marketDataService.enregistrerMarketData(marketData);

        // Mettre à jour le prix actuel de la cryptomonnaie
        cryptomonnaie.setPrixActuel(BigDecimal.valueOf(nouveauPrix));
        cryptomonnaieRepository.save(cryptomonnaie);
    }

    private double getRandomVariation(String profitDirection, double prixActuel) {
        if (profitDirection == null) {
            throw new IllegalArgumentException("ProfitDirection cannot be null");
        }
        Random random = new Random();
        double variation = (random.nextDouble() * 0.0001) * prixActuel; // Variation aléatoire entre 1% et 10%
        System.out.println("pa: "+prixActuel+" v: "+variation);
        if (profitDirection.equalsIgnoreCase("positive")) {
            return variation;
        }
        if (profitDirection.equalsIgnoreCase("negative")) {
            return -variation;
        }
        return 0;
    }

    public List<MarketData> getRecentMarketDataForCrypto(String cryptoSymbol, int limit) {
        Cryptomonnaie cryptomonnaie = cryptomonnaieRepository.findBySymbole(cryptoSymbol)
                .orElseThrow(() -> new IllegalArgumentException("Cryptomonnaie avec le symbole " + cryptoSymbol + " non trouvée."));

        List<MarketData> recentMarketData = marketDataService.findByCryptomonnaieOrderByDateHeureDesc(cryptomonnaie, PageRequest.of(0, limit))
                .getContent();

        return recentMarketData;
    }
    public void sendMarketDataToSessions(String cryptoSymbol, int limit, WebSocketSession session,String compaireCryptoSymbole) {
        List<MarketData> recentMarketData = getRecentMarketDataForCrypto(cryptoSymbol, limit);
        List<MarketData> compaireDefaultMarketData = getRecentMarketDataForCrypto(compaireCryptoSymbole, limit);

        ValueMarketDate valueMarketDate=new ValueMarketDate(recentMarketData,compaireDefaultMarketData);

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(valueMarketDate)));
        } catch (IOException e) {
            System.err.println("Error sending price update to session: " + e.getMessage());
            closeSessionWithError(session);
        }
    }
}