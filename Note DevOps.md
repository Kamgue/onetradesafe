Avantages du DevOps
Pour les entreprises qui suivent les pratiques DevOps, les avantages commerciaux et techniques sont évidents et la plupart contribuent à améliorer la satisfaction des clients :

Accélération et amélioration l'intergration et le deploiement des produits
Résolution plus rapide des bugs 
Stabilité accrue des environnements d'exploitation
Meilleure utilisation des ressources
Automatisation accrue
Meilleure visibilité sur les résultats du système
Innovation renforcée




Chaîne d'outils DevOps

Planification. 
Code. 
Création. 
Test. 
Déploiement. 
Exploitation. 
Supervision. 


Pratiques DevOps

Développement continu. 
Tests continus. 
Intégration continue. 
Livraison continue.
Déploiement continu. 
Surveillance continue. 
Infrastructure-as-code. 




token acces sonarCloud
	8c18b04f9d8bd3974bc87365728a12a3d8e51a74





Qpprendre pour qpprendre
Etqt dexprit fixe Vs Etat d'esprit de croissance

Plan dapprentissage
	Objectif Strategique : Prendre en main le concept de DevOps et ces outils Dans en 5 mois
	Micro Objectifs
	Reesource d'apprentissage
	Emploi du temps

Bien le bonsoir à tous, je réponds au nom de KOUAM KENMEUGNE Franky Brice, j'ai 25 ans, Ingénieur des travaux en informatique de gestion et analyste programmeur, j'ai terminé mon second cycle d'ingénieur de conception en génie logiciel l'année dernière à l'école National Supérieur Polytechnique de Maroua. Passionné par le développement d'applications, l'analyse de données et les nouvelles technologies, j'ai toujours été très déterminé et motivé quant il s'agit d'apprendre de nouvelle chose. Ma plus grande valeur est d'aider mon entourage,
J'ai 1 an d'expérience sur le terrain ; passé par Amla Cameroun et Big Data Solution j'ai eu l'opportunité et la chance de me retrouver à Advance IT où je compte bien y rester de part sa vision et des multiples devis dont je suis confronté chaque jour, ce qui me permet de redoubler d'efforts et d'acquérir de l'expérience J'avais depuis longtemps entendu parler de DevOps et bien qu'enthousiaste et curieux d'en apprendre davantage, l'occasion ne s'était pas présentée à moi jusqu'à aujourd'hui et je remercie l'administration d'Advance IT pour cela. Ceci étant dit, j'aimerais profiter de cette formation pour en apprendre sur le DevOps et monter en compétences dessus.
Comme espérance et aspiration de la formation, j'aimerai qu'à la fin de cette formation je puisse mettre au profit de l'entreprise les connaissances acquises et améliorer la qualité des logiciels produits, faciliter et accélérer le processus d'intégration et de déploiement de nos solutions, simplifier les intégrations de nouvelles fonctionnalités et par conséquent augmenter le niveau de satisfaction de nos clients.








Gérez votre projet informatique facilement
Objectif :
	à différencier les tests,
	les valeurs, les principes et les méthodes Agiles,
	à gérer un projet en utilisant la méthodologie SCRUM,
	à créer et gérer un Backlog.



Cadrez un projet avec un cahier des charges

Avant tout chose faut planifier le projet : Pourquoi planifier un projet ?
	Communication : Avant tout, car cela vous permet d’aller dans le détail du programme demandé. Avant de planifier, il faut en effet aller plus loin que le cahier des charges et approfondir les fonctionnalités. Nous allons donc engager un dialogue avec le client en amont afin de valider avec lui que nous allons bien réaliser ce qu’il souhaite. Vous vous rendrez vite compte que cela vous fera gagner du temps (et éviter les cheveux blancs).
	Sérénité : Que vous travailliez seul ou en équipe, planifier un projet vous aide également à vous concentrer sur l’essentiel pendant la phase de production. Vous n’avez pas envie de vous demander tous les matins : “que manque-t-il à mon programme ?”, n’est-ce pas ?
	Qualité : Enfin, vos développeurs produiront du code de meilleure qualité car leur coordination sera facilitée.

Il existe deux grandes familles de méthodologies de projet : 
	celles dites séquentielles et les méthodes agiles.

Un projet informatique est développé conjointement par deux équipes : la maîtrise d’ouvrage  et la maîtrise d’oeuvre.

les différentes phases et les documents produits dans la realisation d'un projet :
	Etape 1 : définition du projet : les spécifications fonctionnelles
	Etape 2 : conception de l’architecture : les spécifications techniques, elles ont pour objectif de définir le cadre technique du projet. 
	Etape 3 : écriture du code
	Etape 4 : recette : Les tests (tests unitaire ou manuelle)
	
Approche Agiles
	Une approche itérative et incrémentale (et adaptative)
	Des équipes responsabilisées
	Un cérémonial minimal
	Des utilisateurs aux besoins changeants


Les valeurs du manifeste sont les suivantes :
	Les individus et leurs interactions sont plus importants que les processus et les outils,
	Un logiciel qui fonctionne est mieux qu’une documentation exhaustive,
	Collaborer avec les clients est préférable à la négociation contractuelle,
	S’adapter au changement est mieux que de suivre un plan.

Les pratiaues les plus utiliser dans la methode agiles
	User stories									: cest une  fonctionnalité écrite du point de vue de l’utilisateur et très succincte. Elle raconte ce que fera l’utilisateur dans notre programme sous forme d’histoire.
	Pair Programming (programmation en binôme)	: Un écran, un code, deux programmeurs. Le binôme se met d’accord sur une fonctionnalité à développer et le fait ensemble. Les avantages sont multiples : meilleur code, collaboration renforcée et communication facilitée.
	Continuous Integration (intégration continue) : Chaque fonctionnalité est intégrée à la précédente, ajoutée au programme en production
	Acceptance testing (tests d’acceptation)	: Avant de passer à la story suivante, quelqu’un doit valider que celle-ci est bien terminée.
	Iteration Planning / Planning Game / Sprint Planning : a chaque nouvelle User Story, L’équipe se réunit et estime la durée de réalisation de chaque User Story.
	Daily Stand-Up Meeting / Daily Scrum	: L’équipe se réunit chaque jour et chaque membre présente, très rapidement, ce qu’il a fait la veille, ce qu’il fera aujourd’hui et un point bloquant. 
	
	
	
backlog
	connexion,
	chat,
	calendrier, 
	gestionnaire de tâches, 
	tableau de bord

Chaque mise en production est appelée release et est constituée de plusieurs sprints

Pour chaque Story 
	créer des spécifications fonctionnelles,
	concevoir l’architecture de la story,
	coder et tester.


En général un tableau de backlog contient les colonnes suivantes :
	Product backlog : les fonctionnalités qui ont été priorisées par le Product Owner et qui sont en attente de développement.
	Development : User Stories en développement
	Done : User Stories qui ont été mises en production.
	
	
En résumé, une story passe par les phases suivantes :
	Proposition : un jour, une personne suggère une nouvelle fonctionnalité.
	Acceptée : le Product Owner accepte la proposition et l’ajoute dans le backlog.
	Estimée : l’équipe estime la taille de la story.
	Prête : la story est écrite et estimée. Elle est en attente de développement.
	En cours : l’équipe est en train de la développer.
	Fini : elle est en ligne !
	
	
	
	
 Préparer le lancement du Sprint.
	identifier et découper les tâches à traiter pendant le Sprint ;
	quantifier l’effort alloué pour le traitement de chaque ticket ;
	démarrer votre Sprint.
 
 Dans un product backlog ont retrouve des user stories
 
 En résumé
 La préparation d’un Sprint repose sur différentes étapes réalisées en équipe :
	découper techniquement les tâches du Product Backlog lors du Backlog Refinement ;
	estimer la complexité technique ou le temps nécessaire pour traiter chaque ticket grâce au Poker Planning ;
	prioriser et s’engager sur le traitement d’un nombre donné de tickets pendant le Sprint Planning ;
	communiquer ces objectifs aux parties prenantes.
	
	
	
	
	
	un projet selon lq philosophie CD/CI n'est demarrer que lorsque toute l'architecture est 
	mise en oeuvre et aue le client sit capable de savoir ou sera visible son projet
	
	produire un dashbord parlant avec des elts strategique
	presente le nombre de commit realiser par chaque dev
	
	------------------------------------------------------------------------
	
	Rapport reunion DevOps presentielle du 10/04/2022
	
	les wikis : lieu dechange entre les participant au projet
	
	les boards : se presentatant sur forme de backlog par equipes
	
	la gestion des utilisateurs 
	
	documenter les fontionnalites user-stories dans le wiki 
	
	dressser un plan d'action a chaque debut de projet
	
	prendre en main git
	
	
	sprint 
	
	
	
	S’assure que les codes fournis par les développeurs suivent un style. // un format de code, 
	
	les linters : sont les elements quil faut configure, les contraintes sur la redaction du code
	pour dev un ticket est fini lorsque
	son code verifi le tiket
	son code ne presente pas de warning
	
	un code ne peut etre fusionner que si tout les tests son verifier
	
	mettre en place un hook qui permette de nettoyer toutes les autres branche local et distance des taches 
	
	
	
	Réalisez un cahier des charges fonctionnel
	
	Objectifs pédagogiques

		Découvrir le rôle de la documentation projet
		Préparez un cahier des charges fonctionnel
		Utiliser la méthodologie agile pour communiquer la portée d'un projet 
	
	Découvrez à quoi sert la documentation
		Informer les intervenants
		Définir l'interaction entre plusieurs systèmes
		Faciliter la communication entre les équipes distantes
		Soutenir le développement agile
		Préparer des audits de projets et/ou de systèmes
		
	une bonne planification est essentielle à la réussite d'un projet;
	les méthodologies agiles peuvent aplatir la courbe du coût du changement;
	l'objectif premier de toute documentation est la communication;
	
	 	
	# 1. Objectifs 
	## a) Objectif principal
	- Développer une plateforme qui permettra à tout élève, où qu’il soit, d’accéder à un soutien scolaire à distance. 
	## b) Objectif spécifique
	- Mise en place d'un système d'authentification a la plateforme
	- Mise en place d'un tableau de bord regroupe toutes les information importante pour un meilleur suivi
	- Conception et implémentation d'un système de messagerie
	- Conception et implémentation d'une système de gestion des calendriers
	- Conception et implémentation d'un système de gestion des tâches

	# 2. Cibles
	- Bénévoles
	- Elèves

	# 3. À quoi ressemblera le livrable ?
	Répondre à cette question vous aidera à définir le backlog  qui est la liste des tâches qui doivent être accomplies pendant le sprint pour atteindre le but du sprint.
		
	# 4. Qui a besoin du livrable ? 
	 

	# 5. Quand en a-t-on besoin ?
	
	
	
	
	
 
 
 
 ---------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------
		Projet 3 : Mettre en place une démarche d’intégration continue
 ---------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------
	
	
	mettre en place une démarche d’intégration continue
	
	permettre aux dev de travailler en symbiose et à converger vers le même objectif qualité dans le processus de production 
	
	ensemble cohérent de pratiques à adopter par les développeurs pour garantir la qualité de code
	 	
	etape de mise en place d'un pepiline
		les linters : definition des Styles de code
		tests unitaires
		scans des vulnerabilites
		tests d'integrations
		build (compilation du code source)
		analyse static du code (sonar)
		test Coverte : couverture du code ( cas des structure conditionnel)
		constructions des artefacts
		outils pour tests IHM
		
		
		Qu'est-ce que l'intégration continue ?
		L'intégration continue (CI) désigne la pratique qui consiste à automatiser l'intégration des changements de code réalisés par plusieurs contributeurs dans un seul et même projet de développement. 
		
		
	
	L'intégration continue va se faire en 5 étapes :

		1 Planifiez votre développement.
			definition de l'ensembles des taches du projets

		2 Compilez et intégrez votre code.
			Le contrôle de code source
			presense d'un orchestrateur

		3 Testez votre code.
			Les tests unitaires

		4 Mesurez la qualité de votre code.
			utilisation des linters
			utilisant de sonar Lint

		5 Gérez les livrables de votre application.
		
	------------------------------------------------------
	
	planing pour la mise en place d'une integration continue
	
		1 Planification du développement sur Azure DevOps.
			.definition de l'ensembles des taches du projets
		
		2 Installation et configuration des styles de code
		
		3 Redaction des tests unitaires
		
		4 Mesure de la qualité du code grace aux linters et a SonarLint
		
		5 rédaction des wikis expliquant la procédure à suivre pour la réalisation d'un tiket
		
	
	
	
	
	
	
	
	
	
	
	
	
	-------------------------------------------------
	le CI /CD permet de :
	
	accélérer le Time-to-Market (le temps de développement et de mise en production d'une fonctionnalité) ;

	réduire les erreurs lors des livraisons ;

	assurer une continuité de service des applications.
	
	
	
	
	Les les hooks : 
	pour activer les hook les renommer en enlevant .sample dessus
	-> pre-commit
	Execute avant uncommit pour effectuer certaines verification, et valide ou non le commit effectuer
	cela peut etre contourne avec la commande : 	git commit --no-verify
	
	
	
	
	
	
	
	Demarche d'integration Continue
	
	
	Configuration de l'environnement de travail par un nouveau Dev
	- Installer Node version 14
	- Installer Angular CLI 11
	- Installer et configurer Jdk 11
	- Installer et configurer maven 3.8
	- Installer de l'IDE de development (VsCode / Intelij)
	- installer les plugin suivant : ESLINT, SonarLint, TsLint
	- cloner le projet
	- creer une nouvelle branche fille de la branch principale development avec le code code du ticket ; Exple : Task-01
	- Vous pouvez commencer a realiser votre tache
	- Apres realisation du ticket envoyer votre code en ligne.
	- Se connecter sur le projet Azure DevOps et creer un pull Request : Selectionner au moins deux  personnes qui doivent valider le code.
	- lors de la verification de votre pull request par une autre personne tout les commentaires doivent etre marquer comme resolus
	
	
	
	- Affiner la tache (estime/ decrire) de maniere a ce quelle soit bien comprehensible
	- Deplacer la tache a EN COURS dans le Board
	- Creer une branche avec le nom de la tache
	- Merger la nouvelle branche creer avec la branche development
	- L'equide de test valide son travail(la branche nouvellement creer)
	- creer un pull request
	- On approuvre
	- preparer une nouvelle release avec la tache
	- la tache de sera valide qu'a la production de prochaine release
	
	Branches principales
		- working 
		- master
		- release/version
		- pre-release(test)
		
	Branches secondaire
		- branche avec le nom de chaque developpeur
	
	Demarche a suivre par un developpeur lorsqu'il prend une nouvelle tache
	1) Selectionner une tache sur JIRA
	2) Affiner la tache (estime/ decrire) de maniere a ce quelle soit bien comprehensible
	3) Deplacer la tache a EN COURS dans le Board
	4) Se mettre a jour par rapport a la branche working
	5) Chaque commit doit posseder le code de tache JIRA suivi d'un commentaire
	6) Avant chaque push se rassurer que le build passe correctement
	7) A la fin de chaque tache ou a la demande, mettre a jour la branche working (les verifications du code via des hooks doivent etre faites)
	
	Lors de la realisation de chaque Sprint
	8) Pour faire des tests sur les taches deja realiser, creer un pull request de la branche working pour la branche pre-release
	9) une fois les test valide a la fin de chaque Sprint creer un pull request de la branche pre-release vers la branche release
	10) les patch de production sont generer sur la branche release
	
		
	Note docker
	StateLees : l'application stocke un etat
	StateFull l'application ne stocke pas d'etat
	wsl --install
	
	Optimisez votre déploiement en créant des conteneurs avec Docker
	
	Découvrez les conteneurs
	les conteneurs existent depuis plus longtemps que Docker. OpenVZ ou LXC sont des technologies de conteneur qui existent depuis de nombreuses années.
	Docker, une amélioration de LXC
		les conteneurs 
		Ne réservez que les ressources nécessaires
		Démarrez rapidement vos conteneurs
		Donnez plus d'autonomie à vos développeurs
		
		
		Démarrez un serveur Nginx avec un conteneur Docker
		docker run
		nous allons aller plus loin avec celui-ci. Nous allons lancer un conteneur qui démarre un serveur Nginx en utilisant deux options : 
		docker run -d -p 8080:80 nginx
		Vous pourriez aussi avoir besoin de "rentrer" dans votre conteneur Docker pour pouvoir y effectuer des actions. Pour cela, vous devez utiliser la commande 
		docker exec -ti ID_RETOURNÉ_LORS_DU_DOCKER_RUN bash  .
		Dans cette commande, l'argument -ti permet d'avoir un shell bash pleinement opérationnel. Une fois que vous êtes dans votre conteneur, 
		vous pouvez vous rendre, via la commande cd /usr/share/nginx/html  , dans le répertoire où se trouve le fichier index.html  , 
		pour modifier son contenu et voir le résultat en direct à l'adresse http://127.0.0.1:8080 
		
		Arrêtez votre conteneur Docker
		docker stop ID_RETOURNÉ_LORS_DU_DOCKER_RUN
		
		Maintenant que votre conteneur Docker a été arrêté, vous pouvez le supprimer avec la commande
		docker rm ID_RETOURNÉ_LORS_DU_DOCKER_RUN
			
		Récupérez une image du Docker Hub
		docker pull hello-world			-- node/other images
		
		Affichez l'ensemble des conteneurs existants
		docker ps
		
		Vous pouvez aussi voir l'ensemble des images présentes en local sur votre ordinateur, avec la commande 
		docker images -a
		
		pour demarrer une image en interactif
		docker run -it node
		
		Comment nettoyer mon système
		Après avoir fait de nombreux tests sur votre ordinateur, vous pouvez avoir besoin de faire un peu de ménage. Pour cela, vous pouvez supprimer l'ensemble des ressources manuelles dans Docker.
		Ou vous pouvez laisser faire Docker pour qu'il fasse lui-même le ménage. Voici la commande que vous devez utiliser pour faire le ménage : 
		docker system prune
		Celle-ci va supprimer les données suivantes :
			l'ensemble des conteneurs Docker qui ne sont pas en status running ;
			l'ensemble des réseaux créés par Docker qui ne sont pas utilisés par au moins un conteneur ;
			l'ensemble des images Docker non utilisées ;
			l'ensemble des caches utilisés pour la création d'images Docker.
		
		comment creer sa propre image docker
		creer un fichier nommer Dockerfile
		FROM debian:9 / alpine3.14 	// pour specifier l'image de base que nous voulons utiliser
		y place ces commande

		une fois terminer 
		construire l'image avec la commande 
		docker build -t mon_image .
		
		docker tag frontend_digit_achat:live iforce5/frontend_digit_achat:live && docker push iforce5/frontend_digit_achat:live
		docker tag image_source:tag image_destination:tag Exple :	docker tag backend_digit_achat backend_digit_achat:1.2
		exporter une image en tar : 	
			docker save -o frontend_digit_achat.tar frontend_digit_achat:latest
		importer une image tar : 	
			docker load -i frontend_digit_achat.tar
		
		Créez votre fichier .dockerignore
		Notre Dockerfile est maintenant prêt à fonctionner ! Cependant, il nous reste encore quelques petites modifications à faire.
		À la racine de votre projet (soit à côté de votre fichier Dockerfile), vous devez créer un fichier .dockerignore qui contiendra les lignes suivantes :
			node_modules
			.git
			
			
		Nous allons maintenant la publier sur le Docker Hub
		-Voici la première commande que vous allez utiliser : 
		docker tag ocr-docker-build:latest YOUR_USERNAME/ocr-docker-build:latest  . 
			Celle-ci va créer un lien entre notre image ocr-docker-build:latest créée précédemment et l'image que nous voulons envoyer sur le Docker Hub YOUR_USERNAME/ocr-docker-build:latest  .
		-Exécuter la dernière commande nécessaire pour envoyer votre image vers le Docker Hub. Pour cela, vous allez exécuter la commande
		docker push YOUR_USERNAME/ocr-docker-build:latest
		
		Exple :
		docker tag brice_image:latest kouamfranky/brice_image:latest
		docker push kouamfranky/brice_image:latest
		
		**** Password : PoupinetteDocker
		--
		docker tag gateway:latest iforce5/gateway:latest
		docker push iforce5/gateway:latest
		--
		docker tag gateway:dev iforce5/gateway:dev
		docker push iforce5/gateway:dev
		*****
		--
		docker tag eureka-service:dev iforce5/eureka-service:dev
		docker push iforce5/eureka-service:dev
		*****
		--
		docker tag authentification-service:dev iforce5/authentification-service:dev
		docker push iforce5/authentification-service:dev
		
		Pour la solution du barbu, vous devez utiliser la commande docker search ; par exemple, si on recherche une image Nginx :
		docker search nginx
		
		En résumé
			Vous savez maintenant envoyer votre image sur le Docker Hub, et vous êtes aussi capable de rechercher des images sur celui-ci.
			Retenez ces commandes importantes :
			docker push qui vous permet d'envoyer vos images locales sur une registry ;
			docker search qui vous permet de rechercher une image sur votre registry.
		
		resumer
		-Vous connaissez maintenant la différence entre conteneur et machine virtuelle ; vous avez ainsi pu voir les différences entre la virtualisation lourde et la virtualisation légère.
		-Un conteneur doit être léger, il ne faut pas ajouter de contenu superflu dans celui-ci afin de le démarrer rapidement, mais il apporte une isolation moindre. À contrario, les machines virtuelles offrent une très bonne isolation, mais elle sont globalement plus lentes et bien plus lourdes.
		les conteneurs Docker sont différents des conteneurs Linux (LXC), car ils sont par définition immuables et stateless ;
		Docker est utilisé à tous les niveaux de l'infrastructure (CI / Développement / Production) ; 
		Différence entre Docker Enterprise et Docker CE/Desktop. 
			Docker Community Edition (Linux seulement) ;
			Docker Desktop (Mac ou Windows) ;
			Docker Enterprise (Linux seulement).
	En résumé
		Pour créer une image Docker, vous savez utiliser les instructions suivantes :

		FROM qui vous permet de définir l'image source ;
		RUN qui vous permet d’exécuter des commandes dans votre conteneur ;
		ADD qui vous permet d'ajouter des fichiers dans votre conteneur ;
		WORKDIR qui vous permet de définir votre répertoire de travail ;
		EXPOSE qui permet de définir les ports d'écoute par défaut ;
		VOLUME qui permet de définir les volumes utilisables ;
		CMD qui permet de définir la commande par défaut lors de l’exécution de vos conteneurs Docker.
 
	En résumé
		Vous connaissez maintenant les commandes principales pour utiliser une stack Docker Compose. Voici les commandes les plus importantes :
		docker-compose up -d vous permettra de démarrer l'ensemble des conteneurs en arrière-plan ;
		docker-compose ps vous permettra de voir le statut de l'ensemble de votre stack ;
		docker-compose logs -f --tail 5 vous permettra d'afficher les logs de votre stack ;
		docker-compose stop vous permettra d'arrêter l'ensemble des services d'une stack ;
		docker-compose down vous permettra de détruire l'ensemble des ressources d'une stack ;
		docker-compose config vous permettra de valider la syntaxe de votre fichier docker-compose.yml  .

	En résumé
		Vous savez maintenant utiliser les commandes de base de Docker Compose, et créer un fichier docker-compose.yml pour orchestrer vos conteneurs Docker.
		Pour rappel, voici les arguments que nous avons pu voir dans ce chapitre :
		image qui permet de spécifier l'image source pour le conteneur ;
		build qui permet de spécifier le Dockerfile source pour créer l'image du conteneur ;
		volume qui permet de spécifier les points de montage entre le système hôte et les conteneurs ;
		restart qui permet de définir le comportement du conteneur en cas d'arrêt du processus ;
		environment qui permet de définir les variables d’environnement ;
		depends_on qui permet de dire que le conteneur dépend d'un autre conteneur ;
		ports qui permet de définir les ports disponibles entre la machine host et le conteneur.
	
 
	demarrer jenkins
	docker run --name jenkins --rm -p 8080:8080 jenkins/jenkins:latest
	
	acceder a mon conteneur  depuis mon cmd : 
	docker exec -it jenkins_p3 bash
	
	Accéder aux journaux Docker
	docker logs <docker-container-name>
	
	Comme mentionné ci-dessus , votre <docker-container-name>peut être obtenu à l'aide de la commande 
	docker container ls 
	
	Ansible est un auchestrateur
	
	
	-------------------------------------------------------------------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------------------------------------------------------------------
	FORMATION JENKINS
	---------------------------
		
	JENKINS - 2. Premier Job
		creation des jobs
	JENKINS - 3. Configuration 
		presentation de l'interface
		configuration du systeme
		config de la securite globale
		config du nombre de lanceurs de job 
		config de la periode d'attente d'un job
		
	JENKINS - 4. Users & Roles 
		gestion des utilisateur (creation des user et role, affectation des role aux users)
		effectuer ici avec un plugin : Role based Autorization Strategy
	JENKINS - 5. Trigger & Remote url 
	les trigger nous permettent ici de demarrer des jobs les uns apres les autres
		trois type de trigger
			- en cas d'echec
			- en cas de success
			- dans tout les cas
			- lancement distant, via une url
	
	JENKINS - 6. Planifications & Crons 
		parametre de declenchement des builds
		Comme tout bon scheduler, Jenkins nous permet de planifier à fréquence régulière nos jobs
	JENKINS - 7. Paramètres et Jobs 
		Jenkins permet de créer des jobs. Grâce aux paramètres au lancement des jobs, vous allez pouvoir proposer à l'utilisateur de saisir des éléments qui pourront être réutilisés sous forme de variable lors du build.
		
	JENKINS - 8. Premier build, run et test 
		Jenkins est souvent utilisé dans les pipelines pour réaliser une combinaison d'actions que l'on appelle build-run-test. Dans cette vidéo nous allons simplement voir ce principe d'une manière ultra simpliste qui n'est en rien un modèle du genre. Il s'agit simplement d'aborder cette notion.
		
	JENKINS - 9. Plugin Git
		presentation de comment cloner un depot git et faire des operation dessus, de facon native ou en utilisans le plugin git
	
	JENKINS - 10. Comment mettre en place un trigger Git ? // permet de faire un pull a chaque changement sur le depot
		trigger : check à intervales réguliers 
		Ce qui déclenche le build : Scrutation de l'outil de gestion de version 
		ajouter l'interval
		
	JENKINS - 11. Git push automatique 
	https://www.youtube.com/watch?v=l4SXMHbyKYM&list=PLn6POgpklwWr19VXuoVgIr32HCu0MGNt9&index=12&ab_channel=xavki
	apres validation du build nous pouvons poussez sur le depot
	
	JENKINS - 12. Les Vues
	Jenkins est pratique mais au bout d'un moment il se peut que vous disposiez de beaucoup de jobs. Du coup il va devenir important de les organiser. C'est le but des vues. Vous allez pouvoir les personnaliser et configurer à votre guise.
	objectif : organiser le classement des jobs 
	soit une vue personnaliée 
	soit des vues de classement 
	permet de filtrer les files de lanceurs et de constructions  
	peut être alimentée par une regex de filtre (Java_ ...)
	
	JENKINS - 13. Plugin : pipeline delivery
	Jenkins permet de faire des jobs en cascade. Celà peut être un inconvénient pour la visualisation et les résultats de ces builds successifs. Pour faciliter ce travail, il existe un plugin qui va nous aider : delivery pipeline.
	Qui nous permet d'avoir un visuel et suivi claire des jobs dependant des uns des autres
	
	JENKINS - 14. Maven : gestionnaire de dépôts/paquets
	Jenkins est très lié à l'univers de java. Ainsi, il est possible d'utiliser des build spécifique aux projets maven. Packager son projet devient très facile et ce build peut ensuite être utilisé par d'autres jobs.
	
	
	
	
		
		
		
		
		Infos Docker
		https://odeven.fr/docker-prendre-en-main-le-gestionnaire-de-conteneurs/
		
		Postule Job
		https://www.jeffersonfrank.com/fr/apply/Developpeur%20Java%20Sud_1668684883?jobRef=Developpeur+Java+Sud_1668684883
		
		
			
	
	---------------------------------------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------------------------------------
	Configurer un reverse proxy avec nginx
	
	1- copier le fichier de configuration default.conf de notre conteneur pour notre machine physique
		docker cp tp-rp-docker-snake-1:/etc/nginx/conf.d/default.conf .
	2- configurer le fichier
	location /sample {
		proxy_pass http://192.168.246.131:8080/sample;
	}
	3- copier le fichier configurer de notre machine physique pour notre conteneur
		docker cp default.conf tp-rp-docker-snake-1:/etc/nginx/conf.d/
	4- Ensuite, validez et rechargez la configuration du proxy inverse Docker Nginx :
		docker exec -it tp-rp-docker-snake-1 bash
		docker exec tp-rp-docker-snake-1 nginx -t
		docker exec tp-rp-docker-snake-1 nginx -s reload
		
		
	configuration d'un reverse proxy depuis dockerfile
	
	1 création d'une image de proxy inverse
	
	
	2 installation de bash
	
	
	
	docker build -t reverseproxy .
	
	
	Créer et récolter des informations d'un réseau Docker 
		docker network create --driver bridge reseau-local
	lister les réseaux docker
		docker network ls
	récolter des informations sur le réseau docker
		docker network inspect reseau-local
		
	
	taxiride.iforce5.com	http://127.0.0.1:8081		down
	aurore.iforce5.com		http://127.0.0.1:8096		down
	auroreportail.iforce5.com	http://127.0.0.1:8097	up
	demo.iforce5.com		http://127.0.0.1:8091		down	
	dtcweb.iforce5.com		http://127.0.0.1:8092		down
	ivindo.iforce5.com		http://127.0.0.1:8090		down
	oncdc.iforce5.com		http://127.0.0.1:8194		up
	rendezvousservices.iforce5.com	http://127.0.0.1:8091 	down
	
	https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04
	
// création des répertoire pour sauvegarder les certificat et donner les droit 
sudo mkdir -p /var/www/html/domain.com/.well-known
sudo chown -R $USER:$USER /var/www/html/domain.com/.well-known
sudo chown -R $USER:$USER /var/www/html/domain.com
sudo chmod -R 755  /var/www/html/domain.com

// Ajout de la config du certificat dans la conf nginx
location /.well-known {
    alias /var/www/html/domain.com/.well-known;
  }
  
  // similer le chalenge avec le dry-run -- cest le well-known qui permet de faire la similation (verifie au prealable si le sous domaine fonctionne)
  sudo certbot certonly --webroot --webroot-path /var/www/html/domain.com/ -d domain.com --dry-run
  si le dry-run marche alors 


configuration la redirection du http en https
location / {
    rewrite ^ https://$host$request_uri? permanent;
  }
  
 // traiter toute les requettes https
 server {
 listen 443 ssl;
 server_name domain.com ;
 ssl_certificate     /etc/letsencrypt/live/domain.com/fullchain.pem;
 ssl_certificate_key /etc/letsencrypt/live/domain.com/privkey.pem;
 ssl_stapling on;
 ssl_stapling_verify on;
}

pour le renouvelle du certificat ssl 
certbot renew crown job

tester la config de nginx avec sudo ngnix -t

configurer un cron job qui renouvelle automatiquement le certificat



Deploiement BACM

FRONTEND
build d'une nouvelle version de l'image
	docker build -t frontend_digit_achat:bacm-1.0.1 .
constructions de la nouvelle image pour le docker hub de iforce5
	docker tag frontend_digit_achat:bacm-1.0.0 iforce5/frontend_digit_achat:bacm-1.0.0
	docker tag frontend_digit_achat:brice iforce5/frontend_digit_achat:brice
envoi de la mise a jour sur le docker hub
	docker push iforce5/frontend_digit_achat:bacm-1.0.0
	docker push iforce5/frontend_digit_achat:brice
	
	docker tag frontend_digit_achat:2.0.0 iforce5/frontend_digit_achat:latest
	docker tag frontend_digit_achat:demo iforce5/frontend_digit_achat:demo
	docker tag frontend_digit_achat:demo iforce5/frontend_digit_achat:demo
	docker push iforce5/frontend_digit_achat:demo
	
	
	docker tag backend_digit_achat:demo iforce5/backend_digit_achat:demo
	docker push iforce5/backend_digit_achat:demo

recap
	docker build -t frontend_digit_achat:bacm-1.0.1 . && docker tag frontend_digit_achat:bacm-1.0.1 iforce5/frontend_digit_achat:bacm-1.0.1 && docker push iforce5/frontend_digit_achat:bacm-1.0.1

BACKEND 
	docker build -t backend_digit_achat:1.0.3 . && docker tag backend_digit_achat:1.0.3 iforce5/backend_digit_achat:1.0.3 && docker push iforce5/backend_digit_achat:1.0.3
	
	docker tag backend_digit_achat:1.0.3 iforce5/backend_digit_achat:1.0.2 && docker push iforce5/backend_digit_achat:1.0.2

	build docker compose file en allevant les conteneurs orphelins de ce dernier
	docker compose -f docker-compose-live-2.0.0.yml up -d --build  --remove-orphans





Ecouter les modif d'un fichier de logs
tail -f spring.log

demarrer le Jar sur le VPS
sshpass -p 2o18@dvAncEItVPs*  ssh root@142.44.162.120 'java -jar -Dspring.profiles.active=live /root/projects/cima/cimaapp-1.jar --server.port=8090'
sshpass -p 2o18@dvAncEItVPs*  ssh root@142.44.162.120 'java -jar -Dspring.profiles.active=prod /root/projects/aurore/aurore-portail-1.jar --server.port=8097'

sonarqube local
http://192.168.100.140:9000
admin
admin2019

Gitlab local
root
mimba2019
http://192.168.100.140:8088

gmail iforce5
If5.forge@gmail.com
@d20mi21n



iforce5forge / If0rce@2202 / https://gitlab.com/users/sign_in
access_token_gitlab_iforce : glpat-zxqUpe7qw-WRX4xHJUBK


#PUSH
docker tag digeste-frontend_digit_achat:latest iforce5/digeste-frontend_digit_achat:latest && docker push iforce5/digeste-frontend_digit_achat:latest
docker tag digeste-backend_digit_achat:latest iforce5/digeste-backend_digit_achat:latest && docker push iforce5/digeste-backend_digit_achat:latest
tout en un :
docker tag digeste-frontend_digit_achat:latest iforce5/digeste-frontend_digit_achat:latest && docker push iforce5/digeste-frontend_digit_achat:latest && docker tag digeste-backend_digit_achat:latest iforce5/digeste-backend_digit_achat:latest && docker push iforce5/digeste-backend_digit_achat:latest
#PULL
docker pull iforce5/digeste-frontend_digit_achat:latest && docker pull iforce5/digeste-backend_digit_achat:latest && docker compose -f ./digeste/docker-compose-live-3.0.0.yml up --build

user_id;created_at;created_by;updated_at;updated_by;version;account_status;account_suspended;adresse;agence_user;code_iso_lang;display_name;email;is_enabled;last_connection_date;last_request_date;nom;password;prenom;right_provider;provider_user_id;secret_code;suspension_reason;telephone1;telephone2;username;using_two_fa;validated
395;2023-10-10 13:00:42;SYSTEM;2023-10-10 13:00:42;SYSTEM;1;\N;0;\N;\N;FR;MANYANYE RUTH ELODIA;elodia.mouto@banqueatlantique.net;1;\N;\N;MOUTO;$2a$10$iQSyCrTsQqCX9WBmakk3muTR2UKGt92wH2caHdLgHJAJkE4GTSKfK;MANYANYE RUTH ELODIA;local;\N;\N;;;;BACM0490;0;0

user Akwa : elodia.mouto@banqueatlantique.net
pass user : $2a$10$iQSyCrTsQqCX9WBmakk3muTR2UKGt92wH2caHdLgHJAJkE4GTSKfK
Chef : estelle.pone@banqueatlantique.net
pass ca : $2a$10$kKyy3iIstihvuOG2/SwSB.n1dKSbzCnWLM7gGESMMNxtPmFjseGvy
Pass Admin : $2a$10$5IOhHRiOdyqWtnvvrjQof.09Bfd6I3HPZfi49p9CcX77snvCcNhbC



cas.auroresis.com 193.203.164.15
portail.auroresis.com 193.203.164.15
cm.banqueatlantique.digestapp.net 51.195.150.132


installation et generation des certificat ssl


cm.cca.iforce5demo.com



docker build -t frontend_digit_achat:if5demo-1.0 . && docker tag frontend_digit_achat:if5demo-1.0 iforce5/digeste-frontend_digit_achat:if5demo-1.0 && docker push iforce5/digeste-frontend_digit_achat:if5demo-1.0

docker build -t onetradesafe . && docker tag onetradesafe kouamfranky/onetradesafe && docker push kouamfranky/onetradesafe


# rechercher et liberer un port
> netstat -ano | findstr :8080
> taskkill /PID <PID> /F





