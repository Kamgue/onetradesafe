# Docker Build Maven Stage
FROM maven:3.6.3-jdk-11 AS build

# Set environment variables
ENV PROFILE="prod"
ENV HOME=/opt/app

# Create the application directory
WORKDIR $HOME

# Copy the pom.xml and download dependencies
COPY pom.xml .
RUN mvn dependency:go-offline -B

# Copy the rest of the application code
COPY src ./src
COPY . .

# Package the application
RUN mvn clean package -DskipTests -P ${PROFILE}

# Runtime stage
FROM openjdk:11
RUN mkdir -p /logs

# Set build-time arguments and environment variables
ARG APP_NAME="onetradesafe"
ARG APP_VERSION="0.0.1"
ARG JAR_FILE="/opt/app/target/${APP_NAME}-${APP_VERSION}.jar"
COPY --from=build ${JAR_FILE} app.jar

# Environment variables
ENV DB_NAME=db_name
ENV DB_PORT=3333
ENV DB_USERNAME=root
ENV DB_PASSWORD=root123456*
ENV DB_HOST=mysql_db1
ENV ENV_SERVER_PORT=8080
ENV ENV_WEB_SOCKET_PORT=7000
ENV ENV_JPA_DATABASE_PLATFORM=org.hibernate.dialect.MySQL8Dialect
ENV ENV_APP_BASE_URL=http://185.127.19.10:${ENV_SERVER_PORT}/
ENV ENV_APP_BASE_WS=wss://185.127.19.10:${ENV_SERVER_PORT}/

EXPOSE ${ENV_SERVER_PORT}
EXPOSE ${ENV_WEB_SOCKET_PORT}

CMD ["java", "-jar", "-Dspring.datasource.url=jdbc:mysql://${DB_HOST}:${DB_PORT}/${DB_NAME}?createDatabaseIfNotExist=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "-Dspring.datasource.username=${DB_USERNAME}", "-Dspring.datasource.password=${DB_PASSWORD}", "-Dserver.port=${ENV_SERVER_PORT}", "-Dspring.rsocket.server.port=${ENV_WEB_SOCKET_PORT}", "-Dspring.jpa.database-platform=${ENV_JPA_DATABASE_PLATFORM}", "-Dapp.base_url=${ENV_APP_BASE_URL}", "-Dapp.base_ws=${ENV_APP_BASE_WS}", "app.jar"]















#
## Docker Build Maven Stage
#FROM maven:3.6.3-jdk-11 AS build
#
## Set environment variables
#ENV PROFILE="prod"
#ENV HOME=/opt/app
#
## Create the application directory
#RUN mkdir -p $HOME
#WORKDIR $HOME
#
## Copy the pom.xml and download dependencies
#COPY pom.xml $HOME
#COPY src $HOME/src
#RUN mvn dependency:go-offline -B
#
## Copy the rest of the application code
#COPY . $HOME
#
## Package the application
#RUN mvn clean package -DskipTests -P ${PROFILE}
#
## Runtime stage
#FROM openjdk:11
#RUN mkdir -p /logs
#
## Set build-time arguments and environment variables
#ARG APP_NAME="onetradesafe"
#ARG APP_VERSION="0.0.1"
#ARG JAR_FILE="/opt/app/target/${APP_NAME}.jar"
#COPY --from=build ${JAR_FILE} app.jar
#
## Environment variables
#ENV DB_NAME=db_name
#ENV DB_PORT=3333
#ENV DB_USERNAME=root
#ENV DB_PASSWORD=root123456*
#ENV DB_HOST=mysql_db1
#ENV ENV_SERVER_PORT=8080
#ENV ENV_APP_BASE_URL=http://185.127.19.10:${ENV_SERVER_PORT}/
#ENV ENV_APP_BASE_WS=wss://185.127.19.10:${ENV_SERVER_PORT}/
#ENV ENV_JPA_DATABASE_PLATFORM=org.hibernate.dialect.MySQL8Dialect
#
#
#
#EXPOSE ${ENV_SERVER_PORT}
#
#CMD ["java", "-jar", "-Dspring.datasource.url=jdbc:mysql://${DB_HOST}:${DB_PORT}/${DB_NAME}?createDatabaseIfNotExist=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "-Dspring.datasource.username=${DB_USERNAME}", "-Dspring.datasource.password=${DB_PASSWORD}", "-Dserver.port=${ENV_SERVER_PORT}", "-Dspring.rsocket.server.port=${ENV_WEB_SOCKET_PORT}", "-Dspring.jpa.database-platform=${ENV_JPA_DATABASE_PLATFORM}", "-Dapp.base_url=${ENV_APP_BASE_URL}", "-Dapp.base_ws=${ENV_APP_BASE_WS}", "app.jar"]
